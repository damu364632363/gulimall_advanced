package com.atguigu.gulimall.product.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyRedissonConfig {

    /**
     * 所有对redisson的使用都是通过redissonclient对象
     * @return
     * @throws Exception
     */
    @Bean(destroyMethod = "shutdown")
    public RedissonClient redisson() throws Exception{
        //创建配置
        Config config = new Config();
//        config.useClusterServers()
//                .addNodeAddress();
        config.useSingleServer().setAddress("redis://124.70.56.244:6379")
                .setPassword("meiliaichangge");
//        config.useSingleServer().setAddress("redis://192.168.90.100:6379");
        //根据config创建出RedissonClient实例

        return Redisson.create(config);
    }
}
