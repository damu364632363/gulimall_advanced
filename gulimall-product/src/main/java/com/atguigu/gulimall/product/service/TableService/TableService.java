package com.atguigu.gulimall.product.service.TableService;

import com.atguigu.gulimall.product.data.dto.RecByCountDto;
import com.atguigu.gulimall.product.data.vo.RecByVo;

public interface TableService {

    RecByVo recByCount(RecByCountDto recByCountDto);
}
