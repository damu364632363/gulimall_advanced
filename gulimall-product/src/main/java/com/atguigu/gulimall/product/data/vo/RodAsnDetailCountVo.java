package com.atguigu.gulimall.product.data.vo;

import lombok.Data;

@Data
public class RodAsnDetailCountVo {

    private String recBy;

    private String recDate;

    private Integer count;
}
