package com.atguigu.gulimall.product.controller;

import java.util.Arrays;
import java.util.Map;

import com.atguigu.gulimall.product.entity.EmployeesEntity;
import com.atguigu.gulimall.product.service.EmployeesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.R;



/**
 * 
 *
 * @author leifengyang
 * @email leifengyang@gmail.com
 * @date 2022-01-27 13:47:45
 */
@RestController
@RequestMapping("product/employees")
public class EmployeesController {
    @Autowired
    private EmployeesService employeesService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("ware:employees:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = employeesService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{employeeId}")
    //@RequiresPermissions("ware:employees:info")
    public R info(@PathVariable("employeeId") Integer employeeId){
		EmployeesEntity employees = employeesService.getById(employeeId);

        return R.ok().put("employees", employees);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("ware:employees:save")
    public R save(@RequestBody EmployeesEntity employees){
		employeesService.save(employees);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("ware:employees:update")
    public R update(@RequestBody EmployeesEntity employees){
		employeesService.updateById(employees);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("ware:employees:delete")
    public R delete(@RequestBody Integer[] employeeIds){
		employeesService.removeByIds(Arrays.asList(employeeIds));

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/updateEmployeesAndUser")
    //@RequiresPermissions("ware:employees:update")
    public R updateEmployeesAndUser(@RequestBody EmployeesEntity employees){
        employeesService.updateEmployeesAndUser(employees);
        return R.ok();
    }


    @RequestMapping("/updateEmployeesAndDepartments")
    public R updateEmployeesAndDepartments(@RequestBody EmployeesEntity employees){
        employeesService.updateEmployeesAndDepartments(employees);
        return R.ok();
    }

    @RequestMapping("/updateEmployeesAndDepartments2")
    //@RequiresPermissions("ware:employees:update")
    public R updateEmployeesAndDepartments2(@RequestBody EmployeesEntity employees){
        employeesService.updateEmployeesAndDepartments2(employees);
        return R.ok();
    }



}
