package com.atguigu.gulimall.product.service;

import com.atguigu.gulimall.product.entity.DepartmentsEntity;
import com.atguigu.gulimall.product.entity.EmployeesEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author leifengyang
 * @email leifengyang@gmail.com
 * @date 2022-01-27 13:47:45
 */
public interface EmployeesService extends IService<EmployeesEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void updateEmployeesAndUser(EmployeesEntity employees);

    void updateEmployeesAndDepartments(EmployeesEntity employees);

    void updateDepartments(DepartmentsEntity departmentsEntity);

    void updateEmployeesAndDepartments2(EmployeesEntity employees);
}

