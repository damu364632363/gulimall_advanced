package com.atguigu.gulimall.product.web;

import com.atguigu.common.utils.R;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.CategoryService;
import com.atguigu.gulimall.product.vo.Catalog2VO;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static com.atguigu.common.constant.AuthServerConstant.LOGIN_USER;

@Controller
@Slf4j
//@RestController
public class IndexController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @RequestMapping({"/", "/index"})
    public String index(Model model,HttpSession session) {
        session.getServletContext();
        Object loginUser = session.getAttribute("loginUser");
        Object attribute = session.getAttribute(LOGIN_USER);
        // TODO: 2021/9/6 查询一级分类
        List<CategoryEntity> categories = categoryService.getLevel1Categories();
        model.addAttribute("categories", categories);
//        return "indexWithStatic";
        return "index";
    }

    @RequestMapping({ "/index_1"})
    public String index_1(Model model,HttpSession session) {
        session.getServletContext();
        Object loginUser = session.getAttribute("loginUser");
        Object attribute = session.getAttribute(LOGIN_USER);
        // TODO: 2021/9/6 查询一级分类
        List<CategoryEntity> categories = categoryService.getLevel1Categories();
        model.addAttribute("categories", categories);
        return "index_1";
    }

    @RequestMapping({"/testData"})
    public R testData(Model model) {
        // TODO: 2021/9/6 查询一级分类
        List<CategoryEntity> categories = categoryService.getLevel1Categories();
        model.addAttribute("categories", categories);
        return R.ok().put("data", categories);
    }

    /**
     * 首页三级分类渲染所需要的的数据模型
     *
     * @return
     */
    @RequestMapping("/index/catalog.json")
    @ResponseBody
    public Map<String, List<Catalog2VO>> getCatelogJson() {
        Map<String, List<Catalog2VO>> catelogJson = categoryService.getCatelogJson();
        return catelogJson;
    }

    @RequestMapping("/product/indexJ")
    @ResponseBody
    public String indexJ() {
//        Map<String, List<Catalog2VO>> catelogJson = categoryService.getCatelogJson();
        return "hello";
    }


    /**
     * jmeter测试专用
     *
     * @return
     */
    @RequestMapping("/product/index/catalog.json")
    @ResponseBody
    public Map<String, List<Catalog2VO>> productGetCatelogJson() {
        Map<String, List<Catalog2VO>> catelogJson = categoryService.getCatelogJson();
        return catelogJson;
    }

    /**
     * jmeter测试专用
     *
     * @return
     */
    @RequestMapping("/product/testHello")
    @ResponseBody
    public String testHello() {

        //获取一把锁，只要锁的名字一样，那就是同一把锁
        RLock lock = redissonClient.getLock("my-lock");

        //加锁，阻塞式等待。没设置过期时间，
        // 1默认加的锁都是30s，没指定锁的时间，默认30s后过期.2看门狗机制
//        lock.lock();
        //锁的自动续期，如果业务超长，运行期间给锁续上新的30s，不用担心业务时间长，锁自动过期被删除
        //,加锁的业务只要运行完成，就不会给当前锁续期，即使不手动解锁，锁默认在30s以后自动删除
        lock.lock(10, TimeUnit.SECONDS); //10秒自动解锁，自动解锁时间一定要大于业务的执行时间。设置过期时间没有看门狗
        //问题，如果设置锁失效时间，在锁时间到了以后，不会自动续期。
        //如果传递了锁的超时时间，就发送给redis执行脚本，进行占锁，默认超时就是我们指定的时间
        //如果未指定锁的超时时间，就使用 30* 1000 lockWatchdogTimeout看门狗的默认时间。
        // (不设置自动过期时间，看门狗机制)只要占锁成功，就会启动一个定时任务，重新给锁设置过期时间，新的过期时间就是看门狗的默认时间,每隔10秒都会自动再次续期，续成30s
        // internalLockLeaseTime【看门狗时间】 / 3,10s

        //最佳实战
        // 1  lock.lock(10, TimeUnit.SECONDS);省掉了整个续期操作，手动解锁
        try {
            log.info("加锁成功，执行业务》》》" + Thread.currentThread().getId());
            Thread.sleep(30000);
        } catch (Exception e) {
            e.getStackTrace();
        } finally {
            //释放锁
            log.info("释放锁" + Thread.currentThread().getId());
            lock.unlock();
        }
        return "hello";
    }

    //保证一定能读到最新数据，修改期间，写锁是一个排它锁，读锁是一个共享锁
    //写锁没释放，读锁要等待
    //读读，相当于无锁，并发读，只会在redis中记录好，所有当前的读锁。他们都会同时加锁成功
    //写读，等待写锁释放
    //写 + 写，阻塞方式
    //读+写： 有读锁，写也需要等待
    //只要有写的存在，都必须等待，
    @RequestMapping("/product/writeValue")
    @ResponseBody
    public String writeValue() {
        RReadWriteLock readWriteLock = redissonClient.getReadWriteLock("rw-lock");
        String s = "";
        RLock rLock = readWriteLock.writeLock();
        try {
            //改数据加写锁，读数据加读锁。

            rLock.lock();
            s = UUID.randomUUID().toString();
            Thread.sleep(30000);
            redisTemplate.opsForValue().set("writeValue", s);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            rLock.unlock();
        }
        return s;
    }


    @RequestMapping("/product/readValue")
    @ResponseBody
    public String readValue() {
        RReadWriteLock readWriteLock = redissonClient.getReadWriteLock("rw-lock");
        String s = "";
        //加读锁
        RLock rLock = readWriteLock.readLock();
        try {
            rLock.lock();
//            s = UUID.randomUUID().toString();
//            Thread.sleep(30000);
            s = redisTemplate.opsForValue().get("writeValue");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            rLock.unlock();
        }
        return s;
    }

    /**
     * 车库停车
     * 3车位
     * 信号量可以做分布式限流
     */
    @GetMapping("/product/park")
    @ResponseBody
    public String park() {
        RSemaphore park = redissonClient.getSemaphore("park");
        boolean b = false;
        try {
//            park.acquire(); //获取一个信号，获取一个值
            b = park.tryAcquire();
            if (b) {
                //执行业务
            } else {
                return "xio dan ji xia";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "ok:" + b;
    }

    @RequestMapping("/product/go")
    @ResponseBody
    public String go() throws InterruptedException {
        RSemaphore park = redissonClient.getSemaphore("park");
        park.release(); //获取一个信号，获取一个值
        return "ok";
    }


    /**
     * 放假锁门
     * 5个班全部走完，可以锁大门
     */
    @RequestMapping("/product/lockDoor")
    @ResponseBody
    public String lockDoor() throws InterruptedException {
        RCountDownLatch door = redissonClient.getCountDownLatch("door");
        door.trySetCount(5);
        //等待闭锁都完成
        door.await();


        return "放假了。。。。";
    }

    @GetMapping("/product/goHome/{id}")
    public String goHome(@PathVariable("id") Long id) {
        RCountDownLatch door = redissonClient.getCountDownLatch("door");
        door.countDown();//计数减1；
        return id + "班的人都走了";
    }


}
