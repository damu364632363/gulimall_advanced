package com.atguigu.gulimall.product.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author leifengyang
 * @email leifengyang@gmail.com
 * @date 2022-01-27 13:47:46
 */
@Data
@TableName("departments")
public class DepartmentsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer departmentId;
	/**
	 * 
	 */
	private String departmentName;
	/**
	 * 
	 */
	private Integer managerId;
	/**
	 * 
	 */
	private Integer locationId;

}
