package com.atguigu.gulimall.product.data.vo;

import lombok.Data;

import java.util.List;

@Data
public class RecByVo {

    private List<String> legendData;

    public Xaxis xaxis;

    public  List<Series> series;

//    public class Xaxis {
//    }


    @Data
    public
    class Xaxis {
        private String type;

        private Boolean boundaryGap;

        private List<String> xdate;
    }

    @Data
    public class Series {
        private String name;

        private String type;

        private List<Integer> seriesData;
    }
}