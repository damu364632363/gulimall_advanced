package com.atguigu.gulimall.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.gulimall.product.service.CategoryBrandRelationService;
import com.atguigu.gulimall.product.vo.Catalog2VO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimall.product.dao.CategoryDao;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Slf4j
@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

//    @Autowired
//    CategoryDao categoryDao;

    @Autowired
    CategoryBrandRelationService categoryBrandRelationService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Resource
    RedissonClient redisson;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        //1、查出所有分类
        List<CategoryEntity> entities = baseMapper.selectList(null);

        //2、组装成父子的树形结构

        //2.1）、找到所有的一级分类
        List<CategoryEntity> level1Menus = entities.stream().filter(categoryEntity ->
                categoryEntity.getParentCid() == 0
        ).map((menu) -> {
            menu.setChildren(getChildrens(menu, entities));
            return menu;
        }).sorted((menu1, menu2) -> {
            return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
        }).collect(Collectors.toList());


        return level1Menus;
    }

    public List<CategoryEntity> listWithTreeTest() {
        List<CategoryEntity> categoryEntities = baseMapper.selectList(null);
        //找到一级分类
        List<CategoryEntity> firstList = new ArrayList<>();
        for (int i = 0; i < categoryEntities.size(); i++) {
            if (0 == categoryEntities.get(i).getParentCid()) {
                firstList.add(categoryEntities.get(i));
            }
        }
        List<CategoryEntity> resultList = getChildrenTree(firstList, categoryEntities);
        return resultList;
    }

    public List<CategoryEntity> getChildrenTree(List<CategoryEntity> categoryEntityParentList, List<CategoryEntity> categoryEntityAllList) {
        for (CategoryEntity categoryEntity : categoryEntityParentList) {
            List<CategoryEntity> childList = new ArrayList<>();
            for (int i = 0; i < categoryEntityAllList.size(); i++) {
                if (((categoryEntity.getCatId())) == (categoryEntityAllList.get(i).getParentCid())) {
                    childList.add(categoryEntityAllList.get(i));
                }
            }
            childList = getChildrenTree(childList, categoryEntityAllList);
            categoryEntity.setChildren(childList);
        }

        return categoryEntityParentList;
    }


    @Override
    public void removeMenuByIds(List<Long> asList) {
        //TODO  1、检查当前删除的菜单，是否被别的地方引用

        //逻辑删除
        baseMapper.deleteBatchIds(asList);
    }

    //[2,25,225]
    @Override
    public Long[] findCatelogPath(Long catelogId) {
        List<Long> paths = new ArrayList<>();
        List<Long> parentPath = findParentPath(catelogId, paths);

        Collections.reverse(parentPath);


        return parentPath.toArray(new Long[parentPath.size()]);
    }

    /**
     * 级联更新所有关联的数据
     *@CacheEvict:失效模式
     * @param category
     * @Caching多缓存处理,
     * 同时进行多种操作@Caching
     * 存储同一个类型的数据，都可以指定成同一个分区。。分区名默认就是分区的前缀
     *
     */
//    @CacheEvict(value = {"category"}, key = "'getLevel1Categories'")
    @Caching(evict = {
            @CacheEvict(value = {"category"}, key = "'getLevel1Categories'"),
            @CacheEvict(value = {"category"}, key = "'getCatalogJson'")
    })
    @Transactional
    @Override
    public void updateCascade(CategoryEntity category) {
        this.updateById(category);
        categoryBrandRelationService.updateCategory(category.getCatId(), category.getName());
        //同时修改缓存中的数据
        //删除缓存数据，等待下次查询主动更新
        //
    }

    //每一个需要缓存的数据我们都要指定放到哪个名字的缓存。【缓存的分区，按照业务类型分】
//    @Cacheable(value = {"category"}, key = "#root.method.name")  //代表当前方法的结果需要缓存，如果缓存中有，方法不用调用。如果缓存中没有，最后将结果缓存
    @Override
    public List<CategoryEntity> getLevel1Categories() {
        List<CategoryEntity> parent_cid = baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", "0"));
        return parent_cid;
    }

    // TODO: 2021/9/8 会产生堆外内存溢出 OutOfDirectMemoryError
    //springboot2.0以后默认使用lettuce作为操作redis的客户端。他使用netty进行网络通信
    // lettuce的bug导致堆外内存溢出 -Xmx100m,如果netty没有指定堆外内存，默认使用-Xmx
    //可以通过 -Dio.netty.maxDirectMemory进行设置
    // 解决方案：不能使用-Dio.netty.maxDirectMemory 只去调大堆外内存
    //1 升级lettuce客户端。2切换使用jedis
    //
    @Override
    public Map<String, List<Catalog2VO>> getCatelogJson() {
        //给缓存中存放json字符串，拿出的json字符串逆转为能用的对象类型

        /**
         * 1.空结果缓存，解决缓存穿透
         * 2设置过期时间（加随机值），解决缓存雪崩
         * 3，加锁，解决缓存击穿
         */

        //加入缓存逻辑，缓存中村的数据是json字符串，json是跨语言跨平台兼容的
        String catalogJSON = redisTemplate.opsForValue().get("catalogJSON");
        if (StringUtils.isEmpty(catalogJSON)) {
            //缓存没有，查询数据库
            Map<String, List<Catalog2VO>> catelogJsonFromDb = getCatelogJsonFromDb();
            //查到的数据放入缓存，将对象放入缓存
            String jsonString = JSON.toJSONString(catelogJsonFromDb);
            redisTemplate.opsForValue().set("catalogJSON", jsonString);
            return catelogJsonFromDb;
        }
        //复杂类型转换
        Map<String, List<Catalog2VO>> result = JSON.parseObject(catalogJSON, new TypeReference<Map<String, List<Catalog2VO>>>() {
        });
        return result;
    }


    //redis锁2处核心，加锁保证原子性，解锁保证原子性
    public Map<String, List<Catalog2VO>> getCatelogJsonFromDbWithRedisLock() {

        //占分布式所，去redis占坑
        String uuid = UUID.randomUUID().toString();
        Boolean lock = redisTemplate.opsForValue().setIfAbsent("lock", uuid,300, TimeUnit.SECONDS);
        if (lock) {
            log.info("获取分布所成功");

            //加锁成功。执行业务
            //设置过期时间
//            redisTemplate.expire("lock",30, TimeUnit.SECONDS);
            Map<String, List<Catalog2VO>> catelogJsonFromDb = null;
            try {
                catelogJsonFromDb = getCatelogJsonFromDb();
            } finally {
//                lua脚本解锁
                String script = "if redis.call('get', KEYS[1]) == ARGV[1] then return redis.call('del', KEYS[1]) else return 0 end";
                //删除锁，原子锁
                Integer lock1 = redisTemplate.execute(new DefaultRedisScript<Integer>(script, Integer.class), Arrays.asList("lock"), uuid);
            }
            return catelogJsonFromDb;

        } else {
            //加锁失败，重试
            //休眠100ms重试
            log.info("获取分布所失败，，等待重试");

            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return getCatelogJsonFromDbWithRedisLock(); //自旋方式
        }
//        return null;
    }


    /**
     * 缓存里面的数据如何和数据库保持一致
     * 缓存数据一致性问题
     *  双写模式，改完数据库改缓存。会有时效问题，比如1改完数据库未改缓存，被2给改数据库也改缓存了。从而产生脏数据的情况。
     *  ----1、加锁来结局这个问题。2、如果允许不及时同步（一段时间缓存不一致），可以缓存，但是设置失效时间，这样下次更新缓存会改回来的
     *  失效模式，删除缓存数据，等待下次查询主动更新
     *  经常修改的数据，干脆直接不添加缓存
     *
     *  系统的一致性解决方案
     *  1，缓存的所有数据都有过期时间，数据过期下一次查询触发主动更新
     *  2，读写数据的时候，加上分布式的读写锁
     *     经常写，经常读。
     * @return
     */
    public Map<String, List<Catalog2VO>> getCatelogJsonFromDbWithRedissonLock() {
        //锁的名字，锁的粒度，越细越快
        //锁的粒度，具体缓存的是某个数据，11-号商品； product-11-lock
        RLock lock = redisson.getLock("catalogJson-lock");
        lock.lock();

        log.info("获取分布所成功");
            Map<String, List<Catalog2VO>> catelogJsonFromDb = null;
            try {
                catelogJsonFromDb = getCatelogJsonFromDb();
            } finally {
                lock.unlock();
            }
            return catelogJsonFromDb;

//        return null;
    }

    //从数据库查询
    public Map<String, List<Catalog2VO>> getCatelogJsonFromDb() {
        //查出所有1级分类
        List<CategoryEntity> level1Categories = getLevel1Categories();
        //封装数据
        Map<String, List<Catalog2VO>> parent_cid = level1Categories.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            //每一个的一级分类，查到这个一级分类的二级分类
            List<CategoryEntity> categoryEntities = baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", v.getCatId()));
            List<Catalog2VO> catalog2VOS = null;
            if (categoryEntities != null) {
                catalog2VOS = categoryEntities.stream().map(l2 -> {
                    Catalog2VO catalog2VO = new Catalog2VO(v.getCatId().toString(), null, l2.getCatId().toString(), l2.getName());

                    List<CategoryEntity> level3Catelog = baseMapper.selectList(new QueryWrapper<CategoryEntity>().eq("parent_cid", l2.getCatId()));

                    if (null != level3Catelog) {
                        List<Catalog2VO.Catelog3VO> collect = level3Catelog.stream().map(l3 -> {
                            //封装成指定格式
                            Catalog2VO.Catelog3VO catelog3VO = new Catalog2VO.Catelog3VO(l2.getCatId().toString(), l3.getCatId().toString(), l3.getName());
                            return catelog3VO;
                        }).collect(Collectors.toList());

                        catalog2VO.setCatalog3List(collect);

                    }


                    return catalog2VO;
                }).collect(Collectors.toList());
            }
            return catalog2VOS;
        }));
        return parent_cid;
    }

    //225,25,2
    private List<Long> findParentPath(Long catelogId, List<Long> paths) {
        //1、收集当前节点id
        paths.add(catelogId);
        CategoryEntity byId = this.getById(catelogId);
        if (byId.getParentCid() != 0) {
            findParentPath(byId.getParentCid(), paths);
        }
        return paths;

    }


    //递归查找所有菜单的子菜单
    private List<CategoryEntity> getChildrens(CategoryEntity root, List<CategoryEntity> all) {

        List<CategoryEntity> children = all.stream().filter(categoryEntity -> {
            return categoryEntity.getParentCid() == root.getCatId();
        }).map(categoryEntity -> {
            //1、找到子菜单
            categoryEntity.setChildren(getChildrens(categoryEntity, all));
            return categoryEntity;
        }).sorted((menu1, menu2) -> {
            //2、菜单的排序
            return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
        }).collect(Collectors.toList());

        return children;
    }

    public static void main(String[] args) {

        Integer a = 1;
        Long b = 1L;

        System.out.println(a.equals(b));

    }

}