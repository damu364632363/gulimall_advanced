package com.atguigu.gulimall.product.data.vo;

import lombok.Data;

@Data
public class HasStockVo {

    private Long skuId;

    private Boolean hasStock;


//    public static void main(String[] args) {
//        List<Long> longList = new ArrayList<>();
//        longList.add(2L);
//        longList.add(7L);
//        System.out.println(JSON.toJSON(longList));
//    }
}
