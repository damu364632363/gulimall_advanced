package com.atguigu.gulimall.product.web;

import com.atguigu.common.utils.R;
import com.atguigu.gulimall.product.service.SkuInfoService;
import com.atguigu.gulimall.product.vo.SkuItemVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.annotation.Resource;
import java.util.concurrent.ExecutionException;

@Controller
public class ItemController {

    @Resource
    SkuInfoService skuInfoService;

    @GetMapping("/{skuId}.html")
    public String skuItem(@PathVariable("skuId") Long skuId, Model model) throws ExecutionException, InterruptedException {
        System.out.println("查询的skuid" + skuId);
        SkuItemVo skuItemVo = skuInfoService.item(skuId);
        model.addAttribute("item",skuItemVo);
        return "item";
    }

//    @GetMapping("/{skuId}.html")
//    public R skuItem(@PathVariable("skuId") Long skuId, Model model) {
//        System.out.println("查询的skuid" + skuId);
//        SkuItemVo skuItemVo = skuInfoService.item(skuId);
//        model.addAttribute("item",skuItemVo);
//        return R.ok().put("skuItemVo",skuItemVo);
////        return "item";
//    }

//    @GetMapping("")
//    public String skuItem(){
//        return "item";
//    }
}
