package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.DepartmentsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author leifengyang
 * @email leifengyang@gmail.com
 * @date 2022-01-27 13:47:46
 */
@Mapper
public interface DepartmentsDao extends BaseMapper<DepartmentsEntity> {
	
}
