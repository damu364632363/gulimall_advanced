package com.atguigu.gulimall.product.feign;


import com.atguigu.common.to.es.SkuESModel;
import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author wangwei
 * 2020/10/23 14:31
 */
@FeignClient("gulimall-search")
public interface SearchFeignService {

    @RequestMapping("/search/save/product")
    R batchSaveSku(@RequestBody List<SkuESModel> list);
}
