package com.atguigu.gulimall.product.controller;


import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.product.data.dto.RecByCountDto;
import com.atguigu.gulimall.product.data.vo.RecByVo;
import com.atguigu.gulimall.product.service.TableService.TableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("product/table")
public class TableController {

    @Autowired
    TableService tableService;

    @PostMapping("/recByCount")
    //@RequiresPermissions("product:spuinfo:list")
    public R list(@RequestBody RecByCountDto recByCountDto){
        RecByVo recByVo = tableService.recByCount(recByCountDto);
        return R.ok().put("data", recByVo);
    }
}
