package com.atguigu.gulimall.product.dao;


import com.atguigu.gulimall.product.entity.EmployeesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author leifengyang
 * @email leifengyang@gmail.com
 * @date 2022-01-27 13:47:45
 */
@Mapper
public interface EmployeesDao extends BaseMapper<EmployeesEntity> {
	
}
