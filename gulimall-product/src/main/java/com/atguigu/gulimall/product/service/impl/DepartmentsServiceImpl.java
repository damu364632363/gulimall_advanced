package com.atguigu.gulimall.product.service.impl;

import com.atguigu.gulimall.product.dao.DepartmentsDao;
import com.atguigu.gulimall.product.entity.DepartmentsEntity;
import com.atguigu.gulimall.product.service.DepartmentsService;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import org.springframework.transaction.annotation.Transactional;


@Service("departmentsService")
public class DepartmentsServiceImpl extends ServiceImpl<DepartmentsDao, DepartmentsEntity> implements DepartmentsService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DepartmentsEntity> page = this.page(
                new Query<DepartmentsEntity>().getPage(params),
                new QueryWrapper<DepartmentsEntity>()
        );

        return new PageUtils(page);
    }

    @Transactional
    @Override
    public void updateDepartments(DepartmentsEntity departments) {
        this.baseMapper.updateById(departments);
        int i = 9/0;
    }

}