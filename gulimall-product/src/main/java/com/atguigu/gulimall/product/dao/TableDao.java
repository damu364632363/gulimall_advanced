package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.data.dto.RecByCountDto;
import com.atguigu.gulimall.product.data.vo.RecByVo;
import com.atguigu.gulimall.product.data.vo.RodAsnDetailCountVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TableDao {

    RecByVo recByCount(RecByCountDto recByCountDto);

    List<RodAsnDetailCountVo> getCount(RecByCountDto recByCountDto);

}
