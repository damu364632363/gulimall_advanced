package com.atguigu.gulimall.product.data.dto;

import lombok.Data;

import java.util.List;

@Data
public class RecByCountDto {

    private List<String> recByList;

    private String beginDate;

    private String endDate;
}
