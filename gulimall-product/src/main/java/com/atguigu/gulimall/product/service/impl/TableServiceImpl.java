package com.atguigu.gulimall.product.service.impl;

import com.atguigu.gulimall.product.dao.TableDao;
import com.atguigu.gulimall.product.data.dto.RecByCountDto;
import com.atguigu.gulimall.product.data.vo.RecByVo;
import com.atguigu.gulimall.product.data.vo.RodAsnDetailCountVo;

import com.atguigu.gulimall.product.service.TableService.TableService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Service
public class TableServiceImpl implements TableService {

    @Autowired
    TableDao tableDao;

    @Override
    public RecByVo recByCount(RecByCountDto recByCountDto) {
        RecByVo recByVo = new RecByVo();
        List<String> legendDataList = new ArrayList<>();
        List<String> xAxisList = new ArrayList<>();
        RecByVo.Xaxis xaxis = new RecByVo().new Xaxis();
        RecByVo.Series series = new RecByVo().new Series();
        List<RecByVo.Series> seriesList = new ArrayList<>();
        List<String> twoDaysDay = getTwoDaysDayAsc(recByCountDto.getBeginDate(), recByCountDto.getEndDate());
//        List<String> twoDaysDay = getTwoDaysDay(recByCountDto.getEndDate(), recByCountDto.getBeginDate());
//        List<Map<String, Object>> twoDaysDay = getTwoDaysDayAscDayMap(recByCountDto.getBeginDate(), recByCountDto.getEndDate());

        Map<String, RodAsnDetailCountVo> recByMap = new HashMap<>();
        List<RodAsnDetailCountVo> rodAsnDetailCountVos = tableDao.getCount(recByCountDto);
        for (int i = 0; i < rodAsnDetailCountVos.size(); i++) {
            RodAsnDetailCountVo rodAsnDetailCountVo = rodAsnDetailCountVos.get(i);
            String recBy = rodAsnDetailCountVo.getRecBy();
            if (!recByMap.containsKey(recBy)) {
                recByMap.put(recBy, rodAsnDetailCountVo);
                legendDataList.add(recBy);
            }
        }

        for (int i = 0; i < legendDataList.size(); i++) {
            String s = legendDataList.get(i);
            RecByVo.Series seriesData = new RecByVo().new Series();
            seriesData.setName(s);
            seriesData.setType("line");
            List<Integer> dataInteger = new ArrayList<>();
//            List<Map>
            List<Integer> innitDay = getInnitDay(twoDaysDay);
            for (int k = 0; k < twoDaysDay.size(); k++) {
                String date = twoDaysDay.get(k);
                for (int j = 0; j < rodAsnDetailCountVos.size(); j++) {
                    RodAsnDetailCountVo rodAsnDetailCountVo = rodAsnDetailCountVos.get(j);
                    if (StringUtils.equals(s, rodAsnDetailCountVo.getRecBy())) {
                        //判断指定日期，是否有相等值
                        if ((StringUtils.equals(date, rodAsnDetailCountVo.getRecDate()))) {
//                            dataInteger.add(rodAsnDetailCountVo.getCount());
                            innitDay.set(k,rodAsnDetailCountVo.getCount());
                        }
//                        else {
//                            dataInteger.add(0);
//                        }
                    }
                }
            }
            seriesData.setSeriesData(innitDay);
            seriesList.add(seriesData);

        }
        xaxis.setType("category");
        xaxis.setBoundaryGap(false);
        xaxis.setXdate(twoDaysDay);
        recByVo.setLegendData(legendDataList);
        recByVo.setSeries(seriesList);
        recByVo.setXaxis(xaxis);

        return recByVo;
    }

    public List<Integer> getInnitDay(List<String> days){
        List<Integer> innitData = new ArrayList<>();
        for (int i = 0; i < days.size(); i++) {
            innitData.add(0);
        }
        return innitData;
    }

    public static List<String> getTwoDaysDay(String dateStart, String dateEnd) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<String> dateList = new ArrayList<String>();
        try {
            Date dateOne = sdf.parse(dateStart);
            Date dateTwo = sdf.parse(dateEnd);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateTwo);
            dateList.add(dateEnd);
            while (calendar.getTime().after(dateOne)) { //倒序时间,顺序after改before其他相应的改动。
                calendar.add(Calendar.DAY_OF_MONTH, -1);
                dateList.add(sdf.format(calendar.getTime()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateList;
    }


    public static List<String> getTwoDaysDayAsc(String dateStart, String dateEnd) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<String> dateList = new ArrayList<String>();
        try {
            Date dateOne = sdf.parse(dateStart);
            Date dateTwo = sdf.parse(dateEnd);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateOne);
            dateList.add(dateStart);
            while (calendar.getTime().before(dateTwo)) { //倒序时间,顺序after改before其他相应的改动。
                calendar.add(Calendar.DAY_OF_MONTH, +1);
                dateList.add(sdf.format(calendar.getTime()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateList;
    }

    public static List<Map<String, Object>> getTwoDaysDayAscDayMap(String dateStart, String dateEnd) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<Map<String, Object>> dateList = new ArrayList<>();
        try {
            Date dateOne = sdf.parse(dateStart);
            Date dateTwo = sdf.parse(dateEnd);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateOne);
//            dateList.add(dateStart);
            Map<String, Object> dateMap = new HashMap<>();
            dateMap.put(dateStart, null);
            dateList.add(dateMap);

            while (calendar.getTime().before(dateTwo)) { //倒序时间,顺序after改before其他相应的改动。
                calendar.add(Calendar.DAY_OF_MONTH, +1);
                Map<String, Object> dateFormat = new HashMap<>();
                dateFormat.put(sdf.format(calendar.getTime()), null);
                dateList.add(dateFormat);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateList;
    }

    public static void main(String[] args) {
        List<Map<String, Object>> twoDaysDayAscDayMap = getTwoDaysDayAscDayMap("2021-07-19", "2021-08-05");
        log.info(twoDaysDayAscDayMap.toString());
    }
}
