package com.atguigu.gulimall.product.service;

import com.atguigu.gulimall.product.entity.DepartmentsEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;


import java.util.Map;

/**
 * 
 *
 * @author leifengyang
 * @email leifengyang@gmail.com
 * @date 2022-01-27 13:47:46
 */
public interface DepartmentsService extends IService<DepartmentsEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void updateDepartments(DepartmentsEntity departments);
}

