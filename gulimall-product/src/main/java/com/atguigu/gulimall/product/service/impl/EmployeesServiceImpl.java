package com.atguigu.gulimall.product.service.impl;

import com.atguigu.gulimall.product.dao.DepartmentsDao;
import com.atguigu.gulimall.product.dao.EmployeesDao;
import com.atguigu.gulimall.product.entity.DepartmentsEntity;
import com.atguigu.gulimall.product.entity.EmployeesEntity;
import com.atguigu.gulimall.product.entity.UserEntity;
import com.atguigu.gulimall.product.feign.MemberFeignService;
import com.atguigu.gulimall.product.service.DepartmentsService;
import com.atguigu.gulimall.product.service.EmployeesService;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * todo 声明式事务@Transactional传播机制分析
 * 一、声明式事务@Transactional 默认配置default Propagation.REQUIRED
 * 1、同一个类2个事务方法互相调用，事务生效，事务配置失效，都用调用方法的事务配置。因为绕过了代理对象
 * 2、不同方法的2个事务方法调用，事务生效，事务配置生效。
 *
 * 如何解决一.1绕过代理对象的问题，即如何不绕过代理
 * 解决：
 0）、导入 spring-boot-starter-aop
 1）、@EnableTransactionManagement(proxyTargetClass = true)
 2）、@EnableAspectJAutoProxy(exposeProxy=true)
 3）、AopContext.currentProxy();
 4)、AopContext.currentProxy().method() //method()为所调用的事务方法
 * <p>
 * 二、关于事务传播机制  @Transactional(propagation = Propagation.REQUIRED)
 * 1、PROPAGATION_REQUIRED：如果当前没有事务，就创建一个新事务，如果当前存在事务， 就加入该事务，该设置是最常用的设置。
 * 2、PROPAGATION_SUPPORTS：支持当前事务，如果当前存在事务，就加入该事务，如果当 前不存在事务，就以非事务执行。
 * 3、PROPAGATION_MANDATORY：支持当前事务，如果当前存在事务，就加入该事务，如果 当前不存在事务，就抛出异常。
 * 4、PROPAGATION_REQUIRES_NEW：创建新事务，无论当前存不存在事务，都创建新事务。
 * 5、PROPAGATION_NOT_SUPPORTED：以非事务方式执行操作，如果当前存在事务，就把当 前事务挂起。
 * 6、PROPAGATION_NEVER：以非事务方式执行，如果当前存在事务，则抛出异常。
 * 7、PROPAGATION_NESTED：如果当前存在事务，则在嵌套事务内执行。如果当前没有事务， 则执行与 PROPAGATION_REQUIRED 类似的操作。
 *
 *
 */

@Service("employeesService")
public class EmployeesServiceImpl extends ServiceImpl<EmployeesDao, EmployeesEntity> implements EmployeesService {

    @Resource
    private MemberFeignService memberFeignService;

    @Resource
    private DepartmentsService departmentsService;

    @Resource
    private DepartmentsDao departmentsDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<EmployeesEntity> page = this.page(
                new Query<EmployeesEntity>().getPage(params),
                new QueryWrapper<EmployeesEntity>()
        );

        return new PageUtils(page);
    }

    //    @Gla
    @GlobalTransactional
//    @Transactional
    @Override
    public void updateEmployeesAndUser(EmployeesEntity employees) {
        this.baseMapper.updateById(employees);
        UserEntity userEntity = new UserEntity();
        userEntity.setId(employees.getUserId());
        userEntity.setUserName(employees.getEmail());
        memberFeignService.updateUser(userEntity);
    }

//    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void updateEmployeesAndDepartments(EmployeesEntity employees) {
        this.baseMapper.updateById(employees);
        DepartmentsEntity departmentsEntity = new DepartmentsEntity();
        departmentsEntity.setDepartmentId(employees.getDepartmentId());
        departmentsEntity.setDepartmentName(employees.getDepartmentName());
        departmentsService.updateDepartments(departmentsEntity);

    }

    @Transactional(propagation=Propagation.NOT_SUPPORTED)
    @Override
    public void updateDepartments(DepartmentsEntity departmentsEntity) {
        departmentsDao.updateById(departmentsEntity);
        int i = 9 / 0;
    }

    @Transactional
    @Override
    public void updateEmployeesAndDepartments2(EmployeesEntity employees) {
        this.baseMapper.updateById(employees);
        EmployeesServiceImpl o = (EmployeesServiceImpl)AopContext.currentProxy();
        DepartmentsEntity departmentsEntity = new DepartmentsEntity();
        departmentsEntity.setDepartmentId(employees.getDepartmentId());
        departmentsEntity.setDepartmentName(employees.getDepartmentName());
//        updateDepartments(departmentsEntity);
        o.updateDepartments(departmentsEntity);
    }

}


//    create table Provincial(pid int,Provincial varchar(50),primary key (pid))
//        insert into Provincial values(1,'北京市');
//        insert into Provincial values(2,'天津市');
//        insert into Provincial values(3,'上海市');
//        insert into Provincial values(4,'重庆市');
//        insert into Provincial values(5,'河北省');
//        insert into Provincial values(6,'山西省');
//        insert into Provincial values(7,'台湾省');
//        insert into Provincial values(8,'辽宁省');
//        insert into Provincial values(9,'吉林省');
//        insert into Provincial values(10,'黑龙江省');
//        insert into Provincial values(11,'江苏省');
//        insert into Provincial values(12,'浙江省');
//        insert into Provincial values(13,'安徽省');
//        insert into Provincial values(14,'福建省');
//        insert into Provincial values(15,'江西省');
//        insert into Provincial values(16,'山东省');
//        insert into Provincial values(17,'河南省');
//        insert into Provincial values(18,'湖北省');
//        insert into Provincial values(19,'湖南省');
//        insert into Provincial values(20,'广东省');
//        insert into Provincial values(21,'甘肃省');
//        insert into Provincial values(22,'四川省');
//        insert into Provincial values(23,'贵州省');
//        insert into Provincial values(24,'海南省');
//        insert into Provincial values(25,'云南省');
//        insert into Provincial values(26,'青海省');
//        insert into Provincial values(27,'陕西省');
//        insert into Provincial values(28,'广西壮族自治区');
//        insert into Provincial values(29,'西藏自治区');
//        insert into Provincial values(30,'宁夏回族自治区');
//        insert into Provincial values(31,'新疆维吾尔自治区');
//        insert into Provincial values(32,'内蒙古自治区');
//        insert into Provincial values(33,'澳门特别行政区');
//        insert into Provincial values(34,'香港特别行政区')
//        ;
//
//        create table City(cid int not null,city varchar(50) primary key,pid int foreign key references Provincial(pid));
//        insert into City values(1,'北京市',1);
//        insert into City values(1,'天津市',2);
//        insert into City values(1,'上海市',3);
//        insert into City values(1,'重庆市',4);
//        insert into City values(1,'石家庄市',5);
//        insert into City values(2,'唐山市',5);
//        insert into City values(3,'秦皇岛市',5);
//        insert into City values(4,'邯郸市',5);
//        insert into City values(5,'邢台市',5);
//        insert into City values(6,'保定市',5);
//        insert into City values(7,'张家口市',5);
//        insert into City values(8,'承德市',5);
//        insert into City values(9,'沧州市',5);
//        insert into City values(10,'廊坊市',5);
//        insert into City values(11,'衡水市',5);
//        insert into City values(1,'太原市',6);
//        insert into City values(2,'大同市',6);
//        insert into City values(3,'阳泉市',6);
//        insert into City values(4,'长治市',6);
//        insert into City values(5,'晋城市',6);
//        insert into City values(6,'朔州市',6);
//        insert into City values(7,'晋中市',6);
//        insert into City values(8,'运城市',6);
//        insert into City values(9,'忻州市',6);
//        insert into City values(10,'临汾市',6);
//        insert into City values(11,'吕梁市',6);
//        insert into City values(1,'台北市',7);
//        insert into City values(2,'高雄市',7);
//        insert into City values(3,'基隆市',7);
//        insert into City values(4,'台中市',7);
//        insert into City values(5,'台南市',7);
//        insert into City values(6,'新竹市',7);
//        insert into City values(7,'嘉义市',7);
//        insert into City values(8,'台北县',7);
//        insert into City values(9,'宜兰县',7);
//        insert into City values(10,'桃园县',7);
//        insert into City values(11,'新竹县',7);
//        insert into City values(12,'苗栗县',7);
//        insert into City values(13,'台中县',7);
//        insert into City values(14,'彰化县',7);
//        insert into City values(15,'南投县',7);
//        insert into City values(16,'云林县',7);
//        insert into City values(17,'嘉义县',7);
//        insert into City values(18,'台南县',7);
//        insert into City values(19,'高雄县',7);
//        insert into City values(20,'屏东县',7);
//        insert into City values(21,'澎湖县',7);
//        insert into City values(22,'台东县',7);
//        insert into City values(23,'花莲县',7);
//        insert into City values(1,'沈阳市',8);
//        insert into City values(2,'大连市',8);
//        insert into City values(3,'鞍山市',8);
//        insert into City values(4,'抚顺市',8);
//        insert into City values(5,'本溪市',8);
//        insert into City values(6,'丹东市',8);
//        insert into City values(7,'锦州市',8);
//        insert into City values(8,'营口市',8);
//        insert into City values(9,'阜新市',8);
//        insert into City values(10,'辽阳市',8);
//        insert into City values(11,'盘锦市',8);
//        insert into City values(12,'铁岭市',8);
//        insert into City values(13,'朝阳市',8);
//        insert into City values(14,'葫芦岛市',8);
//        insert into City values(1,'长春市',9);
//        insert into City values(2,'吉林市',9);
//        insert into City values(3,'四平市',9);
//        insert into City values(4,'辽源市',9);
//        insert into City values(5,'通化市',9);
//        insert into City values(6,'白山市',9);
//        insert into City values(7,'松原市',9);
//        insert into City values(8,'白城市',9);
//        insert into City values(9,'延边朝鲜族自治州',9);
//        insert into City values(1,'哈尔滨市',10);
//        insert into City values(2,'齐齐哈尔市',10);
//        insert into City values(3,'鹤 岗 市',10);
//        insert into City values(4,'双鸭山市',10);
//        insert into City values(5,'鸡 西 市',10);
//        insert into City values(6,'大 庆 市',10);
//        insert into City values(7,'伊 春 市',10);
//        insert into City values(8,'牡丹江市',10);
//        insert into City values(9,'佳木斯市',10);
//        insert into City values(10,'七台河市',10);
//        insert into City values(11,'黑 河 市',10);
//        insert into City values(12,'绥 化 市',10);
//        insert into City values(13,'大兴安岭地区',10);
//        insert into City values(1,'南京市',11);
//        insert into City values(2,'无锡市',11);
//        insert into City values(3,'徐州市',11);
//        insert into City values(4,'常州市',11);
//        insert into City values(5,'苏州市',11);
//        insert into City values(6,'南通市',11);
//        insert into City values(7,'连云港市',11);
//        insert into City values(8,'淮安市',11);
//        insert into City values(9,'盐城市',11);
//        insert into City values(10,'扬州市',11);
//        insert into City values(11,'镇江市',11);
//        insert into City values(12,'泰州市',11);
//        insert into City values(13,'宿迁市',11);
//        insert into City values(1,'杭州市',12);
//        insert into City values(2,'宁波市',12);
//        insert into City values(3,'温州市',12);
//        insert into City values(4,'嘉兴市',12);
//        insert into City values(5,'湖州市',12);
//        insert into City values(6,'绍兴市',12);
//        insert into City values(7,'金华市',12);
//        insert into City values(8,'衢州市',12);
//        insert into City values(9,'舟山市',12);
//        insert into City values(10,'台州市',12);
//        insert into City values(11,'丽水市',12);
//        insert into City values(1,'合肥市',13);
//        insert into City values(2,'芜湖市',13);
//        insert into City values(3,'蚌埠市',13);
//        insert into City values(4,'淮南市',13);
//        insert into City values(5,'马鞍山市',13);
//        insert into City values(6,'淮北市',13);
//        insert into City values(7,'铜陵市',13);
//        insert into City values(8,'安庆市',13);
//        insert into City values(9,'黄山市',13);
//        insert into City values(10,'滁州市',13);
//        insert into City values(11,'阜阳市',13);
//        insert into City values(12,'宿州市',13);
//        insert into City values(13,'巢湖市',13);
//        insert into City values(14,'六安市',13);
//        insert into City values(15,'亳州市',13);
//        insert into City values(16,'池州市',13);
//        insert into City values(17,'宣城市',13);
//        insert into City values(1,'福州市',14);
//        insert into City values(2,'厦门市',14);
//        insert into City values(3,'莆田市',14);
//        insert into City values(4,'三明市',14);
//        insert into City values(5,'泉州市',14);
//        insert into City values(6,'漳州市',14);
//        insert into City values(7,'南平市',14);
//        insert into City values(8,'龙岩市',14);
//        insert into City values(9,'宁德市',14);
//        insert into City values(1,'南昌市',15);
//        insert into City values(2,'景德镇市',15);
//        insert into City values(3,'萍乡市',15);
//        insert into City values(4,'九江市',15);
//        insert into City values(5,'新余市',15);
//        insert into City values(6,'鹰潭市',15);
//        insert into City values(7,'赣州市',15);
//        insert into City values(8,'吉安市',15);
//        insert into City values(9,'宜春市',15);
//        insert into City values(10,'抚州市',15);
//        insert into City values(11,'上饶市',15);
//        insert into City values(1,'济南市',16);
//        insert into City values(2,'青岛市',16);
//        insert into City values(3,'淄博市',16);
//        insert into City values(4,'枣庄市',16);
//        insert into City values(5,'东营市',16);
//        insert into City values(6,'烟台市',16);
//        insert into City values(7,'潍坊市',16);
//        insert into City values(8,'济宁市',16);
//        insert into City values(9,'泰安市',16);
//        insert into City values(10,'威海市',16);
//        insert into City values(11,'日照市',16);
//        insert into City values(12,'莱芜市',16);
//        insert into City values(13,'临沂市',16);
//        insert into City values(14,'德州市',16);
//        insert into City values(15,'聊城市',16);
//        insert into City values(16,'滨州市',16);
//        insert into City values(17,'菏泽市',16);
//        insert into City values(1,'郑州市',17);
//        insert into City values(2,'开封市',17);
//        insert into City values(3,'洛阳市',17);
//        insert into City values(4,'平顶山市',17);
//        insert into City values(5,'安阳市',17);
//        insert into City values(6,'鹤壁市',17);
//        insert into City values(7,'新乡市',17);
//        insert into City values(8,'焦作市',17);
//        insert into City values(9,'濮阳市',17);
//        insert into City values(10,'许昌市',17);
//        insert into City values(11,'漯河市',17);
//        insert into City values(12,'三门峡市',17);
//        insert into City values(13,'南阳市',17);
//        insert into City values(14,'商丘市',17);
//        insert into City values(15,'信阳市',17);
//        insert into City values(16,'周口市',17);
//        insert into City values(17,'驻马店市',17);
//        insert into City values(18,'济源市',17);
//        insert into City values(1,'武汉市',18);
//        insert into City values(2,'黄石市',18);
//        insert into City values(3,'十堰市',18);
//        insert into City values(4,'荆州市',18);
//        insert into City values(5,'宜昌市',18);
//        insert into City values(6,'襄樊市',18);
//        insert into City values(7,'鄂州市',18);
//        insert into City values(8,'荆门市',18);
//        insert into City values(9,'孝感市',18);
//        insert into City values(10,'黄冈市',18);
//        insert into City values(11,'咸宁市',18);
//        insert into City values(12,'随州市',18);
//        insert into City values(13,'仙桃市',18);
//        insert into City values(14,'天门市',18);
//        insert into City values(15,'潜江市',18);
//        insert into City values(16,'神农架林区',18);
//        insert into City values(17,'恩施土家族苗族自治州',18);
//        insert into City values(1,'长沙市',19);
//        insert into City values(2,'株洲市',19);
//        insert into City values(3,'湘潭市',19);
//        insert into City values(4,'衡阳市',19);
//        insert into City values(5,'邵阳市',19);
//        insert into City values(6,'岳阳市',19);
//        insert into City values(7,'常德市',19);
//        insert into City values(8,'张家界市',19);
//        insert into City values(9,'益阳市',19);
//        insert into City values(10,'郴州市',19);
//        insert into City values(11,'永州市',19);
//        insert into City values(12,'怀化市',19);
//        insert into City values(13,'娄底市',19);
//        insert into City values(14,'湘西土家族苗族自治州',19);
//        insert into City values(1,'广州市',20);
//        insert into City values(2,'深圳市',20);
//        insert into City values(3,'珠海市',20);
//        insert into City values(4,'汕头市',20);
//        insert into City values(5,'韶关市',20);
//        insert into City values(6,'佛山市',20);
//        insert into City values(7,'江门市',20);
//        insert into City values(8,'湛江市',20);
//        insert into City values(9,'茂名市',20);
//        insert into City values(10,'肇庆市',20);
//        insert into City values(11,'惠州市',20);
//        insert into City values(12,'梅州市',20);
//        insert into City values(13,'汕尾市',20);
//        insert into City values(14,'河源市',20);
//        insert into City values(15,'阳江市',20);
//        insert into City values(16,'清远市',20);
//        insert into City values(17,'东莞市',20);
//        insert into City values(18,'中山市',20);
//        insert into City values(19,'潮州市',20);
//        insert into City values(20,'揭阳市',20);
//        insert into City values(21,'云浮市',20);
//        insert into City values(1,'兰州市',21);
//        insert into City values(2,'金昌市',21);
//        insert into City values(3,'白银市',21);
//        insert into City values(4,'天水市',21);
//        insert into City values(5,'嘉峪关市',21);
//        insert into City values(6,'武威市',21);
//        insert into City values(7,'张掖市',21);
//        insert into City values(8,'平凉市',21);
//        insert into City values(9,'酒泉市',21);
//        insert into City values(10,'庆阳市',21);
//        insert into City values(11,'定西市',21);
//        insert into City values(12,'陇南市',21);
//        insert into City values(13,'临夏回族自治州',21);
//        insert into City values(14,'甘南藏族自治州',21);
//        insert into City values(1,'成都市',22);
//        insert into City values(2,'自贡市',22);
//        insert into City values(3,'攀枝花市',22);
//        insert into City values(4,'泸州市',22);
//        insert into City values(5,'德阳市',22);
//        insert into City values(6,'绵阳市',22);
//        insert into City values(7,'广元市',22);
//        insert into City values(8,'遂宁市',22);
//        insert into City values(9,'内江市',22);
//        insert into City values(10,'乐山市',22);
//        insert into City values(11,'南充市',22);
//        insert into City values(12,'眉山市',22);
//        insert into City values(13,'宜宾市',22);
//        insert into City values(14,'广安市',22);
//        insert into City values(15,'达州市',22);
//        insert into City values(16,'雅安市',22);
//        insert into City values(17,'巴中市',22);
//        insert into City values(18,'资阳市',22);
//        insert into City values(19,'阿坝藏族羌族自治州',22);
//        insert into City values(20,'甘孜藏族自治州',22);
//        insert into City values(21,'凉山彝族自治州',22);
//        insert into City values(1,'贵阳市',23);
//        insert into City values(2,'六盘水市',23);
//        insert into City values(3,'遵义市',23);
//        insert into City values(4,'安顺市',23);
//        insert into City values(5,'铜仁地区',23);
//        insert into City values(6,'毕节地区',23);
//        insert into City values(7,'黔西南布依族苗族自治州',23);
//        insert into City values(8,'黔东南苗族侗族自治州',23);
//        insert into City values(9,'黔南布依族苗族自治州',23);
//        insert into City values(1,'海口市',24);
//        insert into City values(2,'三亚市',24);
//        insert into City values(3,'五指山市',24);
//        insert into City values(4,'琼海市',24);
//        insert into City values(5,'儋州市',24);
//        insert into City values(6,'文昌市',24);
//        insert into City values(7,'万宁市',24);
//        insert into City values(8,'东方市',24);
//        insert into City values(9,'澄迈县',24);
//        insert into City values(10,'定安县',24);
//        insert into City values(11,'屯昌县',24);
//        insert into City values(12,'临高县',24);
//        insert into City values(13,'白沙黎族自治县',24);
//        insert into City values(14,'昌江黎族自治县',24);
//        insert into City values(15,'乐东黎族自治县',24);
//        insert into City values(16,'陵水黎族自治县',24);
//        insert into City values(17,'保亭黎族苗族自治县',24);
//        insert into City values(18,'琼中黎族苗族自治县',24);
//        insert into City values(1,'昆明市',25);
//        insert into City values(2,'曲靖市',25);
//        insert into City values(3,'玉溪市',25);
//        insert into City values(4,'保山市',25);
//        insert into City values(5,'昭通市',25);
//        insert into City values(6,'丽江市',25);
//        insert into City values(7,'思茅市',25);
//        insert into City values(8,'临沧市',25);
//        insert into City values(9,'文山壮族苗族自治州',25);
//        insert into City values(10,'红河哈尼族彝族自治州',25);
//        insert into City values(11,'西双版纳傣族自治州',25);
//        insert into City values(12,'楚雄彝族自治州',25);
//        insert into City values(13,'大理白族自治州',25);
//        insert into City values(14,'德宏傣族景颇族自治州',25);
//        insert into City values(15,'怒江傈傈族自治州',25);
//        insert into City values(16,'迪庆藏族自治州',25);
//        insert into City values(1,'西宁市',26);
//        insert into City values(2,'海东地区',26);
//        insert into City values(3,'海北藏族自治州',26);
//        insert into City values(4,'黄南藏族自治州',26);
//        insert into City values(5,'海南藏族自治州',26);
//        insert into City values(6,'果洛藏族自治州',26);
//        insert into City values(7,'玉树藏族自治州',26);
//        insert into City values(8,'海西蒙古族藏族自治州',26);
//        insert into City values(1,'西安市',27);
//        insert into City values(2,'铜川市',27);
//        insert into City values(3,'宝鸡市',27);
//        insert into City values(4,'咸阳市',27);
//        insert into City values(5,'渭南市',27);
//        insert into City values(6,'延安市',27);
//        insert into City values(7,'汉中市',27);
//        insert into City values(8,'榆林市',27);
//        insert into City values(9,'安康市',27);
//        insert into City values(10,'商洛市',27);
//        insert into City values(1,'南宁市',28);
//        insert into City values(2,'柳州市',28);
//        insert into City values(3,'桂林市',28);
//        insert into City values(4,'梧州市',28);
//        insert into City values(5,'北海市',28);
//        insert into City values(6,'防城港市',28);
//        insert into City values(7,'钦州市',28);
//        insert into City values(8,'贵港市',28);
//        insert into City values(9,'玉林市',28);
//        insert into City values(10,'百色市',28);
//        insert into City values(11,'贺州市',28);
//        insert into City values(12,'河池市',28);
//        insert into City values(13,'来宾市',28);
//        insert into City values(14,'崇左市',28);
//        insert into City values(1,'拉萨市',29);
//        insert into City values(2,'那曲地区',29);
//        insert into City values(3,'昌都地区',29);
//        insert into City values(4,'山南地区',29);
//        insert into City values(5,'日喀则地区',29);
//        insert into City values(6,'阿里地区',29);
//        insert into City values(7,'林芝地区',29);
//        insert into City values(1,'银川市',30);
//        insert into City values(2,'石嘴山市',30);
//        insert into City values(3,'吴忠市',30);
//        insert into City values(4,'固原市',30);
//        insert into City values(5,'中卫市',30);
//        insert into City values(1,'乌鲁木齐市',31);
//        insert into City values(2,'克拉玛依市',31);
//        insert into City values(3,'石河子市　',31);
//        insert into City values(4,'阿拉尔市',31);
//        insert into City values(5,'图木舒克市',31);
//        insert into City values(6,'五家渠市',31);
//        insert into City values(7,'吐鲁番市',31);
//        insert into City values(8,'阿克苏市',31);
//        insert into City values(9,'喀什市',31);
//        insert into City values(10,'哈密市',31);
//        insert into City values(11,'和田市',31);
//        insert into City values(12,'阿图什市',31);
//        insert into City values(13,'库尔勒市',31);
//        insert into City values(14,'昌吉市　',31);
//        insert into City values(15,'阜康市',31);
//        insert into City values(16,'米泉市',31);
//        insert into City values(17,'博乐市',31);
//        insert into City values(18,'伊宁市',31);
//        insert into City values(19,'奎屯市',31);
//        insert into City values(20,'塔城市',31);
//        insert into City values(21,'乌苏市',31);
//        insert into City values(22,'阿勒泰市',31);
//        insert into City values(1,'呼和浩特市',32);
//        insert into City values(2,'包头市',32);
//        insert into City values(3,'乌海市',32);
//        insert into City values(4,'赤峰市',32);
//        insert into City values(5,'通辽市',32);
//        insert into City values(6,'鄂尔多斯市',32);
//        insert into City values(7,'呼伦贝尔市',32);
//        insert into City values(8,'巴彦淖尔市',32);
//        insert into City values(9,'乌兰察布市',32);
//        insert into City values(10,'锡林郭勒盟',32);
//        insert into City values(11,'兴安盟',32);
//        insert into City values(12,'阿拉善盟',32);
//        insert into City values(1,'澳门特别行政区',33);
//        insert into City values(1,'香港特别行政区',34);
