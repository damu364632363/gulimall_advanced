package com.atguigu.gulimall.search.service;

import com.atguigu.common.to.es.SkuESModel;

import java.io.IOException;
import java.util.List;

public interface ProductSaveService {


    Boolean productStatusUp(List<SkuESModel> skuESModelList) throws IOException;
}
