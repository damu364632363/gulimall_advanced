package com.atguigu.gulimall.search.controller;

import com.atguigu.gulimall.search.service.MallSearchBySelfService;
import com.atguigu.gulimall.search.service.MallSearchService;
import com.atguigu.gulimall.search.vo.SearchParam;
import com.atguigu.gulimall.search.vo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SearchController {

    @Autowired
    MallSearchService mallSearchService;

    @Autowired
    MallSearchBySelfService mallSearchBySelfService;

    @GetMapping("/list.html")
    public String listPage(SearchParam param, Model model) {

        //根据传递来的页面的查询参数，去es中检索商品
//        SearchResult result = mallSearchService.search(param);
        SearchResult result = mallSearchBySelfService.search(param);
        model.addAttribute("result", result);
        return "list";
    }
}
