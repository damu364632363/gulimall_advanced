package com.atguigu.gulimall.search.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ Author     ：damu
 * @ Date       ：Created in 20:36 2021/8/30
 * @ Modified By：
 * @Version: 1.0.0
 *
 * 导入依赖
 * 编写配置，给容器中注入一个RestHightLevelClient
 */
@Configuration
public class ElasticSearchConfig {


    public static final RequestOptions COMMON_OPTIONS;

    static {
        RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();
//        builder.addHeader("Authorization","Bearer" + TOKEN);
//        builder.setHttpAsyncResponseConsumerFactory(
//                new HttpAsyncResponseConsumerFactory.HeapBufferedResponseConsumerFactory(30*1024*1024*1024)
//        );
        COMMON_OPTIONS = builder.build();
    }


    @Bean
    public RestHighLevelClient esRestClient() {
        RestClientBuilder builder = null;
//        builder = RestClient.builder(new HttpHost("192.168.90.100",9200,"http"));
        builder = RestClient.builder(new HttpHost("8.140.152.182",9200,"http"));
//        builder = RestClient.builder(new HttpHost("192.168.0.107",9200,"http"));

        RestHighLevelClient client = new RestHighLevelClient(builder);

//        RestHighLevelClient client = new RestHighLevelClient(
//                RestClient.builder(
//                     new HttpHost("8.140.152.182",9200,"http")
////                        ,
////                        new HttpHost("localhost",9201,"http")
//        ));
        return client;
    }
}
