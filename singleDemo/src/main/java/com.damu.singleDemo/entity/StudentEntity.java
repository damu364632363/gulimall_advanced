package com.damu.singleDemo.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 
 * 
 * @author leifengyang
 * @email leifengyang@gmail.com
 * @date 2022-04-20 20:44:24
 */
@Data
@TableName("student")
public class StudentEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer id;
	/**
	 * 
	 */
	private String userCode;
	/**
	 * 
	 */
	private String userName;
	/**
	 * 
	 */
	private Integer schoolNo;
	/**
	 * 
	 */
	private String telNum;
	/**
	 * 
	 */
	private String password;

	private Date createTime;

	private String indexNum;

	private transient Integer number;
}
