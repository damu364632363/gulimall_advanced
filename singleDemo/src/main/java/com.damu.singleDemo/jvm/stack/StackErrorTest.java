package com.damu.singleDemo.jvm.stack;

/**
 * 设置栈内存大小：-Xss2m
 */
public class StackErrorTest {
    static int count = 0;
    public static void main(String[] args) {
        System.out.println(count++);
        main(args);
    }
}
