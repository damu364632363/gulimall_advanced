package com.damu.singleDemo.jvm.classLoader;

public class ClassLoaderTest {

    public static void main(String[] args) {
        //系统类加载器
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        System.out.println("系统类加载器" + systemClassLoader);//sun.misc.Launcher$AppClassLoader@18b4aac2

        //获取其上层，扩展类加载器
        ClassLoader extClassLoader = systemClassLoader.getParent();
        System.out.println("扩展类加载器" + extClassLoader);//sun.misc.Launcher$ExtClassLoader@123772c4

        ClassLoader parent = extClassLoader.getParent();
        System.out.println(parent); //null

//        ClassLoader parent1 = parent.getParent();
//        System.out.println(parent1);
        //对于用户自定义类来说：默认使用系统类加载器进行加载
        ClassLoader classLoader = ClassLoaderTest.class.getClassLoader();
        System.out.println("ClassLoaderTest.class.getClassLoader:" + classLoader);

        //String类是使用启动类加载器(引导类加载器）加载的--》java的核心类库都是用引导类加载器加载的
        ClassLoader classLoader1 = String.class.getClassLoader();
        System.out.println("string->classLoader: " + classLoader1);//null
    }
}
