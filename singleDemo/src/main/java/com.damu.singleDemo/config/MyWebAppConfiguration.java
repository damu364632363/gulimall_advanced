package com.damu.singleDemo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyWebAppConfiguration  implements WebMvcConfigurer {

    // 注入我们配置文件中写好的图片保存路径
    @Value("${user.filepath}")
    private String filePath;

    //定制资源映射
    @CrossOrigin
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        //意思是：url中读取到/image时，就会自动将/image解析成D:/idea/java_workspace/image/upload
//        registry.addResourceHandler("/image/**").addResourceLocations("E:/images/");
//        /**
//         * Linux系统
//         * registry.addResourceHandler("/upload/**").addResourceLocations("file:/home/image/upload/");
//         */
        registry.addResourceHandler("/api/images/**")
                .addResourceLocations("file:"+ filePath);
    }
}
