package com.damu.singleDemo.controller;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import com.damu.singleDemo.common.utils.PageUtils;
import com.damu.singleDemo.common.utils.R;
import com.damu.singleDemo.entity.StudentEntity;
import com.damu.singleDemo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


/**
 *
 */
@RestController
@RequestMapping("student")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @Value("${user.filepath}")
    private String filePath;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("ware:student:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = studentService.queryPage(params);

        return R.ok().put("page", page);
    }

    @RequestMapping("/addBatch")
    public R addBatch(@RequestBody StudentEntity studentEntity){
        return R.ok().put("data",studentService.saveStudentList(studentEntity));
    }

    @RequestMapping("/insertBatch")
    public R insertBatch(@RequestBody StudentEntity studentEntity){
        return R.ok().put("data",studentService.insertStudentList(studentEntity));
    }

    @RequestMapping("/insertBatchGroup")
    public R insertBatchGroup(@RequestBody StudentEntity studentEntity){
        return R.ok().put("data",studentService.insertStudentListByGroup(studentEntity));
    }
    @RequestMapping("/insertStudentListThread")
    public R insertStudentListThread(@RequestBody StudentEntity studentEntity){
        return R.ok().put("data",studentService.insertStudentListThread(studentEntity));
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("ware:student:info")
    public R info(@PathVariable("id") Integer id){
		StudentEntity student = studentService.getById(id);

        return R.ok().put("student", student);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("ware:student:save")
    public R save(@RequestBody StudentEntity student){
		studentService.save(student);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("ware:student:update")
    public R update(@RequestBody StudentEntity student){
		studentService.updateById(student);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("ware:student:delete")
    public R delete(@RequestBody Integer[] ids){
		studentService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }



    @RequestMapping(value = "/upload", method = RequestMethod.POST, consumes = "multipart/form-data")
    public String upload(@RequestPart("pic") MultipartFile multipartFile) {

        // 生成一个随机的名称，避免文件名重复
        UUID uuid = UUID.randomUUID();
        // 获取原文件名称
        String originalFileName = multipartFile.getOriginalFilename();
        // 获取原文件的后缀
        String fileSuffix = originalFileName.substring(originalFileName.lastIndexOf('.'));
        // 保存文件
        File file = new File(filePath + uuid + fileSuffix);
        try {
            multipartFile.transferTo(file);
        } catch (IOException e) {
            e.printStackTrace();
            return "error";
        }
        // 返回图片的完整访问路径，这地方ip和端口可以改为动态获得，这样在部署到服务器上时无需改变，为了方便起见这里直接写死了
        return "http://localhost:1717/api/images/" + uuid + fileSuffix;
    }

}
