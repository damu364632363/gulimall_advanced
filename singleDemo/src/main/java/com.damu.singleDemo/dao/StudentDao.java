package com.damu.singleDemo.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.damu.singleDemo.entity.StudentEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author leifengyang
 * @email leifengyang@gmail.com
 * @date 2022-04-20 20:44:24
 */
@Mapper
public interface StudentDao extends BaseMapper<StudentEntity> {
    int addStudentBatch(@Param("studentList") List<StudentEntity> studentEntities);

}
