package com.damu.singleDemo.service;

//import com.baomidou.mybatisplus.extension.service.IService;

import com.baomidou.mybatisplus.extension.service.IService;
import com.damu.singleDemo.common.utils.PageUtils;
import com.damu.singleDemo.entity.StudentEntity;


import java.util.Map;

public interface StudentService extends IService<StudentEntity> {

    PageUtils queryPage(Map<String, Object> params);

    int saveStudentList(StudentEntity studentEntity);

    int insertStudentList(StudentEntity studentEntity);

    int insertStudentListByGroup(StudentEntity studentEntity);

    int insertStudentListThread(StudentEntity studentEntity);
}