package com.damu.singleDemo.service.impl;

//import com.baomidou.mybatisplus.core.conditions.query.Query;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.ObjectUtil;
import com.damu.singleDemo.common.utils.PageUtils;
import com.damu.singleDemo.common.utils.Query;
import com.damu.singleDemo.dao.StudentDao;
//import com.damu.singleDemo.entity.StudentEntity;
import com.damu.singleDemo.entity.StudentEntity;
import com.damu.singleDemo.service.StudentService;
import com.damu.singleDemo.untils.SnowflakeConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;

@Slf4j
@Service("studentService")
public class StudentServiceImpl extends ServiceImpl<StudentDao, StudentEntity> implements StudentService {

    @Autowired
    SnowflakeConfig snowflakeConfig;

    @Resource
    StudentDao studentDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<StudentEntity> page = this.page(
                new Query<StudentEntity>().getPage(params),
                new QueryWrapper<StudentEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public int saveStudentList(StudentEntity studentEntity) {
        List<StudentEntity> studentEntityList = new ArrayList<>();
        int num = 10000;
        if (ObjectUtil.isNotEmpty(studentEntity.getNumber())) {
            num = studentEntity.getNumber();
        }
        for (int i = 0; i < num; i++) {
            StudentEntity studentEntity1 = new StudentEntity();
            studentEntity1.setCreateTime(new Date());
            studentEntity1.setUserName("xxm");
            studentEntity1.setPassword("meiliaichangge");
            studentEntity1.setIndexNum(String.valueOf(i));
            studentEntity1.setUserCode(UUID.randomUUID().toString().substring(0, 8));
            studentEntity1.setTelNum("13" + generateRandomArray(9));
//            studentEntity1.setId(new Random().nextInt(11));
            studentEntityList.add(studentEntity1);
        }
        long beginTime = System.currentTimeMillis();
        boolean b1 = this.saveBatch(studentEntityList);
        log.info("批量插入" + b1);
        long endTime = System.currentTimeMillis();
        log.info("耗时：" + (endTime - beginTime));
        return 0;
    }

    @Override
    public int insertStudentList(StudentEntity studentEntity) {
        int num = 10000;
        if (ObjectUtil.isNotEmpty(studentEntity.getNumber())) {
            num = studentEntity.getNumber();
        }
        List<StudentEntity> studentEntityList = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            StudentEntity studentEntity1 = new StudentEntity();
            studentEntity1.setCreateTime(new Date());
            studentEntity1.setUserName("gql");
            studentEntity1.setPassword("meiliaichangge");
            studentEntity1.setIndexNum(String.valueOf(i));
            studentEntity1.setUserCode(UUID.randomUUID().toString().substring(0, 8));
            studentEntity1.setTelNum("18" + generateRandomArray(9));
//            studentEntity1.setId(new Random().nextInt(11));
            studentEntityList.add(studentEntity1);
        }
        long beginTime = System.currentTimeMillis();
        int b = studentDao.addStudentBatch(studentEntityList);
        log.info("批量插入数据" + b);
        long endTime = System.currentTimeMillis();
        log.info("耗时：" + (endTime - beginTime));
        return 0;
    }

    @Override
    public int insertStudentListByGroup(StudentEntity studentEntity) {
        int num = 10000;
        if (ObjectUtil.isNotEmpty(studentEntity.getNumber())) {
            num = studentEntity.getNumber();
        }
        List<StudentEntity> studentEntityList = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            StudentEntity studentEntity1 = new StudentEntity();
            studentEntity1.setCreateTime(new Date());
            studentEntity1.setUserName("lyh");
            studentEntity1.setPassword("meiliaichangge");
            studentEntity1.setIndexNum(String.valueOf(i));
            studentEntity1.setUserCode(UUID.randomUUID().toString().substring(0, 8));
            studentEntity1.setTelNum("17" + generateRandomArray(9));
//            studentEntity1.setId(new Random().nextInt(11));
            studentEntityList.add(studentEntity1);
        }
        int c = studentEntityList.size() / 20;
        int b = studentEntityList.size() / c;
        int d = studentEntityList.size() % c;
        int sumResult = 0;
        long beginTime = System.currentTimeMillis();
        for (int i = c; i <= c * b; i = i + c) {
            sumResult += studentDao.addStudentBatch(studentEntityList.subList(i - c, i));
        }
        if (d != 0) {
            sumResult += studentDao.addStudentBatch(studentEntityList.subList(c * b, studentEntityList.size()));
        }
        log.info("批量插入数据" + b);
        long endTime = System.currentTimeMillis();
        log.info("耗时：" + (endTime - beginTime));
        return sumResult;
    }

    @Override
    public int insertStudentListThread(StudentEntity studentEntity) {
        int num = 10000;
        if (ObjectUtil.isNotEmpty(studentEntity.getNumber())) {
            num = studentEntity.getNumber();
        }
        List<StudentEntity> studentEntityList = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            StudentEntity studentEntity1 = new StudentEntity();
            studentEntity1.setCreateTime(new Date());
            studentEntity1.setUserName("tmt");
            studentEntity1.setPassword("meiliaichangge");
            studentEntity1.setIndexNum(String.valueOf(i));
            studentEntity1.setUserCode(UUID.randomUUID().toString().substring(0, 8));
            studentEntity1.setTelNum("15" + generateRandomArray(9));
            studentEntityList.add(studentEntity1);
        }
        int nThreads = 50;
        int size = studentEntityList.size();
        ExecutorService executorService = Executors.newFixedThreadPool(nThreads);
        List<Future<Integer>> futures = new ArrayList<>(nThreads);
        long beginTime = System.currentTimeMillis();
        for (int i = 0; i < nThreads; i++) {
           final List<StudentEntity> studentEntityList1 = studentEntityList.subList(size / nThreads * i, size / nThreads * (i + 1));
            Callable<Integer> task1 = ()->{
                int i1 = studentDao.addStudentBatch(studentEntityList1);
                return i1;
            };
            futures.add(executorService.submit(task1));
        }
//        log.info("批量插入数据" + b);
        long endTime = System.currentTimeMillis();
        log.info("耗时：" + (endTime - beginTime));
        executorService.shutdown();
        return 0;
    }

    public String generateRandomArray(int num) {
        String chars = "0123456789";
//        char[] rands = new char[num];
        String string = "";
        for (int i = 0; i < num; i++) {
            int rand = (int) (Math.random() * 10);
            string += String.valueOf(chars.charAt(rand));
        }
        return string;

    }
}