package com.atguigu.gulimall.member.service;

import com.atguigu.gulimall.member.entity.UserEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author leifengyang
 * @email leifengyang@gmail.com
 * @date 2022-01-27 14:51:09
 */
public interface UserService extends IService<UserEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void updateUserBySelf(UserEntity userEntity);
}

