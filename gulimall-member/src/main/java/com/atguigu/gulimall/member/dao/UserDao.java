package com.atguigu.gulimall.member.dao;


import com.atguigu.gulimall.member.entity.UserEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author leifengyang
 * @email leifengyang@gmail.com
 * @date 2022-01-27 14:51:09
 */
@Mapper
public interface UserDao extends BaseMapper<UserEntity> {
	
}
