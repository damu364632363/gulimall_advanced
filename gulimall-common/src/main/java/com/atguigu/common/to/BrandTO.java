package com.atguigu.common.to;

import lombok.Data;

/**
 * @author wangwei
 * 2021/1/11 14:35
 */
@Data
public class BrandTO {

    private Long brandId;

    private String name;
}
