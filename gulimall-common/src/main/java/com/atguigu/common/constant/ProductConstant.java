package com.atguigu.common.constant;

import lombok.Data;

public class ProductConstant {


    public enum  AttrEnum{
        ATTR_TYPE_BASE(1,"基本属性"),ATTR_TYPE_SALE(0,"销售属性");
        private int code;
        private String msg;

        AttrEnum(int code,String msg){
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }


    /**
     * spu状态
     */
    public enum SpuPublishStatus {
        CREATED(0, "刚创建"),
        UP(1,  "已上架"),
        DOWN(2,  "已下架");

        private int value;
        private String desc;

        SpuPublishStatus(int value, String desc) {
            this.value = value;
            this.desc = desc;
        }

        public int getValue() {
            return value;
        }


        public String getDesc() { return desc; }
    }

    /**
     * gulimall-product操作redis的键
     */
    public static class RedisKey {
        public static final String CATELOG_JSON_VALUE = "catelogJson";

        public static final String CATELOG_JSON_LOCK = "catelogJsonLock";
    }

    /**
     * gulimall-product使用springCache时，每个chache的名字(缓存分区)
     */
    public static class CacheName {
        public static final String PRODUCT_CATEGORY = "product-category";
    }
}
