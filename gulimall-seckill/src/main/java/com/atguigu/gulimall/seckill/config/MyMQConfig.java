//package com.atguigu.gulimall.seckill.config;
//
//
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.Exchange;
//import org.springframework.amqp.core.Queue;
//import org.springframework.amqp.core.TopicExchange;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@Configuration
//public class MyMQConfig {
//
////    @RabbitListener(queues = "order.release.order.queue")
////    public void listener(OrderEntity entity, Channel channel, Message message) throws IOException{
////        System.out.println("收到过期的订单信息，准备关闭订单" + entity.getOrderSn());
////
////        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
////    }
//
//    /**
//     * 容器中的bingd，queue，exhanchange都会自动创建（mq里面没有）
//     *
//     * @return
//     */
//    @Bean
//    public Queue orderDelayQueue() {
//        Map<String, Object> arguments = new HashMap<>();
//        arguments.put("x-dead-letter-exchange", "order-event-exchange");
//        arguments.put("x-dead-letter-routing-key", "order.release.order");
//        arguments.put("x-message-ttl", 60000);
//        //String name, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments
//        Queue queue = new Queue("order.delay.queue", true, false, false, arguments);
//        return queue;
//    }
//
//    @Bean
//    public Queue orderReleaseOrderQueue() {
//        Queue queue = new Queue("order.release.order.queue", true, false, false);
//        return queue;
//    }
//
//    @Bean
//    public Exchange orderEventExchange() {
//        return new TopicExchange("order-event-exchange", true, false);
//    }
//
//    @Bean
//    public Binding orderCreateOrderBingding() {
//        return new Binding("order.delay.queue", Binding.DestinationType.QUEUE,
//                "order-event-exchange", "order.create.order", null);
//    }
//
//    @Bean
//    public Binding orderReleaseOrderBingding() {
//        return new Binding("order.release.order.queue", Binding.DestinationType.QUEUE,
//                "order-event-exchange", "order.release.order", null);
//    }
//
//
//    /**
//     * 订单释放直接和库存释放进行绑定
//     * @return
//     */
//    @Bean
//    public Binding orderReleaseOtherBingding() {
//        return new Binding("stock.release.stock.queue", Binding.DestinationType.QUEUE,
//                "order-event-exchange", "order.release.other.#", null);
//    }
//}
