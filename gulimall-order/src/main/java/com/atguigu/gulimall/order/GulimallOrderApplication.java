package com.atguigu.gulimall.order;

import com.alibaba.cloud.seata.GlobalTransactionAutoConfiguration;
import io.seata.spring.annotation.GlobalTransactional;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * 使用rabbitmq
 * 引入场景
 * 给容器中自动配置了rabbittemplate，
 */

/**
 * 本地事务失效问题
 * 同一个对象内事务方法互调默认失败，原因，绕过了代理对象，事务使用代理对象控制的
 * 解决：使用代理对象来调用事务方法
 * 1）。引入aop-starter;spring-boot-starter-aop;引入了aspectj
 * 2）@EnableAspectJAutoProxy; 开启了aspectj动态代理功能。以后所有的动态代理都是aspectj创建的（即使没有接口也可以创建动态代理）
 *   (exposeProxy = true)对外暴露代理对象
 * 3）本类对象 OrderServiceImpl orderService = (OrderServiceImpl)AopContext.currentProxy();
 */

 /** Seata控制分布式事务
         *  1）、每一个微服务必须创建undo_Log
         *  2）、安装事务协调器：seate-server
         *  3）、整合
         *      1、导入依赖
         *      2、解压并启动seata-server：
         *          registry.conf:注册中心配置    修改 registry ： nacos
         *      3、所有想要用到分布式事务的微服务使用seata DataSourceProxy 代理自己的数据源
         *      4、每个微服务，resources都必须导入registry.conf和file.conf (这2个文件都来自seata的软件包conf文件夹下)
         *          vgroup_mapping.{application.name}-fescar-server-group = "default"
         *      5、启动测试分布式事务
         *      6、给分布式大事务的入口标注@GlobalTransactional
         *      7、每一个远程的小事务用@Trabsactional

  *
 */

@EnableAspectJAutoProxy(exposeProxy = true)
@EnableFeignClients
@EnableRedisHttpSession
@EnableRabbit
@SpringBootApplication(exclude = GlobalTransactionAutoConfiguration.class)
public class GulimallOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallOrderApplication.class, args);
    }

}
