package com.atguigu.gulimall.order.config;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.annotation.PostConstruct;
import java.util.UUID;

@Configuration
public class MyRabbitConfig {


//    @Autowired
    RabbitTemplate rabbitTemplate;

    @Primary
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        this.rabbitTemplate = rabbitTemplate;
        rabbitTemplate.setMessageConverter(messageConverter());
        initRabbitTemplate();
        return rabbitTemplate;
    }


    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    /**
     * 定值rabbitTemplate
     * 1）服务器收到消息就回调(发送端确认）
     *    1.spring.rabbitmq.publisher-confirms=true
     *    2.设置确认回调confirmcallback
     * 2）消息正确抵达队列回调(发送端确认）
     *    1.开启发送端消息抵达队列的确认  spring.rabbitmq.publisher-returns=true
     *    2.只要抵达队列，以异步发送优先回调我们这个returnconfirm
     *
     * 3）消费端确认（阿宝在每个消息被正确消费，才可以broker删除这个消息
     *    1、默认自动确认，只要消息接收到，客户端会自动确认，服务端就会移除这个消息
     *    问题：
     *        我们收到很多消息，自动回复给服务器ack，只有一个消息处理成功，宕机，消息会丢失
     *        手动确认。   spring.rabbitmq.listener.simple.acknowledge-mode= manual
     *        消费者手动ack消息 .只要我们没有明确告诉mq，货物被签收。没有ack，消息一直是unacked状态，
     *        即使consumer宕机，消息也不会丢失，会重新变为ready，下一次启动重新是ready状态
     *     2、如何签收
     *       签收channel.basicAck(deliveryTag, false);
     *       拒签 channel.basicNack(deliveryTag,false,true);
     *
     */
//    @PostConstruct//MyRabbitConfig对象创建完成后，执行这个方法
    public void initRabbitTemplate() {
        //设置确认回调
        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
            /**
             *1、只要消息抵达Broker就ack = true
             * @param correlationData 当前消息的唯一关联数据，这个是消息的唯一id
             * @param ack 消息是否成功收到
             * @param cause 失败的原因
             */
            @Override
            public void confirm(CorrelationData correlationData, boolean ack, String cause) {
                /**
                 * 如何防止消息丢失
                 * 1.做好消息确认机制（publisher，consumer[手动ack]
                 * 2.每一个发送的消息都在数据库做好记录，定期将失败的消息再次发送一遍
                 *
                 *
                 * 如何防止消息重复
                 * 将业务消费设计成幂等的，即使消息重复，也不影响业务
                 *
                 * 如何保证消息可靠性-消息积压
                 *
                 * 上线更多的消费者，进行正常消费
                 * 上线专门的队列消费服务，将消息先批量抽取出来，记录数据库，离线慢慢处理
                 */
                /**
                 * 服务器收到
                 */
                System.out.println("confirm。。。correlationData：" + correlationData + " ack;" + ack + " cause:" + cause);

            }
        });

        //设置消息抵达队列的确认回调
        rabbitTemplate.setReturnCallback(new RabbitTemplate.ReturnCallback() {
            /**
             * 只要消息没有投递给指定的队列，就触发这个失败回调
             * @param message 投递失败的消息详细信息
             * @param replyCode 回复的状态码
             * @param replyText 回复的文本内容
             * @param exchange  当时这个消息发给哪个交换机
             * @param routingKey 当时这个消息用哪个路由器
             */
            @Override
            public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
                //报错了，修改数据库当前消息的状态->状态
                System.out.println("message。。。：" + message + " replyCode;" + replyCode + " replyText:" + replyText + " exchange:"
                        + exchange + " routingKey:" + routingKey);
            }
        });
    }


//    public void initRabbitTemplate() {
//
//        /**
//         * 1、只要消息抵达Broker就ack=true
//         * correlationData：当前消息的唯一关联数据(这个是消息的唯一id)
//         * ack：消息是否成功收到
//         * cause：失败的原因
//         */
//        //设置确认回调
//        rabbitTemplate.setConfirmCallback((correlationData,ack,cause) -> {
//            System.out.println("confirm...correlationData["+correlationData+"]==>ack:["+ack+"]==>cause:["+cause+"]");
//        });
//
//
//        /**
//         * 只要消息没有投递给指定的队列，就触发这个失败回调
//         * message：投递失败的消息详细信息
//         * replyCode：回复的状态码
//         * replyText：回复的文本内容
//         * exchange：当时这个消息发给哪个交换机
//         * routingKey：当时这个消息用哪个路邮键
//         */
//        rabbitTemplate.setReturnCallback((message,replyCode,replyText,exchange,routingKey) -> {
//            System.out.println("Fail Message["+message+"]==>replyCode["+replyCode+"]" +
//                    "==>replyText["+replyText+"]==>exchange["+exchange+"]==>routingKey["+routingKey+"]");
//        });
//    }
}
