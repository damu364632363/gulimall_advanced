package com.atguigu.gulimall.order.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.gulimall.order.entity.OrderEntity;
import com.atguigu.gulimall.order.entity.OrderReturnReasonEntity;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimall.order.dao.OrderItemDao;
import com.atguigu.gulimall.order.entity.OrderItemEntity;
import com.atguigu.gulimall.order.service.OrderItemService;


@Slf4j
//@RabbitListener(queues = {"meiliCG"})
@Service("orderItemService")
public class OrderItemServiceImpl extends ServiceImpl<OrderItemDao, OrderItemEntity> implements OrderItemService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<OrderItemEntity> page = this.page(
                new Query<OrderItemEntity>().getPage(params),
                new QueryWrapper<OrderItemEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * queues，生命需要监听的所有队列
     */
//    @RabbitListener(queues = {"gulixueyuan.news"})
    //@RabbitListener可以放在类或者方法上面 监听哪些队列
    //@RabbitHandler放在方法上面，重载区分不同的消息
    @RabbitHandler
    public void receiveMessage(Message message, OrderReturnReasonEntity content, Channel channel) {
        log.info("接收到的消息" + content);
        byte[] body = message.getBody();
        MessageProperties messageProperties = message.getMessageProperties();
        log.info("消息处理完成" + content.getName());

        //channel内按顺序自增的
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        log.info("deliveryTag===>" + deliveryTag);
        //签收货物，非批量模式

        try {
            if (deliveryTag % 2 == 0) {
                //收货
                channel.basicAck(deliveryTag, false);
                log.info("签收了货物：" + deliveryTag);
            }else {
                //退货 var4= false 丢弃 var4=true 重新入队
                //void basicNack(long var1, boolean var3, boolean var4) throws IOException;
                channel.basicNack(deliveryTag,false,true);
                log.info("消息没有签收：" + deliveryTag);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    @RabbitHandler
//    public void receiveMessage(OrderEntity content) {
//        log.info("消息处理完成" + content);
//    }

    public static void main(String[] args) {
        List<String> listJson = new ArrayList<>();
        listJson.add("9");
        listJson.add("88");
        List<Integer> listIntegerJson = new ArrayList<>();
        listIntegerJson.add(66);
        listIntegerJson.add(99);
        String jsonString = JSON.toJSONString(listJson);

        String jsonString1 = JSON.toJSONString(listIntegerJson);
        System.out.println("j1" +jsonString);
        System.out.println("j2" + jsonString1);
        System.out.println(">>>");
    }

}