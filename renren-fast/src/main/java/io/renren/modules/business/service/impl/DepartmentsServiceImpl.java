package io.renren.modules.business.service.impl;

import io.renren.modules.business.service.DepartmentsService;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import io.renren.modules.business.dao.*;
import io.renren.modules.business.entity.*;



@Service("departmentsService")
public class DepartmentsServiceImpl extends ServiceImpl<DepartmentsDao, DepartmentsEntity> implements DepartmentsService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<DepartmentsEntity> page = this.page(
                new Query<DepartmentsEntity>().getPage(params),
                new QueryWrapper<DepartmentsEntity>()
        );

        return new PageUtils(page);
    }

}