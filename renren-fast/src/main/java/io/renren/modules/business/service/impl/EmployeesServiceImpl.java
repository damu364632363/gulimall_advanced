package io.renren.modules.business.service.impl;

import io.renren.modules.business.service.EmployeesService;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import io.renren.modules.business.dao.EmployeesDao;
import io.renren.modules.business.entity.EmployeesEntity;


@Service("employeesService")
public class EmployeesServiceImpl extends ServiceImpl<EmployeesDao, EmployeesEntity> implements EmployeesService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<EmployeesEntity> page = this.page(
                new Query<EmployeesEntity>().getPage(params),
                new QueryWrapper<EmployeesEntity>()
        );

        return new PageUtils(page);
    }

}