package io.renren.modules.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;

import java.util.Map;
import io.renren.modules.business.entity.EmployeesEntity;

/**
 *
 */
public interface EmployeesService extends IService<EmployeesEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

