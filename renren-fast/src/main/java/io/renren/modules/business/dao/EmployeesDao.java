package io.renren.modules.business.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import io.renren.modules.business.entity.EmployeesEntity;

/**
 * 
 * 
 * @author leifengyang
 * @email leifengyang@gmail.com
 * @date 2022-01-14 10:48:53
 */
@Mapper
public interface EmployeesDao extends BaseMapper<EmployeesEntity> {
	
}
