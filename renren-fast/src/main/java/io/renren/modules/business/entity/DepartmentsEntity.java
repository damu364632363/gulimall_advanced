package io.renren.modules.business.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 *
 */
@Data
@TableName("departments")
public class DepartmentsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer departmentId;
	/**
	 * 
	 */
	private String departmentName;
	/**
	 * 
	 */
	private Integer managerId;
	/**
	 * 
	 */
	private Integer locationId;

}
