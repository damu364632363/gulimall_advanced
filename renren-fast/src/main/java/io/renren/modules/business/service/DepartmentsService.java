package io.renren.modules.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import io.renren.modules.business.entity.*;


import java.util.Map;

/**
 *
 */
public interface DepartmentsService extends IService<DepartmentsEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

