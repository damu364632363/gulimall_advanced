package io.renren.modules.business.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.renren.annotation.IsMobile;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 */
@Data
@TableName("employees")
public class EmployeesEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Integer employeeId;
	/**
	 * 
	 */
	private String firstName;
	/**
	 * 
	 */
	private String lastName;
	/**
	 * 
	 */
	private String email;
	/**
	 * 
	 */
	@IsMobile(message = "{mobile}")
	private String phoneNumber;
	/**
	 * 
	 */
	private String jobId;
	/**
	 * 
	 */
	private Double salary;
	/**
	 * 
	 */
	private Double commissionPct;
	/**
	 * 
	 */
	private Integer managerId;
	/**
	 * 
	 */
	private Integer departmentId;
	/**
	 * 
	 */
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date hiredate;

}
