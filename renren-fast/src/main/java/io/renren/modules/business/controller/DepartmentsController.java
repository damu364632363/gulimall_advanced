package io.renren.modules.business.controller;

import java.util.Arrays;
import java.util.Map;

import io.renren.modules.business.service.DepartmentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.renren.modules.business.entity.DepartmentsEntity;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.R;




@RestController
@RequestMapping("demo/departments")
public class DepartmentsController {
    @Autowired
    private DepartmentsService departmentsService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("ware:departments:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = departmentsService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{departmentId}")
    //@RequiresPermissions("ware:departments:info")
    public R info(@PathVariable("departmentId") Integer departmentId){
		DepartmentsEntity departments = departmentsService.getById(departmentId);

        return R.ok().put("departments", departments);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("ware:departments:save")
    public R save(@RequestBody DepartmentsEntity departments){
		departmentsService.save(departments);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("ware:departments:update")
    public R update(@RequestBody DepartmentsEntity departments){
		departmentsService.updateById(departments);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("ware:departments:delete")
    public R delete(@RequestBody Integer[] departmentIds){
		departmentsService.removeByIds(Arrays.asList(departmentIds));

        return R.ok();
    }

}
