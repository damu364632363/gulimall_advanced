package io.renren.data.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class RedisParamDto implements Serializable{

    private String key;

    private String item;

    private Object object;
}
