package io.renren.data.vo;

import lombok.Data;

@Data
public class NopOrderPickupVo {

    private String relNo;
    private String skuCode;
    private String inventoryType;
    private String locationCode;
    private String qty;
    private String receiveDate;
}
