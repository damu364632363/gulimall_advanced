package io.renren.damu;

import org.apache.commons.lang3.ObjectUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class dateTest {
//    public static void main(String[] args) {
//        String srInformDate="2022/3/29";
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        try {
//            Date parse = simpleDateFormat.parse(srInformDate);
//            System.out.println("parse:" + parse);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }

    //Arrays.asList(r) 通过arrays.asList()转换的集合时，不能使用其修改集合相关的方法。
    //他的add/remove/clear 方法会抛出unsupportedOperationException异常
//    public static void main(String[] args) {
//        Integer[] r = {2, 5, 7, 8};
//        List<Integer> list = Arrays.asList(r);
//        list.add(99);
//        System.out.println("<<<" + list.toString());
//    }


    //不要在 for 循环里进行元素的 remove/add操作。remove元素请使用Iterator方式，如果并发操作，需要对Iterator对象加锁。
    //ConcurrentModificationException
//    public static void main(String[] args) {
//        List<String> list = new ArrayList<String>();
//        list.add("2");
//        list.add("8");
//        list.add("6");
//        System.out.println("<<<<" + list);
////        for (String s : list) {
////            if ("2".equals(s)) {
////                list.remove(s);
////            }
////        }
////        for (int i = 0; i < list.size(); i++) {
////            if ("6".equals(list.get(i))) {
////                list.remove(list.get(i));
////            }
////        }
////        list.forEach((p) -> {
////            if ("6".equals(p)) {
////                list.remove(p);
////            }
////        });
//        Iterator<String> iterator = list.iterator();
//        while (iterator.hasNext()){
//            if ("6".equals(iterator.next())) {
//                iterator.remove();
//            }
//        }
//        System.out.println("<<<<" + list);
//    }

    public static void main(String[] args) {
//        List<String> list = new ArrayList<>();
//        list.add("yy");
//        list.add("yy");
//        list.add("kk");
//        Set<String> set = new HashSet<>(list);
//        list = new ArrayList<>(set);
//        System.out.println("list>>>" + list);
        Object o = null;
        if("0".equals(o)){
            System.out.println(">>>>>>");
        } else {
            System.out.println("<<<<<<,");
        }

    }
}
