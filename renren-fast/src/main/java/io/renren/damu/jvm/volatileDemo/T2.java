package io.renren.damu.jvm.volatileDemo;

public class T2 {

    public static void main(String[] args) {
        int mpp = 0;
        MyNumber myNumber = new MyNumber();
        new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            myNumber.setNum(500);
            System.out.println(Thread.currentThread().getName() + "\t update number, number value:" + myNumber.num);
        }, "AAA").start();
//        while (myNumber.num == 10) {
////            System.out.println(mpp++);
//            //需要有一种通知机制告诉main线程，number已经修改，跳出while
//        }
        System.out.println(myNumber.num);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + "\t mission is over num:" + myNumber.num);
    }
}

class MyNumber {
    volatile int num = 10;
//    int num = 10;

    public void setNum(int num) {
        this.num = num;
    }
}
