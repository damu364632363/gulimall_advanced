package io.renren.damu.jvm.HeapDemo.jvm;

public class config {

    public static void main(String[] args) {  //查看CUP核数
        System.out.println(Runtime.getRuntime().availableProcessors());
        //返回Java虚拟机试图使用的最大内存量。-Xmx
        long maxMemory = Runtime.getRuntime().maxMemory();
        //返回Java虚拟机中的内存总量。-Xms
        long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("MAX_MEMORY=" + maxMemory + " (字节) " + (maxMemory / (double) 1024 / 1024) + "MB");
        System.out.println("TOTAL_MEMORY=" + totalMemory + "(字节)" + (totalMemory / (double) 1024 / 1024) + "MB");
    }
}
