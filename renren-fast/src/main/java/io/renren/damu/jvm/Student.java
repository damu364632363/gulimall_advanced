package io.renren.damu.jvm;

class Student {
    Student() {
        System.out.println("Student-Constructor");
    }

    {
        System.out.println("Student 构造块");
    }

    static {
        System.out.println("Student");
    }
}

 class Test3 {
    {
        System.out.println("Test3 构造块");
    }

    static {
        System.out.println("Test3");
    }

    Test3() {
        System.out.println("Test3-Constructor");
    }

    public static void main(String[] args) {
        System.out.println("启动");
        new Student();
        System.out.println("-------我是1分界线---------");
        new Student();
        System.out.println("-------我是2分界线---------");
        new Test3();
    }
}
/**
 * JVM规定：静态>构造块>构造函数
 * 输出结果：Test3启动StudentStudent 构造块Student-Constructor
 * -------我是1分界线---------Student 构造块Student-Constructor
 * -------我是2分界线---------Test3 构造块Test3-Constructor Process finished with exit code 0
 */
