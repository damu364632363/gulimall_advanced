package io.renren.damu.jvm;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Random;

/**
 * 默认情况新生代老年代内存占比1:2
 * -Xms600m -Xmx600m  -XX:+PrintGCDetails
 * -XX:+PrintGCDetails(查看垃圾回收细节)
 * -XX:SurvivorRatio 调整比例 例如-XX:SurvivorRatio=8   eden空间和另外两个survivor空间缺省默认所占比例是6:1:1
 * -Xms600m 堆最小大小600m
 * -Xxs  用来设置堆空间（年轻代+老年代）最大内存大小
 */
public class OOMTest {
//    public static void main(String[] args) {

//        ArrayList list = new ArrayList<>();
//        while (true) {
//            try {
//                Thread.sleep(25);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            list.add(new Picture(new Random().nextInt( 128 * 1024)));
//        }
//    }
//
//    static class Picture {
//        private byte[] pixels;
//
//        public Picture(int length) {
//            this.pixels = new byte[length];
//        }
//    }

    public static void main(String[] args) {
//        String url = "E:/upload/c57baa56-49cc-476b-b4be-68faadbce14ey.jpg";
//        String m = "https://xmnpsys.kerryeas.com:8057/images/kerry/gdc/6e5aa024dfd84ca2bbea1f7a30f1f32d.jpg";
//        System.out.println(m.length());
//        String substring = url.substring(url.lastIndexOf("/") + 1);
//        System.out.println(url);
//        System.out.println(substring);
        BigDecimal bigDecimal = new BigDecimal("");
//        Object.
//        System.out.println("bigDecimal>>>" + bigDecimal);
    }
}

//
//        'KXMSC220614001-0',
//        'GIT100000474502-0',
//        'GIT100000474124-0',
//        'GIT100000473651-0',
//        'GIT100000473399-0',
//        'KXMSC220424001-0',
//        'KSZSC220311002-0',
//        'KSZSC220311001-0',
//        'KSZSC220303001-0',
//        'KSZSC220121001-2',
//        'KSZSC220121001-1',
//        'KSZSC220121001-0',
//        'KSZSC220217003-0',
//        'KSZSC220217001-0',
//        'IQNPI210818016-0',

