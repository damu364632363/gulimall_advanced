package io.renren.damu.lock.ReEntry;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ReentryLockDemo {
    static Object objectLock = new Object();
//    static Object objectLock2 = new Object();

    public static void main(String[] args) {
        Lock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        new Thread(() ->{
            lock.lock();
            try {
                System.out.println(Thread.currentThread().getName() + "\t" + "外层");
                lock.lock();
                try {
                    System.out.println(Thread.currentThread().getName() + "\t" + "中层");
                } finally {
//                    lock.unlock();
                }
            } finally {
                lock.unlock();
            }
        },"t1").start();

        new Thread(() ->{
            lock.lock();
            try {
                System.out.println(Thread.currentThread().getName() + "\t" + "外层");
                lock.lock();
            } finally {
                lock.unlock();
            }
        },"t2").start();


    }
    public synchronized void m1(){
        System.out.println("......m1");
        m2();
    }
    public synchronized void m2(){
        System.out.println("......m2");
        m3();
    }
    public synchronized void m3(){
        System.out.println("......m3");
    }

    private static void lockDemo1() {
        new Thread(()->{
            synchronized (objectLock){
                System.out.println("外层");
                synchronized (objectLock){
                    System.out.println("中层");
                    synchronized (objectLock){
                        System.out.println("内层");
                    }
                }
            }
        },"a").start();
    }

}
