package io.renren.damu.lock.deadLock;

import java.util.concurrent.TimeUnit;

public class DeadLockDemo {
    static Object lockA = new Object();
    static Object lockB = new Object();

    public static void main(String[] args) {
        new Thread(() -> {
            synchronized (lockA) {
                System.out.println(Thread.currentThread().getName() + "\t" + "持有A，期待b锁");
//                try {
//                    TimeUnit.SECONDS.sleep(1);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                synchronized (lockB) {
                    System.out.println(Thread.currentThread().getName() + "\t" + "获得B锁成功");
                }
            }
        }, "a").start();
//        try {
//            TimeUnit.SECONDS.sleep(1);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        new Thread(() -> {
            synchronized (lockB) {
                System.out.println(Thread.currentThread().getName() + "\t" + "持有B，期待a锁");
//                try {
//                    TimeUnit.SECONDS.sleep(1);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                synchronized (lockA) {
                    System.out.println(Thread.currentThread().getName() + "\t" + "获得A锁成功");
                }
            }
        }, "b").start();
    }

}
