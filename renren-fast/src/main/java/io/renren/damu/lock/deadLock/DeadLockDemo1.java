package io.renren.damu.lock.deadLock;

import java.util.concurrent.TimeUnit;

public class DeadLockDemo1 {
    static Object o1 = new Object();
    static Object o2 = new Object();
    public static void main(String[] args) {

        new Thread(()->{
            synchronized (o1){
                System.out.println(Thread.currentThread().getName() + "获取o1");
//                try {
//                    TimeUnit.SECONDS.sleep(3);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                synchronized (o2){
                    System.out.println(Thread.currentThread().getName() + "获取到o2");
                }
            }
        },"a").start();

        new Thread(()->{
            synchronized (o2){
                System.out.println(Thread.currentThread().getName() + "获取o2");
//                try {
//                    TimeUnit.SECONDS.sleep(3);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                synchronized (o1){
                    System.out.println(Thread.currentThread().getName() + "获取到o1");
                }
            }
        },"b").start();


    }
}
