package io.renren.damu.lock;

import java.util.concurrent.TimeUnit;

class Phone
{
    public static synchronized void sendEmail(){
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("...sendEmail");
    }
    public synchronized void sendSMS(){
        System.out.println(">>>>>>>sendSMS");
    }
    public void hello(){
        System.out.println(">>>>hello>>>>>");
    }
}

/**
 * 谈谈你对多线程锁的了解
 * 1、标准访问ab两个线程，先打印哪一个
 * 2、sendEmail方法暂停3秒钟，请问先打印邮件还是短信
 * 3、新增一个普通的hello方法，请问先打印邮件，还是hello
 * 4、两部手机，先打印邮件还是打印短信
 * 5、两个静态同步方法，同一部手机 先打印邮件还是短信
 * 6、两个静态同步方法，两部手机 先打印邮件还是短信
 * 7、一个静态同步方法，一个普通同步方法，同一部手机 先打印邮件还是短信
 * 8、一个静态同步方法，一个普通同步方法，两部手机 先打印邮件还是短信
 */

/**
 *  1-2
 *  一个对象里面如果有多个synchronized 方法，某一个时间内，只要一个线程去调用其中的一个synchronized方法
 *  其他的线程都只能等待，换句话说，某一个时刻内，只能有唯一的一个线程去访问这些synchronized方法。
 *  锁的是当前对象，this，被锁定后，其他的线程都不能进入到当前对象的其他synchronized方法。
 * 3-4
 * 价格普通方法后，和同步锁无关
 * 换成两个对象后，不是同一把锁了。
 * 5-6
 * 3种synchronized锁的内容有一些区别；
 * 对于普通同步方法，锁的是当前实例对象，通常指this，具体的一部部手机，所有的普通同步方法用的都是同一把锁————实例对象本身
 * 对于静态同步方法，锁的是当前的class对象，如果Phone.class唯一的一个模板
 * 对于同步方法块，锁的是synchronized括号内的对象
 *
 * 7-8
 * 当一个线程试图访问同步代码时，它首先必须得到锁，退出或抛出异常时必须释放锁。
 *
 * 所有的普通同步方法用的都是同一把锁----实例对象本身。就是new出来的具体对象本身，本类this
 * 也就是说，如果一个实例对象的普通同步方法获取锁后，该实例对象的其他普通同步方法必须等待获取锁的方法释放锁后才能获取锁。
 *
 * 所有的静态同步方法用的也是同一把锁————类对象本身，就是我们说过的唯一模板class
 * 具体实例对象this和唯一模板class，这两把锁是两个不同的对象，所以静态同步方法与普通同步方法之间是不会有竞争的。一个是类锁，一个是对象锁
 * 但是一旦一个静态同步方法获取锁后，其他的静态同步方法都必须等待该方法释放后才能获取锁
 *
 */
public class Lock8Demo {

    public static void main(String[] args) {
        Phone phone1 = new Phone();
        Phone phone2 = new Phone();
        new Thread(()->{
            phone1.sendEmail();
        },"a").start();

        new Thread(()->{
//            phone2.sendSMS();
//            phone1.hello();
            phone1.sendSMS();
        },"b").start();
    }

}
