package io.renren.damu.lock;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class MyResource{
    Map<String,String> map = new HashMap<>();
    Lock lock = new ReentrantLock();
    ReentrantReadWriteLock rwLock = new ReentrantReadWriteLock();
    public void write(String key, String value){
        lock.lock();
        try{
            System.out.println(Thread.currentThread().getName() + "正在写入");
            map.put(key,value);
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "写入结束");
        }finally {
            lock.unlock();
        }
    }

    public void read(String key){
        lock.lock();
        try{
            System.out.println(Thread.currentThread().getName() + "正在读取");
            String result = map.get(key);
            try {
                TimeUnit.MILLISECONDS.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "完成读取" + result);
        }finally {
            lock.unlock();
        }
    }
}

public class ReentrantReadWriteLockDemo {
    public static void main(String[] args) {
        MyResource myResource = new MyResource();
        for (int i = 0; i < 10; i++) {
            int fi = i;
            new Thread(()->{
                myResource.write(fi + "", fi + "");
            },String.valueOf(i)).start();
        }
    }
}
