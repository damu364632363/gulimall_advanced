package io.renren.damu.jvmDC;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

public class T1 {

    private static List<Integer> list1 = new ArrayList<>();
    private static List<Integer> list2 = new ArrayList<>();
    private static List<Integer> list3 = new ArrayList<>();
    private static Lock lock = new ReentrantLock();

    public static void main(String[] args) {
        IntStream.range(0,10000).forEach(list1::add); //串行流
        IntStream.range(0,10000).parallel().forEach((value) ->
            list2.add(value));//并行流，线程安全问题
        IntStream.range(0,10000).forEach((i)->{
            lock.lock();
            try {
                list3.add(i);
            } finally {
                lock.unlock();
            }
        });

        System.out.println(list1.size());
        System.out.println(list2.size());
        System.out.println(list3.size());

    }


}
