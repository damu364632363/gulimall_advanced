package io.renren.damu.ms.juc;

import java.text.SimpleDateFormat;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class SemaphoreDemo {
    //    public static void main(String[] args) {
//        //Semaphore 信号量
//        Semaphore semaphore = new Semaphore(3);//模拟3个停车位
//        for (int i = 0; i < 6; i++) { //模拟6部汽车
//            new Thread(()->{
//                try {
//                    semaphore.acquire();
//                    System.out.println(Thread.currentThread().getName()+"\t 抢到车位");
//                    TimeUnit.SECONDS.sleep(3);
//                    System.out.println(Thread.currentThread().getName() + "\t 停车3秒后离开车位");
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }finally {
//                    semaphore.release();
//                }
//            },String.valueOf(i)).start();
//        }
//    }
    public static void main(String[] args) throws Exception {
        String time = "20101125102503";
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-HH-dd HH:mm:ss");
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyyHHddHHmmss");
        time = formatter1.format(formatter2.parse(time));
        System.out.println(time);
    }

}
