package io.renren.damu.ms.jmm;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * ABA问题的解决
 */
public class ABADemo {
    static AtomicReference<Integer> atomicReference = new AtomicReference<>(100);
    static AtomicStampedReference<Integer> atomicStampedReference = new AtomicStampedReference<>(100,1);

    public static void main(String[] args) {
        System.out.println("++++++++++++以下是ABA问题的产生+++++++++++");
        new Thread(() -> {
//            atomicReference.compareAndSet(100, 1006);
//            atomicReference.compareAndSet(1006, 100);
            //-128-127这个区间是有int缓存的是同一个东西，但你值大一点就没缓存了
            System.out.println(">>>" + atomicReference.compareAndSet(100, 129));
            System.out.println("<<<<<<<<<<" + atomicReference.get());

            System.out.println(">>>" + atomicReference.compareAndSet(129, 100));
            System.out.println("<<<<<<<<<<" + atomicReference.get());
        }, "t1").start();

//        new Thread(() -> {
//            //暂停一下，保证t1先执行
//            try {
//                TimeUnit.SECONDS.sleep(1);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            System.out.println(atomicReference.compareAndSet(100, 2019) + " " + atomicReference.get());
//        }, "t2").start();

        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("++++++++++++以下是ABA问题的解决+++++++++++");

        new Thread(()->{
            int stamp = atomicStampedReference.getStamp();
            System.out.println(Thread.currentThread().getName()+"\t第一版本号" + stamp);
            try {
                TimeUnit.SECONDS.sleep(1);
                atomicStampedReference.compareAndSet(100,101,atomicStampedReference.getStamp(),atomicStampedReference.getStamp()+1);
                System.out.println(Thread.currentThread().getName()+"\t第2版本号" + atomicStampedReference.getStamp());
                atomicStampedReference.compareAndSet(101,100,atomicStampedReference.getStamp(),atomicStampedReference.getStamp()+1);
                System.out.println(Thread.currentThread().getName()+"\t第3版本号" + atomicStampedReference.getStamp());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"t3").start();


        new Thread(()->{
            int stamp = atomicStampedReference.getStamp();
            System.out.println(Thread.currentThread().getName()+"\t第一版本号" + stamp);
            try {
                //保证上面的t3线程完成一次aba操作
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            boolean result = atomicStampedReference.compareAndSet(100,2022,stamp,stamp+1);
            System.out.println(Thread.currentThread().getName()+"是否修改成功" + result + " 当前最新实际版本号" + atomicStampedReference.getStamp());
            System.out.println(Thread.currentThread().getName()+"当前实际最新值" + atomicStampedReference.getReference());
        },"t4").start();
    }
}
