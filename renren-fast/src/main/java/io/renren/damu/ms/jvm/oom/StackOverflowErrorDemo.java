package io.renren.damu.ms.jvm.oom;

import lombok.extern.slf4j.Slf4j;

/**
 * -XX:+PrintCommandLineFlags -version 打印默认收集器
 * 并行回收 -XX:+UseParallelGC
 * 串行回收 -XX:+UseSerialGC
 */
@Slf4j
public class StackOverflowErrorDemo {
    public static void main(String[] args) {
        try {
            StackOverflowError();
        } catch (Exception e) {
            log.info(">>>>>>>>>>");
            e.printStackTrace();
        }
    }
    private static void StackOverflowError(){
        StackOverflowError(); //Exception in thread "main" java.lang.StackOverflowError
    }
}
