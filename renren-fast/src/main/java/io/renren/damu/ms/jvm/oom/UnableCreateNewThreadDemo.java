package io.renren.damu.ms.jvm.oom;


public class UnableCreateNewThreadDemo {
    public static void main(String[] args) {
        for (int i = 0; i < 5000000; i++) {
            System.out.println(">>>>>>>>>>>> i=" + i);
            new Thread(()->{
                try {
                    Thread.sleep(Integer.MAX_VALUE);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            },"" + i).start();
        }
    }
}
