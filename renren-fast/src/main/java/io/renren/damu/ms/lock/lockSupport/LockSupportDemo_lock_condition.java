package io.renren.damu.ms.lock.lockSupport;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockSupportDemo_lock_condition {

    static Object objectLock = new Object();
    static Lock lock = new ReentrantLock();
    static Condition condition = lock.newCondition();

    public static void main(String[] args) {
        LockSupportDemo_lock_condition lockSupportDemo_lock_condition = new LockSupportDemo_lock_condition();
//        lockSupportDemo_lock_condition.withoutLock();
        lockSupportDemo_lock_condition.signalAwait();
    }

    public void withLock(){
        new Thread(() -> {
            lock.lock();
            try {
                System.out.println(Thread.currentThread().getName() + "come in");
                try {
                    condition.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "唤醒");
            } finally {
                lock.unlock();
            }
        }, "AAA").start();

        new Thread(()->{
            lock.lock();
            try {
                condition.signal();
                System.out.println(Thread.currentThread().getName() + "通知");
            } finally {
                lock.unlock();
            }
        },"BBB").start();
    }


    public void withoutLock(){
        new Thread(() -> {
//            lock.lock();
            try {
                System.out.println(Thread.currentThread().getName() + "come in");
                try {
                    condition.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "唤醒");
            } finally {
//                lock.unlock();
            }
        }, "AAA").start();

        new Thread(()->{
//            lock.lock();
            try {
                condition.signal();
                System.out.println(Thread.currentThread().getName() + "通知");
            } finally {
//                lock.unlock();
            }
        },"BBB").start();
    }

    public void signalAwait(){
        new Thread(() -> {
            try {
                Thread.sleep(300L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            lock.lock();
            try {
                System.out.println(Thread.currentThread().getName() + "comein");
                try {
                    condition.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "唤醒");
            } finally {
                lock.unlock();
            }
        }, "AAA").start();

        new Thread(()->{
            lock.lock();
            try {
                condition.signal();
                System.out.println(Thread.currentThread().getName() + "通知 ");
            } finally {
                lock.unlock();
            }
        },"BBB").start();
    }
}
