package io.renren.damu.ms.cas;

import java.util.concurrent.atomic.AtomicInteger;

public class casDemo {
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(5);
//        atomicInteger.getAndIncrement();
        boolean b = atomicInteger.compareAndSet(5, 2022);
        System.out.println("current data:" + atomicInteger.get() + "  b:" + b);

        System.out.println(atomicInteger.compareAndSet(5, 2022) + " " + "current data:" + atomicInteger.get());
        atomicInteger.getAndIncrement();
    }
}
