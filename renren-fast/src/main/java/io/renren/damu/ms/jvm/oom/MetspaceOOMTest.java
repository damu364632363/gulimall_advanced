package io.renren.damu.ms.jvm.oom;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * -XX:MetaspaceSize=8m -XX:MaxMetaspaceSize=8m
 *
 * 元空間存放了一下信息
 * 虚拟机加载的类信息
 * 常量池
 * 静态变量
 * 即时编译后的代码
 */
public class MetspaceOOMTest {
    static class OOMTest{

    }
    public static void main(String[] args) {
//        System.out.println("hello");
        int i = 0;

        try {
            while (true){
                i++;
                Enhancer enhancer = new Enhancer();
                enhancer.setSuperclass(OOMTest.class);
                enhancer.setUseCache(false);
                enhancer.setCallback(new MethodInterceptor() {
                    @Override
                    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                        return methodProxy.invokeSuper(o,args);
                    }
                });
                enhancer.create();
            }
        } catch (Throwable e) {
            System.out.println(">>>>>>>>>>>>>>多少次后发送了异常" +i);
            e.printStackTrace();
        }
    }
}
