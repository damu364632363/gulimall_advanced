package io.renren.damu.ms.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class ListNotSafe {
    public static void main(String[] args) {
//        List<String> list = new ArrayList<>();
//        for (int i = 0; i < 5; i++) {
//            new Thread(() -> {
//                list.add("99");
//                System.out.println(list);
//            }, String.valueOf(i)).start();
//        }
        List<String> list = Collections.synchronizedList(new ArrayList<>());
        for (int i = 0;i<20;i++){
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,8));
                System.out.println(Thread.currentThread().getName()+"\t"+list);
            }).start();
        }
//        List<String> list = new ArrayList<>();
//        for (int i = 0; i < 50; i++) {
//            new Thread(()->{
//                list.add(String.valueOf("you"));
//            },String.valueOf(i)).start();
//            System.out.println(">>>>" + list.size());
//        }
//        System.out.println(">>>UOIO>" + list.size());
//
//        try {
//            Thread.sleep(6000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        System.out.println(">>>>" + list.size());

    }
}
