package io.renren.damu.ms.jvm;

import java.net.URL;
import java.net.URLClassLoader;

/**
 * 类加载器
 * 通俗易懂 启动类加载器、扩展类加载器、应用类加载器通俗易懂
 * 启动类加载器、扩展类加载器、应用类加载器
 * https://zhuanlan.zhihu.com/p/73359363
 */
public class MyObject {
    public static void main(String[] args) {
        Object o = new Object();
        System.out.println(o.getClass().getClassLoader());

        MyObject myObject = new MyObject();
        System.out.println(myObject.getClass().getClassLoader().getParent().getParent());
        //扩展类加载器
        System.out.println(myObject.getClass().getClassLoader().getParent());
        //应用类加载器
        System.out.println(myObject.getClass().getClassLoader());

        System.out.println("打印启动类加载器BootstrapClassLoader的加载路径>>>>>>>>>>>>>>>>");

        //启动类加载器的加载路劲
        URL[] urls = sun.misc.Launcher.getBootstrapClassPath().getURLs();
        for (URL url : urls) {
            System.out.println(url);
        }

        System.out.println("打印一下扩展类加载器ExtentionClassLoader的加载路径>>>>>>>>>>>>>>>>");
        //扩展类加载器的加载路劲
        URL[] extentionClassLoaderUrls = ((URLClassLoader) ClassLoader.getSystemClassLoader().getParent()).getURLs();
        for (URL url : extentionClassLoaderUrls) {
            System.out.println(url);
        }

        System.out.println("");
        System.out.println("打印一下AppClassLoader的加载路径>>>>>>>>>>>>>>>>");
        URL[] appClassLoaderUrls = ((URLClassLoader) ClassLoader.getSystemClassLoader()).getURLs();
        for (URL url : appClassLoaderUrls) {
            System.out.println(url);
        }
    }
}
