package io.renren.damu.ms.lock.lockSupport;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

public class LockSupportDemo {

    public static void main(String[] args) {
//        locksupportUsed_park_unpark();
        locksupportUsed_unpark_park();
    }

    private static void locksupportUsed_park_unpark() {
        Thread a =new Thread(()->{
            System.out.println(Thread.currentThread().getName() + "come in");
            LockSupport.park(); //被阻塞。。。等待通知等待放行，它要通过需要许可证
            System.out.println(Thread.currentThread().getName() + "被唤醒");
        },"AAA");
        a.start();

        try {
            TimeUnit.SECONDS.sleep(3L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Thread b =new Thread(()->{

            LockSupport.unpark(a);
            System.out.println(Thread.currentThread().getName() + "通知");
        },"BBB");
        b.start();
    }

    private static void locksupportUsed_unpark_park() {
        Thread a =new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(3L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(Thread.currentThread().getName() + "come in");
            LockSupport.park(); //被阻塞。。。等待通知等待放行，它要通过需要许可证
            System.out.println(Thread.currentThread().getName() + "被唤醒");
        },"AAA");
        a.start();



        Thread b =new Thread(()->{

            LockSupport.unpark(a);
            System.out.println(Thread.currentThread().getName() + "通知");
        },"BBB");
        b.start();
    }
}
