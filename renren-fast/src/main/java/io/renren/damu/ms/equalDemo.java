package io.renren.damu.ms;

import java.util.HashSet;
import java.util.Set;

public class equalDemo {

    public static void main(String[] args) {
        String s1 = new String("abc");
        String s2 = new String("abc");
        System.out.println(s1 == s2);
        System.out.println(s1.equals(s2));
        Set<String> hashSet = new HashSet<>();
        hashSet.add(s1);
        hashSet.add(s2);
        //hashSet.add(),底层是hashmap.put()
        //hashmap.put()是会对key进行求hashCode，如果hashCode是一致，则会被覆盖
        System.out.println("+++++++++++++++++++++++s1.hashCode():" + s1.hashCode());
        System.out.println("+++++++++++++++++++++++s2.hashCode():" + s2.hashCode());
        System.out.println(hashSet.size());
        System.out.println("+++++++++++++++++++++++");

        System.out.println("+++++++++++++++++++++++");
        Person p1 = new Person("abc");
        Person p2 = new Person("abc");
        System.out.println(p1 == p2);
        System.out.println(p1.equals(p2));
        Set<Person> set02 = new HashSet<>();
        set02.add(p1);
        set02.add(p2);
        System.out.println("+++++++++++++++++++++++p1.hashCode():" + p1.hashCode());
        System.out.println("+++++++++++++++++++++++p2.hashCode():" + p2.hashCode());
        //todo ，注意这边person用@Getter，@Setter和用@Data是不一样的
        System.out.println(set02.size());


    }
}
