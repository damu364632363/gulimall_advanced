package io.renren.damu.ms.jmm;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

class MyData {
    //volatile保证可见性，不保证原子性。
    //可见性：添加了volatile，可以解决可见性问题(其他线程可以感知volatile修饰的变量的及时变化）
    volatile int number = 0;

    //        int number = 0;
    public void addTo() {
        this.number = 60;
    }

    //此时number前面是添加修饰符
    //number ++在多线程下是非线程安全的，为何不加synchronized解决
    public void addPlusPlus() {
        number++;
    }

    //juc接口中存在指定数据类型对应原子性的类
    AtomicInteger atomicInteger = new AtomicInteger();

    public void addAtomic() {
        atomicInteger.getAndIncrement();
    }

//    new AtomicDouble, new AtomicLong,,,,,,
}

/**
 * 验证volatile的可见性
 * 1.1 假如int number =0;number变量之前根本没有添加volatile关键字修饰
 * 1.2添加了volatile，可以解决可见性问题(其他线程可以感知volatile修饰的变量的及时变化）
 * <p>
 * 验证volatile不保证原子性
 * 原子性指的是什么：不可分割，完整性，也即某个线程正在做某个具体业务时，中间不可以被加塞或者被分割。需要整体完整
 * 要么同时成功，要么同时失败。
 * volatile不保证原子性的案例演示（类似多线程调用数值，没有synchronized的情况）
 * why
 * 如何解决原子性
 * 1，加synchronized
 * 2,使用juc下的atomicInteger
 * 添加volatile禁止指令重排
 */
public class VolatileDemo {

    public static void main(String[] args) {

//        volatileKeJianXing();
//        seeIntegerNoVolatile();
//        volatileNoActomic();
        ActomicInteger();
//        System.out.println(Thread.currentThread().getName() + "\t 调用addAtomic后的值 value:" + myData.atomicInteger);
    }

    /**
     * volatile并不保证可见性，
     */
    private static void volatileNoActomic() {
        MyData myData = new MyData();
        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000; j++) {
                    myData.addPlusPlus();
//                    myData.addAtomic();
                }
            }, String.valueOf(i)).start();
        }
        //需要等待上面20个线程都计算完成后，再用main线程取得最终的结果值。
        //暂停一会线程
//        try {
//            TimeUnit.SECONDS.sleep(5);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        //后台默认有2个线程，一个是main线程，一个是gc线程
        while (Thread.activeCount() > 2) {
            Thread.yield();
        }
        System.out.println(Thread.currentThread().getName() + "\t finally number value:" + myData.number);
    }

    //volatile可以保证可见性，及时通知其他线程，主物理内存的值已经被修改。
    //如果不添加volatile，则主线程判断不出myData.number的变化
    private static void volatileKeJianXing() {
        MyData myData = new MyData(); //资源类
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "\t come in");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            myData.addTo();
            System.out.println(Thread.currentThread().getName() + "\t update number value" + myData.number);
        }, "A").start();

        while (myData.number == 0) {
//            System.out.println("ka");
            //main线程就一直在这边等待循环，直到num不等于0
        }
        System.out.println(Thread.currentThread().getName() + "\t misson is over,main get number value:" + myData.number);
    }

    private static void seeIntegerNoVolatile() {
        MyData myData = new MyData(); //资源类
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "\t dsds");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            myData.addAtomic();
            System.out.println(Thread.currentThread().getName() + "\t update value " + myData.atomicInteger);
            if (myData.atomicInteger.get() == 1) {
                System.out.println("myData.atomicInteger.equals(1)");
            }
        }, "A").start();

        while (myData.atomicInteger.get() == 0) {
            //main线程就一直在这边等待循环，直到num不等于0
        }
        System.out.println(Thread.currentThread().getName() + "\t misson is over,main get number value:" + myData.atomicInteger);
    }

    /**
     * 原子性解决 num++问题
     */
    private static void ActomicInteger() {
        MyData myData = new MyData();
        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000; j++) {
//                    myData.addPlusPlus();
                    myData.addAtomic();
                }
            }, String.valueOf(i)).start();
        }
        //需要等待上面20个线程都计算完成后，再用main线程取得最终的结果值。
        //暂停一会线程
//        try {
//            TimeUnit.SECONDS.sleep(5);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        //后台默认有2个线程，一个是main线程，一个是gc线程
        while (Thread.activeCount() > 2) {
            Thread.yield();
        }
        System.out.println(Thread.currentThread().getName() + "\t finally number value:" + myData.atomicInteger);
    }
}
