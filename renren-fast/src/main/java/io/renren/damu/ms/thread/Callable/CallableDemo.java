package io.renren.damu.ms.thread.Callable;

import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

class MyThread1 implements Runnable {

    @Override
    public void run() {

    }
}

//class MyThread implements Callable<Integer> {
//
//    @Override
//    public Integer call() throws Exception {
//        System.out.println("Come in the call method");
//        return 1024;
//    }
//}

class MyThread implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        System.out.println(">>>>>>");
        Thread.sleep(3000);
        return 9898;
    }
}

class MyThread2 implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        System.out.println(">>>>>>");
        Thread.sleep(3000);
        return 9898;
    }
}

public class CallableDemo {


    //    public static void main(String[] args) throws ExecutionException, InterruptedException {
//        FutureTask<Integer> futureTask = new FutureTask(new MyThread());
//        new Thread(futureTask, "A").start();
//        Integer result = futureTask.get();
//        System.out.println(result);
//    }
    public static void main(String[] args) throws Exception{
//        FutureTask<Integer> integerFutureTask = new FutureTask<>(new MyThread());
////        new Thread(integerFutureTask).start();
//        new Thread(integerFutureTask, "A").start();
//        Integer integer = integerFutureTask.get();
//        System.out.println(integer);
        long startTime = System.currentTimeMillis();

        FutureTask<Integer> futureTask = new FutureTask<>(new MyThread());
        new Thread(futureTask).start();
        FutureTask<Integer> futureTask2 = new FutureTask<>(new MyThread2());
        new Thread(futureTask2).start();

        System.out.println("futureTask.isDone()?????????" + futureTask.isDone());
//        while (!futureTask.isDone()){
//            System.out.println("futureTask计算完成");
//        }


        Integer integer = futureTask.get();//

        System.out.println("integer:" + integer);

        System.out.println("futureTask.isDone()" + futureTask.isDone());
        Integer integer2 = futureTask2.get();
        System.out.println("integer2:" + integer2);
//        System.out.println(">>>" + integer2 + integer);

        long times = System.currentTimeMillis() - startTime;
        System.out.println("time:" +times);
    }
}
