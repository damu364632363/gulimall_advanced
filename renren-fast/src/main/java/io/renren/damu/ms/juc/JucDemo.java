package io.renren.damu.ms.juc;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

public class JucDemo {

    public static void main(String[] args) {
//        countDownLatchMethod();
        cyclicBarrierMethod();
    }

    private static void cyclicBarrierMethod() {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(10,() ->{
            System.out.println("执行完毕");
        });
        for (int i = 0; i < 10; i++) {
            try {
                final int itm = i;
                new Thread(()->{
                    try {
                        System.out.println(Thread.currentThread().getName() + "\t 收集到第" + itm + "龙珠");
                        cyclicBarrier.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (BrokenBarrierException e) {
                        e.printStackTrace();
                    }
                },String.valueOf(i)).start();
//                Thread.sleep(100);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void countDownLatchMethod() {
        CountDownLatch countDownLatch = new CountDownLatch(10);
        for (int i = 0; i < 10; i++) {
            try {
                new Thread(()->{
                    countDownLatch.countDown();
                    System.out.println(Thread.currentThread().getName() + "\t当前线程。 执行了");
                },String.valueOf(i)).start();
//                Thread.sleep(100);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("活已经干完了，准备回家");
    }
}
