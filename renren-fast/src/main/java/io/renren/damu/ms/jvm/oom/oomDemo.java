package io.renren.damu.ms.jvm.oom;

import java.util.ArrayList;
import java.util.List;

public class oomDemo {
    public static void main(String[] args) {
        doOom();
    }
    private static void doOom() {
        List<byte[]> list = new ArrayList<>();
        while (true) {
            list.add(new byte[1024 * 1024 * 4]);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
