package io.renren.damu.ms.queue;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class Source {
    int initData = 0;

    public void increase() {
        ++initData;
    }

    public void decrease() {
        --initData;
    }
}

/**
 * 一个初始值为零的变量，两个线程对其交替操作，一个加1一个减1
 * 1 线程  操作（方法） 资源类
 * 2 判断  干活        通知
 * 3 防止虚假唤醒机制
 */
public class ProdConsumer_TraditionDemoCopy {

    public static void main(String[] args) throws Exception {
        Lock lock = new ReentrantLock();
        Condition condition = lock.newCondition();
        Source source = new Source();

        new Thread(() -> {
            for (int i = 0; i < 5; i++) {
                lock.lock();
                try {
                    while (source.initData != 0) {
                        condition.await();
                    }
                    source.increase();
                    System.out.println("increase后的值：" + source.initData);
                    condition.signalAll();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    lock.unlock();
                }
            }
        }, "AA").start();
        new Thread(() -> {
            for (int i = 0; i < 12; i++) {
                lock.lock();
                try {
                    while (source.initData == 0) {
                        condition.await();
                    }
                    source.decrease();
                    System.out.println("decrease后的值：" + source.initData);
                    condition.signalAll();
                } catch (Exception e) {
                    e.printStackTrace();
                }finally {
                    lock.unlock();
                }
            }
        }, "BB").start();
    }
}
