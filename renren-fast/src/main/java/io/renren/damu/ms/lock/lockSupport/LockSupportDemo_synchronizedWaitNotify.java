package io.renren.damu.ms.lock.lockSupport;


/**
 * 3种让线程等待和唤醒的方法
 */
public class LockSupportDemo_synchronizedWaitNotify {



    static Object objectLock = new Object();
    public static void main(String[] args) {
        LockSupportDemo_synchronizedWaitNotify LockSupportDemo_synchronizedWaitNotify = new LockSupportDemo_synchronizedWaitNotify();
//        LockSupportDemo_synchronizedWaitNotify.waitNotify();
//        LockSupportDemo_synchronizedWaitNotify.waitNotifyWithoutSynchronized();
        LockSupportDemo_synchronizedWaitNotify.notifyWait();
    }


    /**
     * AAA come in
     BBB 通知
     AAA-------被唤醒
     */
    public void waitNotify(){
        new Thread(()->{
            synchronized (objectLock){
                System.out.println(Thread.currentThread().getName() + " come in");
                try {
                    objectLock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "-------被唤醒");
            }
        },"AAA").start();

        new Thread(()->{
            synchronized (objectLock){
                objectLock.notify();
                System.out.println(Thread.currentThread().getName() + " 通知");
            }
        },"BBB").start();
    }

    /**
     * wait notify notifyAll的调用者必须是同步监视器
     */
    public void waitNotifyWithoutSynchronized(){
        new Thread(()->{
//            synchronized (objectLock){
                System.out.println(Thread.currentThread().getName() + " come in");
                try {
                    objectLock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "-------被唤醒");
//            }
        },"AAA").start();

        new Thread(()->{
//            synchronized (objectLock){
                objectLock.notify();
                System.out.println(Thread.currentThread().getName() + " 通知");
//            }
        },"BBB").start();
    }

    /**
     * 先notify再wait.   AAA线程一直wait，没被唤醒
     */
    public void notifyWait(){
        new Thread(()->{
            try {
                Thread.sleep(3000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (objectLock){
                System.out.println(Thread.currentThread().getName() + " comein");
                try {
                    objectLock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "-------被唤醒");
            }
        },"AAA").start();

        new Thread(()->{
            synchronized (objectLock){
                objectLock.notify();
                System.out.println(Thread.currentThread().getName() + " 通知");
            }
        },"BBB").start();
    }
}
