package io.renren.damu.ms.lambdaExpressDemo;

@FunctionalInterface
interface Foo {
    int add(int x, int y);

    default int mul(int x, int y) {
        return x * y;
    }
}

/**
 * 函数式编程,一定是函数式接口才能使用lambda表达式
 * 函数式接口里面不能有多个抽象方法
 * 拷贝小括号，写死右箭头， 大括号
 */
public class LambdaExpressDemo2 {
    public static void main(String[] args) {

        Foo foo = (int x, int y) -> {
            System.out.println(",lk");
            return x + y;
        };

        System.out.println(foo.add(5, 6));
        System.out.println(foo.mul(5,6));
    }
}
