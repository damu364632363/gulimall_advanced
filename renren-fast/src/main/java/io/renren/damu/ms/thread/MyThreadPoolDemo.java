package io.renren.damu.ms.thread;

import java.util.concurrent.*;

/**
 * 第四种获得java多线程的方式，线程池
 */
public class MyThreadPoolDemo {
    public static void main(String[] args) {
//        executorsDemo();

        System.out.println(Runtime.getRuntime().availableProcessors());

        ExecutorService threadPool = new ThreadPoolExecutor(
                2,
                5,
                100L,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<Runnable>(3),//等候区
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());
        //最大线程数量，maximumpoolsize + linkedBlockingDeque
        try {
            for (int i = 0; i < 18; i++) { //模拟6个顾客来办理业务，手里窗口max只有5个
                final int tmpi = i;
                threadPool.execute(() -> {
                    System.out.println(Thread.currentThread().getName() + "\t 处理业务" + "\t" + tmpi);
                    try {
                        TimeUnit.SECONDS.sleep(4);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            threadPool.shutdown();
        }

    }

    private static void executorsDemo() {
//        ExecutorService threadPool = Executors.newFixedThreadPool(5);//一池5个处理线程
//        ExecutorService threadPool = Executors.newSingleThreadExecutor();//一个线程池1个处理线程
        ExecutorService threadPool = Executors.newCachedThreadPool();//一个线程池n个线程

        try {
            for (int i = 0; i < 30; i++) {
                threadPool.execute(() -> {
                    System.out.println(Thread.currentThread().getName() + "\t 板栗业务");
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            threadPool.shutdown();
        }
    }
}
