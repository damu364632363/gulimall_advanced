package io.renren.damu.ms.cycleDependency.setinjection;

public class ClientSet {

    public static void main(String[] args) {
        ServiceAA a = new ServiceAA();
        ServiceBB b = new ServiceBB();
        a.setServiceBB(b);
        b.setServiceAA(a);
    }
}
