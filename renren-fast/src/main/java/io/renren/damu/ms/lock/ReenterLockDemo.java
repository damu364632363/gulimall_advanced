package io.renren.damu.ms.lock;

class Phone1{

    public synchronized void sendSMS() throws Exception{
        System.out.println(Thread.currentThread().getId() + "\t sendSMS()");
        sendEmail();
    }

    public synchronized void sendEmail() throws Exception{
        System.out.println(Thread.currentThread().getId() + "\t lhjlhj sendEmail()");
    }
}

public class ReenterLockDemo {
    public static void main(String[] args) {
        Phone1 phone1 = new Phone1();
        new Thread(()->{
            try {
                phone1.sendSMS();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"t1").start();

        new Thread(()->{
            try {
                phone1.sendSMS();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"t2").start();
    }
}
