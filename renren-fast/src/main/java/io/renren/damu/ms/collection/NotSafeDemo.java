package io.renren.damu.ms.collection;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.stream.Collectors;

/**
 * 故障现象,java.util.ConcurrentModificationException
 * <p>
 * 导致原因
 * 解决方法
 * List<String> list = new Vector<>();,不推荐使用，效率不高哦
 * List<String> list = Collections.synchronizedList((new ArrayList<>()));
 * new CopyOnWriteArrayList();
 * <p>
 * 优化建议
 */


public class NotSafeDemo {

    public static void main(String[] args) {
//        Map<String,Object> hashMap = new HashMap<>();
//        hashMap.put("a","d");
//        Map<String,Object> hashMap1 = new HashMap<>();
//        System.out.println(hashMap.size());
//        System.out.println(1<<4);
//        mapNotSave();

//        listNotSafe();
        setNotSafe();
    }

    private static void mapNotSave() {
        Map<String, Object> map = new ConcurrentHashMap<>();
        Map<String, Object> map1 = new HashMap<>();
        Map<String, Object> map2 = Collections.synchronizedMap(new HashMap<>());
        for (int i = 0; i <= 30; i++) {
            new Thread(() -> {
                map.put(Thread.currentThread().getName(), UUID.randomUUID().toString().substring(0, 8));
                System.out.println(map);
            }, String.valueOf(i)).start();
        }
        map.hashCode();
        map.equals(map1);

        Object o = new Object();
        o.hashCode();
        String y = "i";
        map1.put("m","m");
    }

    private static void setNotSafe() {

//        Set<String> set = new HashSet<>();
//        Set<String> set = Collections.synchronizedSet(new HashSet<>());
        Set<String> set = new CopyOnWriteArraySet<>(new HashSet<>());

        for (int i = 0; i <= 30; i++) {
            new Thread(() -> {
                set.add(UUID.randomUUID().toString().substring(0, 8));
                System.out.println(set);
            }, String.valueOf(i)).start();
        }
    }

    private static void listNotSafe() {
        //        List<String> list = new ArrayList<>();
        //1 不用vector，因为效率不高
//        List<String> list = new Vector<>();
        //2 使用collections.synchronizedList(new ArrayList<>());
        List<String> list = Collections.synchronizedList((new ArrayList<>()));
        //3 new CopyOnWriteArrayList();
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        new ConcurrentLinkedQueue<>();
        //java.util.ConcurrentModificationException
        for (int i = 0; i <= 30; i++) {
            new Thread(() -> {
                list.add(UUID.randomUUID().toString().substring(0, 8));
                copyOnWriteArrayList.add(UUID.randomUUID().toString().substring(0, 8));

                System.out.println(list);
            }, String.valueOf(i)).start();
        }
    }
}

