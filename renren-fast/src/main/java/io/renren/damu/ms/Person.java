package io.renren.damu.ms;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
//@Data
@Getter
@Setter
public class Person {
    private String name;

    private Integer id;

    public Person(String name){
        this.name = name;
    }
}
