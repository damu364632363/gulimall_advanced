package io.renren.damu.ms.jmm;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.concurrent.atomic.AtomicReference;

@Getter
@ToString
@AllArgsConstructor
class User{
    String userName;
    int age;
}

public class AtomicReferenceDemo {

    public static void main(String[] args) {
        User meili = new User("meili", 85);
        User jiayou = new User("jiayou", 98);
        //原子引用包装类
        AtomicReference<User> atomicReference = new AtomicReference<>();
        atomicReference.set(meili);
        System.out.println(atomicReference.compareAndSet(meili,jiayou) + "\t" +atomicReference.get().toString());
        System.out.println(atomicReference.compareAndSet(meili,jiayou) + "\t" +atomicReference.get().toString());

    }
}
