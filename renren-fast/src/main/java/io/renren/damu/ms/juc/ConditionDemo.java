package io.renren.damu.ms.juc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class ShareData{
    private int number =1;
    private Lock lock = new ReentrantLock();
    private Condition c1 = lock.newCondition();
    private Condition c2 = lock.newCondition();
    private Condition c3 = lock.newCondition();


    public void print5(){
        lock.lock();
        try {
            //判断
            while (number != 1){
                //wait
                c1.await();
            }
            //执行
            for (int i = 0; i <5; i++) {
                System.out.println(Thread.currentThread().getName() + "\t" + i);
            }
            //通知
            number =2;
            //如何通知第二个要执行的线程
            c2.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void print10(){
        lock.lock();
        try {
            //判断
            while (number != 2){
                //wait
                c2.await();
            }
            //执行
            for (int i = 0; i <10; i++) {
                System.out.println(Thread.currentThread().getName() + "\t" + i);
            }
            //通知
            number =3;
            //如何通知第二个要执行的线程
            c3.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void print15(){
        lock.lock();
        try {
            //判断
            while (number != 3){
                //wait
                c3.await();
            }
            //执行
            for (int i = 0; i <15; i++) {
                System.out.println(Thread.currentThread().getName() + "\t" + i);
            }
            //通知
            number =1;
            //如何通知第二个要执行的线程
            c1.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}

/**
 * 多线程之间按顺序调用，实现a,b,c
 * 三个线程启动，
 * AA打印5次，b打印10次，c打印15次
 * a打印5次，b打印10次，c打印15次
 * 10轮
 */
public class ConditionDemo {
    public static void main(String[] args) {
        ShareData shareData = new ShareData();
//        for (int i = 0; i < 10; i++) {
//
//        }
        new Thread(()->{
            for (int i = 0; i < 1; i++) {
                shareData.print5();
            }
        },"A").start();

        new Thread(()->{
            for (int i = 0; i < 1; i++) {
                shareData.print10();
            }
        },"B").start();

        new Thread(()->{
            for (int i = 0; i < 1; i++) {
                shareData.print15();
            }
        },"C").start();

    }
}
