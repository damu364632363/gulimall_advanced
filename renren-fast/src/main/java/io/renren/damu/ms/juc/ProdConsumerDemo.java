package io.renren.damu.ms.juc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class Aircondition{
    private int number =0;

    private Lock lock = new ReentrantLock();
    private Condition condition =lock.newCondition();
        public void add() throws Exception{
        //判断 干活 通知
            lock.lock();
            try {
                while(number != 0){
                    condition.await();
//                    this.wait();
                }
                number++;
                System.out.println(Thread.currentThread().getName()+"\t" + number);
//                this.notifyAll();
                condition.signalAll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }

        public void minus() throws Exception{
            try {
                while (number == 0){
//                    this.wait();
                    condition.await();
                }
                number--;
                System.out.println(Thread.currentThread().getName()+"\t" + number);
//                this.notifyAll();
                condition.signalAll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

//    public synchronized void add() throws Exception{
//        //判断 干活 通知
//        while(number != 0){
//            this.wait();
//        }
//        number++;
//        System.out.println(Thread.currentThread().getName()+"\t" + number);
//        this.notifyAll();
//
//    }
//    public synchronized void minus() throws Exception{
//        while (number == 0){
//            this.wait();
//        }
//        number--;
//        System.out.println(Thread.currentThread().getName()+"\t" + number);
//        this.notifyAll();
//    }


}

/**
 * 现在2个线程，可以操作初始值为零的一个变量
 * 实现一个线程对该变量加1，一个线程对该变量减1
 * 实现交替，10轮，变量初始值为0
 *
 * 高内聚低耦合前提下，线程操作资源类
 * 判断、干活、通知
 * 防止多线程虚假唤醒。多线程的交互判断，不能用if，只能用while
 *
 * 多线程编程+while判断，新版写法
 */
public class ProdConsumerDemo {
    public static void main(String[] args) throws Exception{
        Aircondition bl = new Aircondition();
        for (int i = 0; i <=10; i++) {
            new Thread(()->{
                try {
                    bl.add();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            },"A").start();
            new Thread(()->{
                try {
                    bl.minus();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            },"B").start();

            new Thread(()->{
                try {
                    bl.add();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            },"C").start();
            new Thread(()->{
                try {
                    bl.minus();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            },"D").start();
        }

    }
}
