package io.renren.damu.ms;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestTransferValue {
    public void changeValue1(int age){
        age = 30;
    }
    public void changeValue2(Person  person){
        person.setName("per");
    }
    public void changeValue3(String str){
        str = "XXX";
    }

    public static void main(String[] args) {
//        TestTransferValue testTransferValue = new TestTransferValue();
//        int age =20;
//        testTransferValue.changeValue1(age);
//        System.out.println("age>>>>>" + age);
//
//        Person person = new Person();
//        person.setName("abc");
//        testTransferValue.changeValue2(person);
//        System.out.println("personName:" + person.getName());
//
//        String str = "abc";
//        testTransferValue.changeValue3(str);
//        System.out.println("String>>>>" + str);

        try {
            int i = 9/0;
        } catch (Exception e) {
            e.printStackTrace();
            log.error("K " + e.getMessage(),e);
        }
//        int i = 9/0;
    }
}
