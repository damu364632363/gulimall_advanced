package io.renren.damu.ms.lock;

import java.util.concurrent.TimeUnit;

class Phone{
     public static synchronized void sendEmail()throws Exception{
         TimeUnit.SECONDS.sleep(4);
         System.out.println("sentEmail>>>>>>>>>>");
     }
    public synchronized void sendSMS()throws Exception{
        System.out.println("sendSMS>>>>>>>>>>");
    }

    public void sayHello(){
        System.out.println("sayHello>>>>>>");
    }


}

/**
 * 1标准访问，先打印邮件还是短线
 * 2暂停4秒钟在邮件方法，先打印邮件还是短信
 * 3新增普通sayhello方法，先打印邮件还是hello
 * 两部手机，请问先打印邮件还是短信
 * 5两个静态同步方法，同一部手机，先打印邮件还是短信
 * 6两个静态同步方法，两部手机，先打印邮件还是短信
 *7/  1个静态同步方法，1个普通同步方法，一部手机，先打印邮件还是短信
 *  1个静态同步方法，1个普通同步方法，2部手机，先打印邮件还是短信
 */
public class Lock8Demo05 {
    public static void main(String[] args) throws Exception{
        Phone phone = new Phone();
        Phone phone1 = new Phone();
        new Thread(()->{
            try {
                phone.sendEmail();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"A").start();

        Thread.sleep(1000);

        new Thread(()->{
            try {
                phone.sendSMS();
//                phone.sayHello();
//                phone1.sendSMS();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"B").start();
    }
}
