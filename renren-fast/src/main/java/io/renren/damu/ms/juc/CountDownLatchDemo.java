package io.renren.damu.ms.juc;

import io.renren.damu.ms.enumDemo.CountryEnum;

import java.util.concurrent.CountDownLatch;

/**
 * 需要线程完毕，才可以执行下一步
 * 控制前提完成，才能执行下一步的例子
 */
public class CountDownLatchDemo {
    
    public static void main(String[] args) throws Exception{

        CountDownLatch countDownLatch = new CountDownLatch(6);
        for (int i = 1; i <= 6; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName() + "\t 国，被灭");
                countDownLatch.countDown();
            }, CountryEnum.forEachCountryEnum(i).getRetMessage()).start();
        }
        countDownLatch.await();
        System.out.println(Thread.currentThread().getName() + "\t 》》》》》》》》》秦国统一了");

//        closeDoor();
    }

    private static void closeDoor() throws InterruptedException {
        // TODO: 2022/1/11 completableFuture和CountDownLatch比较
        CountDownLatch countDownLatch = new CountDownLatch(6);
        for (int i = 0; i < 6; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName() + "\t 上完自习，离开教室");
                countDownLatch.countDown();
            },String.valueOf(i)).start();
        }
        countDownLatch.await();
        System.out.println(Thread.currentThread().getName() + "\t 》》》》》》》》》班长最后关门走人了");
    }
}
