package io.renren.damu.ms.lock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 自旋锁
 * 自旋锁好处：循环比较获取知道成功为止，没有类似wait的阻塞
 *
 * 通过cas操作完成自旋锁，a线程先进来调用myLock方法自己持有锁5秒钟，B随后进来后发现当前有线程持有锁
 * 不是null，直到A释放锁后B随后抢到
 */
public class SpinLockDemo {
    //原子引用线程
    AtomicReference<Thread> atomicReference = new AtomicReference<>();
    public void myLock(){
        Thread thread = Thread.currentThread();
        System.out.println(Thread.currentThread().getName() + "\t come in");
        while (!atomicReference.compareAndSet(null,thread)){
//            try {
//                System.out.println("抢到线程休息5秒");
//                Thread.sleep(5);
//                atomicReference.compareAndSet(thread,null);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
        }
    }

    public void myUnlock(){
        Thread thread = Thread.currentThread();
        atomicReference.compareAndSet(thread,null);
        System.out.println(Thread.currentThread().getName() + "\t invoked myUnLock()");
    }
    public static void main(String[] args) {
        SpinLockDemo spinLockDemo = new SpinLockDemo();

        new Thread(()->{
            spinLockDemo.myLock();
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            spinLockDemo.myUnlock();

        },"A").start();

        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(()->{
            spinLockDemo.myLock();
            spinLockDemo.myUnlock();
        },"B").start();

    }
}
