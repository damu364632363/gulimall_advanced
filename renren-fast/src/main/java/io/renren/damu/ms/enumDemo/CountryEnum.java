package io.renren.damu.ms.enumDemo;

import lombok.Getter;

public enum CountryEnum {

    ONE(1, "齐"),
    TWO(2, "楚"),
    THREE(3, "燕"),
    FOUR(4, "赵"),
    FIVE(5, "魏"),
    SIX(6, "韩");

    CountryEnum(Integer recCode, String retMessage) {
        this.recCode = recCode;
        this.retMessage = retMessage;
    }

    @Getter
    private Integer recCode;
    @Getter
    private String retMessage;

    public static CountryEnum forEachCountryEnum(int index) {
        CountryEnum[] myArray = CountryEnum.values();
        for (CountryEnum element : myArray) {
            if (index == element.getRecCode()) {
                return element;
            }
        }
        return null;
    }
}
