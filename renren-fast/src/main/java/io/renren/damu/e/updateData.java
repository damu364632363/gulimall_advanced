package io.renren.damu.e;

import io.renren.util.FileUtil;
import jxl.write.WritableWorkbook;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.CellCopyPolicy;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Slf4j
public class updateData {
//    public static void main(String[] args) {
//        updateData updateData = new updateData();
//        updateData.exportQueryInventoryListReportByExcel(null);
//    }


    public static void main(String[] args) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String format = simpleDateFormat.format(new Date());
        System.out.println(">>>>>" + format);
    }

    public String exportQueryInventoryListReportByExcel(Map<String, String> paramMap) {
//        List<Map<String, Object>> list = this.getDao().queryRecInventory(paramMap, null).getList();
        List<Map<String, Object>> list = new ArrayList<>();
        //template
        String oldFile = "D:\\GDC\\InventoryListTest.xlsx";
        String filePath = "D:\\GDC\\InventoryListTest5.xlsx";

        FileUtil.copyFile(oldFile, filePath);
        FileOutputStream os = null;
        WritableWorkbook wwb = null;
        FileInputStream fs = null;
        XSSFWorkbook newWorkbook = null;
//		try {
//			os =new FileOutputStream(new File(fileName));
//			Workbook.get
//			wwb = Workbook.createWorkbook(os);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
        XSSFSheet sheet = null;
        XSSFWorkbook wb = null;
        try {
//            fs = new FileInputStream(fileName);
            FileInputStream ps = new FileInputStream(filePath);
            wb = new XSSFWorkbook(ps);
            sheet = wb.getSheetAt(0); //获取到工作表，因为一个excel可能有多个工作表
//            HSSFRow row=sheet.getRow(0);
            int hang = 0;
//            String fileNameC = "D:\\xmnwmsFiles\\keas001\\1657510157688_1660440326.xlsx";
//			ClassPathResource resource = new ClassPathResource(fileNameC);
//			InputStream in = resource.getInputStream();
//			newWorkbook = new XSSFWorkbook(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
//		XSSFSheet sheet = newWorkbook.getSheetAt(0);
//		XSSFRow row;

        XSSFRow row;
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        //获取第16行的row
        XSSFRow row16 = sheet.getRow(15);
//        XSSFCellStyle cellStyle2 = wb.createCellStyle();
        XSSFCellStyle rowStyle_16 = row16.getRowStyle();
//        cellStyle2.cloneStyleFrom(rowStyle_16);
        //获取本行高度
        short height = row16.getHeight();
        XSSFCell cell16_1 = row16.getCell(1);
        XSSFCell cell16_6 = row16.getCell(6);
        //第16行，第1列的格式。作为需要输入两行的数据模板
        XSSFCellStyle firstCellStyle = wb.createCellStyle();
        XSSFCellStyle cellStyle = cell16_1.getCellStyle();
        //换行符关键
        firstCellStyle.cloneStyleFrom(cellStyle);
        firstCellStyle.setWrapText(true);
//
//        XSSFRow row_20 = sheet.createRow(19);
//        row_20.setHeight(height);
//        row_20.setRowStyle(currentStyle);
//        XSSFRow row_21 = sheet.createRow(20);
//        row_21.setHeight(height);
//        row_21.setRowStyle(cellStyle2);


        //第16行，第6列的格式。作为黄色单元格的数据模板
        XSSFCellStyle yellowStyle = wb.createCellStyle();
        XSSFCellStyle cellStyle1 = cell16_6.getCellStyle();
        yellowStyle.cloneStyleFrom(cellStyle1);
//        currentStyleYellow.set

//        XSSFRow row_22 = sheet.createRow(21);
//        row_22.setHeight(height);
//        row_22.setRowStyle(currentStyleYellow);

        //往下移动n行
        int n = 10;
        sheet.shiftRows(19, 22, n, true, true);

//        this.moveRows(sheet, 19, 22, 26, new CellCopyPolicy(), 16, firstCellStyle, yellowStyle, height);

        this.moveRowsByShift(sheet, 19,n,firstCellStyle, yellowStyle, height);

        for (int i = 0; i < list.size(); i++) {
            row = sheet.createRow(i + 13);
            int j = 0;
            row.createCell(j++).setCellValue(String.valueOf(i + 1));
            if (null != (list.get(i).get("PROJECT_ID"))) {
                row.createCell(j++).setCellValue(list.get(i).get("PROJECT_ID").toString());
            }
            if (null != (list.get(i).get("ORDER_NUMBER"))) {
                row.createCell(j++).setCellValue(list.get(i).get("ORDER_NUMBER").toString());
            }
            row.createCell(j++).setCellValue("oasiudou");
        }
        //16,实际上是第17行
        row = sheet.getRow(16);
//        row.setRowStyle(rowStyle_16);
//        row.setHeight(height);
        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.add("单元格第1行");
        arrayList.add("单元格第2行");
//        XSSFCellStyle cs = wb.createCellStyle(); // 换行的关键，自定义单元格内容换行规则
//        cs.setWrapText(true);
        String content = String.join("\n", arrayList);
        XSSFCell cell0 = row.createCell(0);
        cell0.setCellValue(content);
        String content2 = String.join("\r\n", arrayList);
        XSSFCell cell1 = row.getCell(1);
        cell1.setCellValue(content2);
        cell1.setCellStyle(firstCellStyle);

        XSSFCell cell2 = row.getCell(2);
        cell2.setCellValue("oiuweor8ogt");
        cell2.setCellStyle(firstCellStyle);

        XSSFCell cell6 = row.getCell(6);
        cell6.setCellValue("黄色区域");
//        cell6.setCellStyle(currentStyleYellow);
        int size = sheet.getMergedRegions().size();


        //设置报关单号
        XSSFRow rowComsumerNum = sheet.getRow(2);
        XSSFCell cell_customer = rowComsumerNum.createCell(5);
        cell_customer.setCellValue("1111678694FHSf");

        try {
            out.flush();
            wb.write(out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return filePath;
    }


    //模板中第20-23页末标签，一旦数据量大的情况，需要将这个页末标签下移，数据量多出几行，标签下移几行，
    //标签下移后，需要将中间的空白填补成和上面数据区一样的格式

    private static void moveRowsByShift(XSSFSheet sheet, int srcBeginRow, int n, XSSFCellStyle firstCellStyle, XSSFCellStyle yellow, short height) {
        for (int i = srcBeginRow; i < srcBeginRow + n; i++) {
            XSSFRow row = sheet.createRow(i);
            row.setHeight(height);
            for (int j = 0; j < 10; j++) {
                if (j == 6 || j == 7) {
                    row.createCell(j).setCellStyle(yellow);
                } else {
                    row.createCell(j).setCellStyle(firstCellStyle);
                }
            }
        }
    }

    /**
     * @param sheet          sheet
     * @param srcBeginRow    复制模板的起始行
     * @param srcEndRow      复制模板的末尾行
     * @param destBeginRow   复制目的地的起始行
     * @param cellCopyPolicy 复制策略
     * @param demoRow        复制完成后，空白地区。以该row做为格式模板，设置这段区域的格式
     */
    private static void moveRows(XSSFSheet sheet, int srcBeginRow, int srcEndRow, int destBeginRow, CellCopyPolicy cellCopyPolicy, int demoRow, XSSFCellStyle firstCellStyle, XSSFCellStyle yellow, short height) {
        //先复制到目的位置
//        sheet.copyRows(srcBeginRow, srcEndRow, destBeginRow, cellCopyPolicy);
//        sheet.shiftRows(srcBeginRow, srcEndRow, 3, true, true);
//        for (int i = srcBeginRow; i<srcBeginRow+3;i++){
//            XSSFRow row = sheet.createRow(i);
//            row.setHeight(height);
//            for (int j = 0; j < 10; j++) {
//                if(j == 6 || j == 7){
//                    row.createCell(j).setCellStyle(yellow);
//                } else {
//                    row.createCell(j).setCellStyle(firstCellStyle);
//                }
//            }
//        }
        //清理之前残留的合并单元格
//        List<Integer> removeMergedRegion = new ArrayList<>();
//        for(int i=sheet.getMergedRegions().size()-1;i>=0;i--){
//            CellRangeAddress address = sheet.getMergedRegions().get(i);
//            //如果合并单元格在复制前的位置，则删除
//            if(address.getFirstRow() >= srcBeginRow && address.getFirstRow()<destBeginRow ){
//                removeMergedRegion.add(i);
//            }
//        }
//        //执行清理合并的单元格
//        for(Integer i:removeMergedRegion){
//            sheet.removeMergedRegion(i);
//        }
//        //删除移动后原地的行
//        for (int i = srcBeginRow; i <= srcEndRow; i++){
//            Row row = sheet.getRow(i);
//            if (row != null){
//                //删除复制后残留的行
//                sheet.removeRow(row);
//            }
//        }
//        //将移动后的到页末标签的位置格式用原有格式补充
//        for (int i = srcBeginRow; i < 16; i++) {
//            sheet.copyRows(demoRow, demoRow, i, cellCopyPolicy);
//        }
//        sheet.shiftRows(18,18 ,5 ,true ,true );

//        XSSFRow row = sheet.getRow(18);
//        XSSFRow row_15 = sheet.getRow(15);
//        row.
//        row.setRowStyle(row_15);

    }

}
