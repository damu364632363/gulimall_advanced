package io.renren.damu.interrupt;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class InterruptDemo {
    static volatile boolean isStop = false;
    static AtomicBoolean atomicBoolean = new AtomicBoolean(false);


    public static void main(String[] args) {

    }

    /**
     * Tests whether the current thread has been interrupted.
     * The interrupted status of the thread is cleared by this method.
     * In other words, if this method were to be called twice in succession,
     * the second call would return false (unless the current thread were interrupted again, after the first call had cleared its interrupted status and before the second call had examined it).
     */
    private static void secondCallreturnFalse() {
        System.out.println(Thread.currentThread().getName() + ">>>>>>" + Thread.interrupted());
        System.out.println(Thread.currentThread().getName() + ">>>>>>" + Thread.interrupted());
        System.out.println(">>>>>>>");
        Thread.currentThread().interrupt();
        System.out.println(">>>>>>>");
        System.out.println(Thread.currentThread().getName() + ">>>>>>" + Thread.interrupted());
        System.out.println(Thread.currentThread().getName() + ">>>>>>" + Thread.interrupted());
    }

    /**
     * interrupted，没有马上停止线程，并且sleep状态下如果是interrupt，将失败。
     */
    private static void m6() {
        //中断为true后，并不是立刻stop程序
        Thread t1 = new Thread(() -> {
            while (true){
                if(Thread.currentThread().isInterrupted()){
                    System.out.println(">>>>>>>>isInterrupted（）true,程序结束");
                    break;
                }
                try {
                    TimeUnit.MILLISECONDS.sleep(500);
                } catch (InterruptedException e) {
                    //异常再停一次
                    //线程中断标识位被恢复成false，停不下来，interrupt无效。需要再次interrupt。
                    Thread.currentThread().interrupt();
                    e.printStackTrace();
                }
                System.out.println("---------hello Interrupt");
            }
        }, "t1");
        t1.start();
        try {
            TimeUnit.MILLISECONDS.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(()->{
            t1.interrupt();
        },"t2").start();
    }

    private static void m5() {
        //中断为true后，并不是立刻stop程序
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 300; i++) {
                System.out.println(">>>>>>>>i:" + i);
            }
            System.out.println("t1.interrupt()调用之后02" + Thread.currentThread().isInterrupted());
        }, "t1");
        t1.start();
        System.out.println("t1.interrupt调用之前" + t1.isInterrupted());
        try {
            TimeUnit.MILLISECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t1.interrupt();
        System.out.println("t1.interrupt()调用之后01" + t1.isInterrupted());
        try {
            TimeUnit.MILLISECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //非活动状态，t1线程不在执行中，已经结束执行了。 t1.isInterrupted()变成true
        System.out.println("t1.interrupt()调用之后03" + t1.isInterrupted());
    }

    private static void m4() {
        Thread t1 = new Thread(() -> {
            while (true) {
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("----------isInterrupted = true,程序结束");
                    break;
                }
                System.out.println("----hello isInterrupted");
            }
        }, "t1");
        t1.start();
        System.out.println(">>>>>>>>>>>>>>>>" + t1.isInterrupted());
        try {
            TimeUnit.MILLISECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t1.interrupt();//修改t1线程的中断标识位为true
        System.out.println(">>>>>>>>>>>>>>>>" + t1.isInterrupted());
    }

    private static void m3() {
        Thread t1 = new Thread(() -> {
            while (true) {
                if (Thread.currentThread().isInterrupted()) {
                    System.out.println("----------isInterrupted = true,程序结束");
                    break;
                }
                System.out.println("----hello isInterrupted");
            }
        }, "t1");
        t1.start();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(() -> {
            t1.interrupt();//修改t1线程的中断标识位为true
        }, "t2").start();
    }

    /**
     * AtomicBoolean
     */
    private static void m2() {
        new Thread(() -> {
            while (true) {
                if (atomicBoolean.get()) {
                    System.out.println("----------atomicBoolean.get() = true,程序结束");
                    break;
                }
                System.out.println("----hello atomicBoolean.get()");
            }
        }, "t1").start();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(() -> {
            atomicBoolean.set(true);
        }, "t2").start();
    }

    /**
     * volatile
     */
    private static void m1() {
        new Thread(() -> {
            while (true) {
                if (isStop) {
                    System.out.println("----------isStop = true,程序结束");
                    break;
                }
                System.out.println("----hello isStop");
            }
        }, "t1").start();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(() -> {
            isStop = true;
        }, "t2").start();
    }
}
