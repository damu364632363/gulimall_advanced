package io.renren.damu.interrupt;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

/**
 * LockSupport.park方法让线程等待之后，唤醒方式有2种
 * 1，调用Locksupport.unpark
 * 2,调用等待线程的interrupt方法，给等待的t1线程发送中断信号协商，可以唤醒t1线程
 */
public class T2 {
    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            System.out.println("t1,park之前中断标识位" + Thread.currentThread().isInterrupted());
            LockSupport.park();
            System.out.println("t1,park之前中断标识位后" + Thread.currentThread().isInterrupted());
            System.out.println(Thread.currentThread().getName() + " 被唤醒" + Thread.currentThread().isInterrupted());
        }, "t1");
        t1.start();
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t1.interrupt();
        System.out.println(">>>>>>>>>>" + t1.isInterrupted());

    }
}
