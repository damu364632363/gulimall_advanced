package io.renren.damu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

public class ListTest {

//    protected static ArrayList<Object> arrayList = new ArrayList<Object>();

    // 解决措施①：使用Vector集合
//    protected static Vector<Object> arrayList = new Vector<Object>();

    // 解决措施②：我们加上Collections.synchronizedList，它会自动将我们的list方法进行改变，最后返回给我们一个加锁了List
    static List<Object> arrayList = Collections.synchronizedList(new ArrayList<Object>());

    public static void main(String[] args) {
        Thread[] threads = new Thread[500];
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new ArrayListThread();
            threads[i].start();
        }

        for (int i = 0; i < threads.length; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // 输出list中的对象元素
        for (int i = 0; i < threads.length; i++) {
            System.out.println(arrayList.get(i));
        }
    }
}

class ArrayListThread extends Thread {

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // 增加元素
        ListTest.arrayList.add(Thread.currentThread().getName());
    }

}

