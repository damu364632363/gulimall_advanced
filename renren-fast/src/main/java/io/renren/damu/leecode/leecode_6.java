package io.renren.damu.leecode;

import java.util.ArrayList;
import java.util.List;

public class leecode_6 {
    public static void main(String[] args) {
        leecode_6 lee = new leecode_6();
        String qwertqyqewery = lee.convert("qwertqyqewery", 3);
        System.out.println(qwertqyqewery);
    }

    public String convert(String s, int numRows) {
        if (numRows == 1) {
            return s;
        }
        int temp = 2 * numRows - 2;
        StringBuffer str = new StringBuffer();
        for (int i = 0; i < numRows; i++) {
            if (i == 0) {
                for (int k = 0; k * temp < s.length(); k++) {
                    str.append(s.charAt(k * temp));
                }
            } else if (i == numRows - 1) {
                for (int j = 0; j * temp + numRows - 1 < s.length(); j++) {
                    str.append(s.charAt(j * temp + numRows - 1));
                }
            } else {
                for (int j = 0; j * temp + i < s.length(); j++) {
                    str.append(s.charAt(j * temp + i));
                    if ((j + 1) * temp - i < s.length()) {
                        str.append(s.charAt((j + 1) * temp - i));
                    }
                }
            }
        }
        return str.toString();
    }

    public String convert2(String s, int numRows) {
        if (numRows == 1) {
            return s;
        }
        StringBuilder ret = new StringBuilder();
        int n = s.length();
        int temp = 2 * numRows - 2;
        for (int i = 0; i < numRows; i++) {
            for (int k = 0; k + i < n; k += temp) {
                ret.append(s.charAt(k + i));
                if (i != 0 && i != numRows - 1 && k + temp - i < n)
                    ret.append(s.charAt(k + temp - i));
            }
        }
        return ret.toString();
    }
}
