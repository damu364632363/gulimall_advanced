package io.renren.damu.leecode;

import java.util.HashSet;
import java.util.Set;

/**
 * 无重复字符的最长子串
 */
public class leecode_3 {
    public static void main(String[] args) {
        leecode_3 leecode_3 = new leecode_3();
        String demo = "abcabcdefbb";
        System.out.println(leecode_3.lengthOfLongestSubstring(demo));
    }

    //    public int lengthOfLongestSubstring(String s) {
//        int n = s.length();
//        int ans = 0;
//        for (int i = 0; i < n; i++) {
//            for (int j = i + 1; j <= n; j++) {
//                if (allUnique(s, i, j)) {
//                    ans = Math.max(ans, j - i);
//                }
//            }
//        }
//        return ans;
//    }
//
//    public boolean allUnique(String s, int start, int end) {
//        Set<Character> set = new HashSet<>();
//        for (int i = start; i < end; i++) {
//            Character ch = s.charAt(i);
//            if (set.contains(ch)) {
//                return false;
//            }
//            set.add(ch);
//        }
//        return true;
//    }
    public int lengthOfLongestSubstring(String s) {

        int ans = 0;
        for (int i = 0; i < s.length(); i++) {
            for (int j = i + 1; j < s.length(); j++) {
                if (unionString(s, i, j)) {
                    ans = Math.max(ans, j - i);
                }
            }
        }
        return ans;
    }

    public boolean unionString(String s, int i, int j) {
        String substring = s.substring(i, j);
        Set<Character> setString = new HashSet<>();
        for (int k = 0; k < substring.length(); k++) {
            if (setString.contains(substring.charAt(k))) {
                return false;
            } else {
                setString.add(substring.charAt(k));
            }
        }
        return true;
    }
}
