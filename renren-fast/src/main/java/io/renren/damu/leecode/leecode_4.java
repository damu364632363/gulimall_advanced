package io.renren.damu.leecode;

/**
 * 给定两个大小分别为 m 和 n 的正序（从小到大）数组 nums1 和 nums2。请你找出并返回这两个正序数组的 中位数 。
 * 算法的时间复杂度应该为 O(log (m+n)) 。
 * 链接：https://leetcode-cn.com/problems/median-of-two-sorted-arrays
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class leecode_4 {
    public static void main(String[] args) {

        System.out.println(Double.valueOf((6+7)/2));
        System.out.println(Double.valueOf((6+7)/2d));

//        leecode_4 result = new leecode_4();
//        int[] nums1 = {1, 6, 8, 9, 12, 26};
//        int[] nums2 = {2, 3, 5,6, 7, 11, 12, 16};
////        int[] nums1 = {1, 2, 2};
////        int[] nums2 = {2, 3};
//        Double medianSortedArrays = result.findMedianSortedArrays(nums1, nums2);
//        System.out.println("中位数:" + medianSortedArrays);
    }

    public Double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int[] temp = new int[nums1.length + nums2.length];
        int i = 0, j = 0, k = 0;
        while (i < nums1.length && j < nums2.length) {
            if (nums1[i] <= nums2[j]) {
                temp[k++] = (nums1[i++]);
            } else {
                temp[k++] = (nums2[j++]);
            }
        }
        while (i < nums1.length) {
            temp[k++] = (nums1[i++]);
        }
        while (j < nums2.length) {
            temp[k++] = (nums2[j++]);
        }

        for (int p = 0; p < k; p++) {
            System.out.println(">>>" + temp[p]);
        }
        if (k % 2 == 1) {
            return Double.valueOf(temp[k/2 -1]);
        } else {
            return Double.valueOf((temp[k/2-1] + temp[k/2])/2d);
        }

    }

}
