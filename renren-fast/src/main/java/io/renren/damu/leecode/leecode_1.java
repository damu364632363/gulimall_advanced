package io.renren.damu.leecode;

import java.util.HashMap;
import java.util.Map;

/**
 * 给定一个整数数组 nums 和一个整数目标值 target，
 * 请你在该数组中找出 和为目标值 target  的那 两个 整数，
 * 并返回它们的数组下标。
 */
public class leecode_1 {
    public static void main(String[] args) {
        int[] nums = {2, 7, 9, 11, 5, 17};
        int target = 13;
        int[] ints = twoSum(nums, target);
        for (int i = 0; i < ints.length; i++) {
            System.out.println(ints[i]);
        }
    }

    public static int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int descrease = target - nums[i];
            if (map.containsKey(descrease)) {
                return new int[]{map.get(descrease), i};
            } else {
                map.put(nums[i], i);
            }
        }
        throw new IllegalArgumentException("没有这样的2个数");
    }
}
