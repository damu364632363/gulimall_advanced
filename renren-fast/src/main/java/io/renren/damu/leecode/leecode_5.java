package io.renren.damu.leecode;

public class leecode_5 {
    public static void main(String[] args) {
        leecode_5 leecode = new leecode_5();
        String babad = longestPalindrome("babad");
        System.out.println(">>>>" + babad);
    }



    public static String longestPalindrome(String s) {
        //存储最长子串
        String str = "";
        //存储最长长度
        int longest = 0;
        for (int i = 0; i < s.length(); i++) {
            for (int j = i+1; j < s.length() + 1; j++) {
                String str1 = s.substring(i, j);
                if (isPalindromes(str1) && str1.length()>longest) {
                    str = str1;
                    longest= str.length();
                }
            }
        }
        return str;
    }

    public static boolean isPalindromes(String str) {
        if (str.length() == 1) {
            return true;
        }
        for (int i = 0; i < str.length() / 2; i++) {
            if (str.charAt(i) != str.charAt(str.length() - 1 - i)) {
                return false;
            }
        }
        return true;
    }
}
