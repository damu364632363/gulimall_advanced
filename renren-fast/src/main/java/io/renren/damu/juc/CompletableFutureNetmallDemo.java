package io.renren.damu.juc;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class CompletableFutureNetmallDemo {

    static List<NetMall> list = Arrays.asList(new NetMall("jd"), new NetMall("pdd"), new NetMall("tmall"));

    //同步
    public static List<String> getPriceByStep(List<NetMall> list, String productName) {
        return list.stream().map(netMall -> String.format(productName + "in %s price is %.2f", netMall.getMallName(), netMall.calcPrice(productName))).collect(Collectors.toList());
    }

    //异步
    public static List<String> getPriceByASync(List<NetMall> list, String productName) {
//        return list.stream().map(netMall -> CompletableFuture.supplyAsync(() -> String.format(productName + "in %s price is %.2f", netMall.getMallName(), netMall.calcPrice(productName)))).collect(Collectors.toList()).stream().map(CompletableFuture::join).collect(Collectors.toList());
        return list
                .stream()
                .map(netMall -> CompletableFuture.supplyAsync(() -> String.format(productName + "in %s price is %.2f", netMall.getMallName(), netMall.calcPrice(productName))))
                .collect(Collectors.toList())
                .stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());


    }


    public static void main(String[] args) throws Exception {
        long startTime = System.currentTimeMillis();
        getPriceByStep(list,"aiai");
        long endTime = System.currentTimeMillis();
        System.out.println("coustTIme>>>>>>" + (endTime - startTime));

        long startTime2 = System.currentTimeMillis();
        getPriceByASync(list,"mysql");
        long endTime2 = System.currentTimeMillis();
        System.out.println("coustTIme>>>>>>" + (endTime2 - startTime2));



    }
}

class NetMall {
    @Getter
    private String mallName;

    public NetMall(String mallName) {
        this.mallName = mallName;
    }

    public double calcPrice(String productName) {
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return ThreadLocalRandom.current().nextDouble() * 2 + productName.charAt(0);
    }
}