package io.renren.damu.juc;


class TickDemo {
    int number = 50;

    public synchronized void saleTicket() {
        if (number > 0) {
            System.out.println(Thread.currentThread().getName() + "卖出第" + number-- + "张");
        } else {
            System.out.println(">票已经卖完了。" + number);
        }
    }
}

class House {
    //    ThreadLocal<Integer> threadLocal = new ThreadLocal<Integer>(){
//        @Override
//        protected Integer initialValue(){
//            return 0;
//        }
//    };
    int i = 0;
    ThreadLocal<Integer> threadLocal = ThreadLocal.withInitial(() -> 0);
//    ThreadLocal<Integer> threadLoca2 = new ThreadLocal<Integer>() {
//        @Override
//        protected Integer initialValue() {
//            return 0;
//        }
//    };

    public void saleHouse() {
        Integer integer = threadLocal.get();
        integer++;
        threadLocal.set(integer);
    }

}

public class ThreadLocalDemo {

    public static void main(String[] args) {
//        TickDemo tickDemo = new TickDemo();
//        for (int i = 0; i < 3; i++) {
//            new Thread(() -> {
//                for (int j = 0; j < 20; j++) {
//                    tickDemo.saleTicket();
//                }
//            }, i + "").start();
//        }
        House house = new House();

        new Thread(() -> {
            try {
                for (int i = 0; i < 3; i++) {
                    house.saleHouse();
                }
                System.out.println(Thread.currentThread().getName() + "\t" + "卖出" + house.threadLocal.get());
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                house.threadLocal.remove();
            }
        }, "t1").start();

        new Thread(() -> {
            try {
                for (int i = 0; i < 9; i++) {
                    house.saleHouse();
                }
                System.out.println(Thread.currentThread().getName() + "\t" + "卖出" + house.threadLocal.get());
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                house.threadLocal.remove();
            }
        }, "t2").start();


        new Thread(() -> {
            try {
                for (int i = 0; i < 8; i++) {
                    house.saleHouse();
                }
                System.out.println(Thread.currentThread().getName() + "\t" + "卖出" + house.threadLocal.get());
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                house.threadLocal.remove();
            }
        }, "t3").start();

        System.out.println(Thread.currentThread().getName() + "\t" + "卖出" + house.threadLocal.get());
    }
}
