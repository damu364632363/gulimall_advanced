package io.renren.damu.juc;

import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

class MyObject {
    @Override
    protected void finalize() throws Throwable {
        System.out.println("gc,finalize() invoked");
    }
}

public class ReferenceDemo {
    public static void main(String[] args) {
//        weakReference();
    }

    /**
     * 假如有一个应用需要读取大量的本地图片。
     * 如果每次读取图片都从硬盘读取则会严重影响性能，
     * 如果一次性全部加载到内存中又可能造成内存一次
     * 此时使用软引用可以解决这个问题
     * 用一个hashmap来报错图片的路径和相应图片对象关联的软引用之间的映射关系，在内存不足时，jvm会自动回收这些缓存图片对象所占用的空间，
     * 从而有效的避免了oom的问题
     * Map<String,SoftReference<Bitmap>> imageCache = new HashMap<String, SoftReference<Bitmap>>();
     */
    private static void weakReference() {
        WeakReference<MyObjectDemo> weakReference = new WeakReference(new MyObjectDemo());
        System.out.println("gc before:" + weakReference.get());
        System.gc();
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("gc after:" + weakReference.get());
    }


    /**
     * 对于软引用的对象来说。当系统内存充足时，他不会被回收。当系统内存不足时，他会被回收。
     * 软引用通常在对内存敏感的程序中，比如高速缓存就有用到软引用，内存够用的时候就保留，不够用就回收。
     */
    private static void softRefenence() {

        SoftReference<MyObjectDemo> softReference = new SoftReference<>(new MyObjectDemo());
//        MyObject softReference = new MyObject();
        //内存够用
//        System.out.println("gc before内存够用" + softReference);
//        System.gc();
//        try {
//            TimeUnit.SECONDS.sleep(1);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        System.out.println("gc before内存够用" + softReference);

        //设置参数-Xms10m -xmx10m
        System.gc();
        System.out.println("gc before内存不够用" + softReference);
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            byte[] bytes = new byte[9 * 1024 * 1024];
            TimeUnit.SECONDS.sleep(2);
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            System.out.println("gc after内存不够用" + softReference);
        }
    }

    /**
     * 强引用，不管内存是否够用，只要还有引用指向我，死都不放手，大不了oom。除非null
     */
    private static void strongReference() {
        MyObjectDemo myObject = new MyObjectDemo();
        System.out.println("gc before" + myObject);

        myObject = null;

        System.gc();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("gc after" + myObject);
    }
}
