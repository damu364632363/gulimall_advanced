package io.renren.damu.juc;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

class AddDemo{

    public AtomicInteger getAtomicInteger() {
        return atomicInteger;
    }

    public void setAtomicInteger(AtomicInteger atomicInteger) {
        this.atomicInteger = atomicInteger;
    }

    AtomicInteger atomicInteger = new AtomicInteger();
    //volatile不保证原子性，
    //volatile读是直接读主内存,写也是直接刷新到主内存
    //volatile并发number++的时候，一个线程（a)先读取并加（未刷新number到主内存），另外一个线程(b)也读取并加（未刷新number到主内存）
    //当a线程刷新到主内存的时候，b线程的值也跟着被一起同步掉（因为是直接读取主内存的），导致b再将number刷新到主内存的时候是刷新a线程的值
    //也就是ab2个线程，执行后的值，是只同步a线程操作后的值，也就是只执行一次，线程不安全问题。
    // ->>>>>手里的是旧址(旧值)，虽然加了，但是主内存更新了手里的值，作废。cas不丢，因为一直比较。do(){}while{}
//     public final int getAndAddInt(Object o, long offset, int delta){
//         int v;
//         do{
//             v = getIntVolatile(o,offset);
//         } while (!compareAndSwapInt(o,offset,v,v + delta));
//         return v;
//     }
    volatile int number = 0;
    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public void add(){
        atomicInteger.getAndIncrement();
        System.out.println(Thread.currentThread().getName() + "\t" +number++);
//        number++;
    }
}

public class T3 {
    public static void main(String[] args) {
        AddDemo addDemo = new AddDemo();
        for (int i = 0; i < 20000; i++) {
            new Thread(()->{
                addDemo.add();
            }).start();
        }
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(addDemo.getNumber());
        System.out.println(addDemo.getAtomicInteger());
    }
}
