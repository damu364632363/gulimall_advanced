package io.renren.damu.juc;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

class AutomicAdd{
    AtomicInteger atomicInteger = new AtomicInteger(0);
    public void addAdd(){
        atomicInteger.incrementAndGet();
    }
//    public
}

public class AtomicIntegerDemo {

    public static void main(String[] args) {
        AutomicAdd automicAdd = new AutomicAdd();
        for (int i = 0; i < 50; i++) {
            new Thread(()->{
                for (int j = 0; j < 1000; j++) {
                    automicAdd.addAdd();
                }
            }).start();
        }
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + "\t" + automicAdd.atomicInteger.get());
    }

}
