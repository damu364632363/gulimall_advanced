package io.renren.damu.juc;

import io.netty.util.concurrent.CompleteFuture;

import java.util.concurrent.*;

public class CompletableFutureAPIDemo {
    public static void main(String[] args) throws Exception{
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 20, 1L,
                TimeUnit.SECONDS, new LinkedBlockingDeque<>(50), Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());
        System.out.println(CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 1;
        }).applyToEither(CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 2;
        }), r -> {
            return r;
        }).join());

        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        threadPoolExecutor.shutdown();
    }

    private static void thenApplyTest() {
        System.out.println(CompletableFuture.supplyAsync(() -> {
            System.out.println(">>>>>1");
            return 1;
        }).thenApply(f -> {
            System.out.println(">>>>>2");
            return f + 2;
        }).thenApply(f -> {
            System.out.println(">>>>>3");
            int k = 2/0;
            return f + 3;
        }).thenApply(f -> {
            System.out.println(">>>>>4");
            return f + 4;
        }).thenApply(f -> {
            System.out.println(">>>>>5");
            return f + 5;
        }).whenComplete((v, e) -> {
            if (e == null) {
                System.out.println("result" + v);
            }
        }).exceptionally(e -> {
            e.printStackTrace();
            return null;
        }).join());
    }

    private static void handleTest() {
        System.out.println(CompletableFuture.supplyAsync(() -> {
            System.out.println(">>>>>1");
            return 1;
        }).handle((f,e) -> {
            System.out.println(">>>>>2");
            return f + 2;
        }).handle((f,e) -> {
            System.out.println(">>>>>3");
            int k = 2/0;
            return f + 3;
        }).handle((f,e)-> {
            System.out.println(">>>>>4");
            return f + 4;
        }).handle((f,e) -> {
            System.out.println(">>>>>5");
            return f + 5;
        }).whenComplete((v, e) -> {
            if (e == null) {
                System.out.println("result" + v);
            }
        }).exceptionally(e -> {
            e.printStackTrace();
            return null;
        }).join());
    }

    private static void m1() throws InterruptedException, ExecutionException {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 20, 1L,
                TimeUnit.SECONDS, new LinkedBlockingDeque<>(50), Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());
        CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 1;
        });
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(future.complete(88) + "\t" + future.get());
        threadPoolExecutor.shutdown();
    }
}
