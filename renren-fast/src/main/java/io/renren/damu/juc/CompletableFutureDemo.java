package io.renren.damu.juc;

//import com.mysql.cj.util.TimeUtil;

import java.util.concurrent.*;

public class CompletableFutureDemo {

    public static void main(String[] args) throws Exception {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 20, 1L,
                TimeUnit.SECONDS, new LinkedBlockingDeque<>(50), Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.AbortPolicy());
        CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 1;
            //有指定的方法，thenApply()->得上面的方法完成之后，才进行下面的方法
            //thenApplyAsync()，异步方法
        }).thenApply(f -> {
            return f + 2;
        }).whenComplete((v, e) -> {
            if (e == null) {
                System.out.println("..." + v);
            }
        }).exceptionally(e ->{
            e.printStackTrace();
            return null;
        });
//        System.out.println(">>>" + future1.get());
        System.out.println("----------over");
        //主线程不要立刻结束，否则completableFuture默认使用的线程池会立刻关闭：暂停3秒钟线程
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        threadPoolExecutor.shutdown();
    }

    /**
     * completableFuture 的比较
     * @param threadPoolExecutor
     * @throws InterruptedException
     * @throws ExecutionException
     */
    private static void m1(ThreadPoolExecutor threadPoolExecutor) throws InterruptedException, ExecutionException {
        CompletableFuture<Void> future1 = CompletableFuture.runAsync(() -> {
            System.out.println(Thread.currentThread().getName() + "\t" + ">>>come in");
        });
        System.out.println(future1.get());
        CompletableFuture<Void> future2 = CompletableFuture.runAsync(() -> {
            System.out.println(Thread.currentThread().getName() + "\t" + "----come in");
        }, threadPoolExecutor);
        System.out.println(future2.get());

        CompletableFuture<Integer> future3 = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread().getName() + "\t" + "---come in");
            return 232;
        });
        System.out.println(future3.get());

        CompletableFuture<Integer> future4 = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread().getName() + "\t" + "---come in");
            return 2392;
        }, threadPoolExecutor);

        System.out.println(future4.get());
    }
}
