package io.renren.damu.juc;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.util.concurrent.atomic.AtomicReference;

@Getter
@ToString
@AllArgsConstructor
class User{
    String userName;
    int age;
}

public class AtomicReferenceDemo {
    public static void main(String[] args) {
        User z = new User("z", 22);
        User li = new User("li", 26);
        AtomicReference atomicReference = new AtomicReference();
        atomicReference.set(z);
        System.out.println(atomicReference.compareAndSet(z,li) + "\t" + atomicReference.get().toString());
        System.out.println(atomicReference.compareAndSet(z,li) + "\t" + atomicReference.get().toString());

    }
}
