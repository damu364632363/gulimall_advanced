package io.renren.damu.juc;

import org.openjdk.jol.info.ClassLayout;
import org.openjdk.jol.vm.VM;

public class MyObjectDemo {
    public static void main(String[] args) {
//        System.out.println(VM.current().details());
//        System.out.println(VM.current().objectAlignment());
        Object o = new Object();
        //引入了jol，
//        System.out.println(ClassLayout.parseInstance(o).toPrintable());
        synchronized (o) {
            System.out.println(ClassLayout.parseInstance(o).toPrintable());
        }
    }
}
