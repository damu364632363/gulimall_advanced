package io.renren.damu.juc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ThreadLocalDateUtils {
    public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public Date parse(String strignDate) throws ParseException {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.parse(strignDate);
    }

    public static final ThreadLocal<SimpleDateFormat> sdfThreadLocal = ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
    public Date threadLocalParse(String strignDate) throws ParseException {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdfThreadLocal.get().parse(strignDate);
    }

    public static void main(String[] args) throws ParseException {
//        System.out.println(ThreadLocalDateUtils.parse("2021-11-11 11:11:11"));
        ThreadLocalDateUtils threadLocalDateUtils = new ThreadLocalDateUtils();
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                for (int j = 0; j < 2000; j++) {
                    try {
//                    System.out.println(threadLocalDateUtils.parse("2021-11-11 11:11:11"));
                        System.out.println(threadLocalDateUtils.threadLocalParse("2021-11-11 11:11:11"));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    finally {
                        threadLocalDateUtils.sdfThreadLocal.remove();
                    }
                }
            }, i + "").start();
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
