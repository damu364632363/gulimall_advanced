package io.renren.damu.juc;

import java.util.concurrent.atomic.AtomicInteger;

public class CASDemo {
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(5);
        System.out.println(atomicInteger.get());
        System.out.println(atomicInteger.compareAndSet(5,338) + "\t" + atomicInteger.get());
        System.out.println(atomicInteger.compareAndSet(5,666) + "\t" + atomicInteger.get());
    }
}
