package io.renren.damu.juc;

import java.util.concurrent.*;

public class FutureTaskDemo {
    public static void main(String[] args) throws Exception{
        FutureTask<Integer> futureTask = new FutureTask<>(() -> {
            System.out.println(Thread.currentThread().getName() + "线程进来");
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return 1024;
        });
        new Thread(futureTask,"t1").start();
        //必须等待。只要出现get方法，不管是否计算完成都阻塞等待结果出来再运行。
//        System.out.println("获取的futureTask的值" + futureTask.get());
        //过时不候，
        try {
            System.out.println("获取的futureTask的值" + futureTask.get(2L,TimeUnit.SECONDS));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            System.out.println("》》》》》超时");
            e.printStackTrace();
        }
        System.out.println("继续执行");
//        new Thread(() -> {
//            futureTask
//        },"t2").start();
    }

//    CompletableFuture
}
