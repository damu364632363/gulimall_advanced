package io.renren.damu.juc;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAccumulator;
import java.util.concurrent.atomic.LongAdder;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class ClickNumber {
    int number = 0;
    int numLock = 0;

    public synchronized void addSynchronized() {
        number++;
    }

    Lock lock = new ReentrantLock();
    public void addLock() {
        lock.lock();
        try {
            numLock++;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    AtomicInteger atomicInteger = new AtomicInteger();

    public void addAtomicInteger() {
        atomicInteger.incrementAndGet();
    }

    AtomicLong atomicLong = new AtomicLong();

    public void addAtomicLong() {
        atomicLong.incrementAndGet();
    }

    LongAdder longAdder = new LongAdder();

    public void addLongAdder() {
        longAdder.increment();
    }

    LongAccumulator longAccumulator = new LongAccumulator((x, y) -> {
        return x + y;
    }, 0);

    public void addLongAccumulator() {
        longAccumulator.accumulate(1);
    }

}

public class LongAdderCalcDemo {
    public static final int SIZE_THREAD = 50;
    public static final int _1W = 10000;

    public static void main(String[] args) throws Exception {
        ClickNumber clickNumber = new ClickNumber();
        CountDownLatch countDownLatch1 = new CountDownLatch(SIZE_THREAD);
        CountDownLatch countDownLatch2 = new CountDownLatch(SIZE_THREAD);
        CountDownLatch countDownLatch3 = new CountDownLatch(SIZE_THREAD);
        CountDownLatch countDownLatch4 = new CountDownLatch(SIZE_THREAD);
        CountDownLatch countDownLatch5 = new CountDownLatch(SIZE_THREAD);
        CountDownLatch countDownLatch6 = new CountDownLatch(SIZE_THREAD);

        long startTime = 0L;
        long endTime = 0L;

        startTime = System.currentTimeMillis();
        for (int i = 0; i < SIZE_THREAD; i++) {
            new Thread(() -> {
                try {
                    for (int j = 0; j < 100 * _1W; j++) {
                        clickNumber.addSynchronized();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    countDownLatch1.countDown();
                }
            }, i + "").start();
        }
        //必须等待所有线程执行完，会一直等待下去，当然也可以设置指定时间等待超时 await(timeout);
        countDownLatch1.await();
        endTime = System.currentTimeMillis();
//        System.out.println(clickNumber.number);
        //addSynchronized   1398
        System.out.println("---costTime:" + (endTime - startTime) + "毫秒" + "\t addSynchronized:" + clickNumber.number);

        startTime = System.currentTimeMillis();
        for (int i = 0; i < SIZE_THREAD; i++) {
            new Thread(() -> {
                try {
                    for (int j = 0; j < 100 * _1W; j++) {
                        clickNumber.addLock();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    countDownLatch6.countDown();
                }
            }, i + "").start();
        }
        //必须等待所有线程执行完，会一直等待下去，当然也可以设置指定时间等待超时 await(timeout);
        countDownLatch6.await();
        endTime = System.currentTimeMillis();
//        System.out.println(clickNumber.number);
        //addSynchronized   1398
        System.out.println("---costTime:" + (endTime - startTime) + "毫秒" + "\t addLock:" + clickNumber.numLock);

        startTime = System.currentTimeMillis();
        for (int i = 0; i < SIZE_THREAD; i++) {
            new Thread(() -> {
                try {
                    for (int j = 0; j < 100 * _1W; j++) {
                        clickNumber.addAtomicInteger();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    countDownLatch2.countDown();
                }
            }, i + "").start();
        }
        //必须等待所有线程执行完，会一直等待下去，当然也可以设置指定时间等待超时 await(timeout);
        countDownLatch2.await();
        endTime = System.currentTimeMillis();
//        System.out.println(clickNumber.number);
        //addSynchronized   1398
        System.out.println("---costTime:" + (endTime - startTime) + "毫秒" + "\t addAtomicInteger:" + clickNumber.atomicInteger.get());

        startTime = System.currentTimeMillis();
        for (int i = 0; i < SIZE_THREAD; i++) {
            new Thread(() -> {
                try {
                    for (int j = 0; j < 100 * _1W; j++) {
                        clickNumber.addAtomicLong();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    countDownLatch3.countDown();
                }
            }, i + "").start();
        }
        //必须等待所有线程执行完，会一直等待下去，当然也可以设置指定时间等待超时 await(timeout);
        countDownLatch3.await();
        endTime = System.currentTimeMillis();
//        System.out.println(clickNumber.number);
        //addSynchronized   1398
        System.out.println("---costTime:" + (endTime - startTime) + "毫秒" + "\t addAtomicLong:" + clickNumber.atomicLong.get());


        startTime = System.currentTimeMillis();
        for (int i = 0; i < SIZE_THREAD; i++) {
            new Thread(() -> {
                try {
                    for (int j = 0; j < 100 * _1W; j++) {
                        clickNumber.addLongAdder();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    countDownLatch4.countDown();
                }
            }, i + "").start();
        }
        //必须等待所有线程执行完，会一直等待下去，当然也可以设置指定时间等待超时 await(timeout);
        countDownLatch4.await();
        endTime = System.currentTimeMillis();
//        System.out.println(clickNumber.number);
        //addSynchronized   1398
        System.out.println("---costTime:" + (endTime - startTime) + "毫秒" + "\t addLongAdder:" + clickNumber.longAdder.longValue());


        startTime = System.currentTimeMillis();
        for (int i = 0; i < SIZE_THREAD; i++) {
            new Thread(() -> {
                try {
                    for (int j = 0; j < 100 * _1W; j++) {
                        clickNumber.addLongAccumulator();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    countDownLatch5.countDown();
                }
            }, i + "").start();
        }
        //必须等待所有线程执行完，会一直等待下去，当然也可以设置指定时间等待超时 await(timeout);
        countDownLatch5.await();
        endTime = System.currentTimeMillis();
//        System.out.println(clickNumber.number);
        //addSynchronized   1398
        System.out.println("---costTime:" + (endTime - startTime) + "毫秒" + "\t addLongAccumulator:" + clickNumber.longAccumulator.longValue());


    }

}
