package io.renren.damu.juc;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

class BankAccount {
    String bankName = "ccb";
    //以一种线程安全的方式操作非线程安全对象内的某些字段
    //1更新的对象属性必须使用public volatile 修饰符.否则会报错
    public volatile int money = 0;
    public volatile int salary = 0;
    //可以用AtomicInteger修饰money，但是粒度会比较大，内存占用会比较大
    AtomicIntegerFieldUpdater fieldUpdater = AtomicIntegerFieldUpdater.newUpdater(BankAccount.class, "money");

    public void transfer(BankAccount bankAccount) {
        fieldUpdater.incrementAndGet(bankAccount);
    }

    public void addSalary() {
        salary++;
    }

}

public class AtomicIntegerFieldUpdaterDemo {
    public static void main(String[] args) {
        BankAccount bankAccount = new BankAccount();
        for (int i = 0; i < 1000; i++) {
            new Thread(() -> {
                for (int j = 0; j < 100; j++) {
                    bankAccount.transfer(bankAccount);
                    bankAccount.addSalary();
                }
            }, String.valueOf(i)).start();
        }
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(">>>>>>>" + bankAccount.money);
        System.out.println(">>>>>>>" + bankAccount.salary);
    }
}
