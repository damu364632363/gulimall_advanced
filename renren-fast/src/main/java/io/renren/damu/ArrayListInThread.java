package io.renren.damu;

import java.util.ArrayList;
//import java.util.Collections;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

public class ArrayListInThread implements Runnable {

//    private static List threadList = new ArrayList();

//    threadList = Collections.synchronizedList(new ArrayList<>());
//    private static List threadList = Collections.synchronizedList(new ArrayList<>());
    private static List threadList = new Vector();

    @Override
    public void run() {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        threadList.add(Thread.currentThread().getName());
    }

    public static void main(String[] args) throws InterruptedException {
        ArrayListInThread listInThread = new ArrayListInThread();
        for (int i = 0; i < 100; i++) {
            Thread thread = new Thread(listInThread, String.valueOf(i));
            thread.start();
        }

        Thread.sleep(2000);

        System.out.println(listInThread.threadList.size());
        //輸出list中的值
        for (int i = 0; i < listInThread.threadList.size(); i++) {
            if(listInThread.threadList.get(i) == null){
                System.out.println();
            }
            System.out.print(listInThread.threadList.get(i) + " ");
        }
    }
}
