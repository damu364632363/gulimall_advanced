//package io.renren.hadoop.hdfs;
//
//import org.apache.hadoop.conf.Configuration;
//import org.apache.hadoop.fs.*;
//import org.apache.hadoop.io.IOUtils;
//import org.apache.hadoop.yarn.webapp.hamlet2.Hamlet;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.net.URI;
//import java.net.URISyntaxException;
//import java.nio.ByteBuffer;
//import java.util.Arrays;
//
//public class HdfsClient {
//
//    @Test
//    public void testMkdirs() throws IOException, URISyntaxException,
//            InterruptedException {
//        // 1 获取文件系统
//        Configuration configuration = new Configuration();
//        // FileSystem fs = FileSystem.get(new URI("hdfs://hadoop102:8020"), configuration);
//        FileSystem fs = FileSystem.get(new URI("hdfs://master:8020"),
//                configuration,"root");
//        // 2 创建目录
//        fs.mkdirs(new Path("/xiyou/huaguoshan/"));
//        // 3 关闭资源
//        fs.close();
//    }
//}
