package io.renren.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import io.renren.data.dto.RedisParamDto;
import io.renren.data.entity.Student;
import io.renren.service.DataTestService;
import io.renren.util.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Random;

@Slf4j
@Service
public class DataTestServiceImpl implements DataTestService {
    @Resource
    private RedisService redisService;

    @Override
    public void redisHash(RedisParamDto redisParamDto) {
        Student student = new Student();
        student.setAddress(new Random().nextInt() + "");
        student.setAddress(18 + "");
        student.setName("xxm");
        student.setAge(0);
        Object obj = redisService.hget(redisParamDto.getKey(), redisParamDto.getItem());
        log.info("查询到的数据:" + JSONObject.toJSONString(obj));
        if (ObjectUtil.isNull(obj)) {
            redisService.hset(redisParamDto.getKey(), redisParamDto.getItem(), JSONObject.toJSONString(student), 60L);
        }
//        return null;
    }
}
