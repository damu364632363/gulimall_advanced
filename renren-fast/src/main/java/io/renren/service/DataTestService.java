package io.renren.service;

import io.renren.data.dto.RedisParamDto;

public interface DataTestService {

    void redisHash(RedisParamDto redisParamDto);
}
