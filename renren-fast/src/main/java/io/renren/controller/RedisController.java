package io.renren.controller;


import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import io.renren.data.dto.RedisParamDto;
import io.renren.data.entity.Student;
import io.renren.service.DataTestService;
import io.renren.util.RedisService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Random;

@Api(tags = "RedisConttroller",description = "redis查询设置")
@Slf4j
@Validated
@RestController
@RequestMapping("redis")
@RequiredArgsConstructor
public class RedisController {

    private final DataTestService dataTestService;
    @PostMapping("/hset")
    public String hset(@RequestBody RedisParamDto redisParamDto) {
        dataTestService.redisHash(redisParamDto);
        return "";
    }

    @GetMapping("test")
    public String test(){
        return "success";
    }

    @GetMapping("testEKL")
    public String select(){
        log.info("RedisController查询");
        log.warn("meiliddwarn");
        log.error("跟你说，你小心一点");


        return "success";
    }
}
