package io.renren.controller;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Validated
@RestController
@RequestMapping("jvm")
@RequiredArgsConstructor
public class jvmController {

    @GetMapping("oom")
    public void doOom() {
        log.info("执行oom");
        List<byte[]> list1 = new ArrayList<>();
        while (true) {
            list1.add(new byte[1024 * 1024 * 4]);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @GetMapping("oom2")
    public void doOom2() {
        log.info("执行oom2");
        List<byte[]> list1 = new ArrayList<>();
        while (true) {
            list1.add(new byte[1024 * 1024 * 4]);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @GetMapping("success")
    public String success() {
        log.info("success");
        return "success";
    }

    @GetMapping("avgTimeTest1")
    public String avgTimeTest1() {
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("success");
        return "success";
    }

    @GetMapping("avgTimeTest2")
    public String avgTimeTest2() {
//        try {
//            TimeUnit.SECONDS.sleep(3);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        log.info("success");
        return "success";
    }



}
