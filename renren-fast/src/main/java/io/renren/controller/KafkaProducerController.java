//package io.renren.controller;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@Slf4j
//@Validated
//@RestController
//@RequestMapping("kafka/")
//public class KafkaProducerController {
//    // Kafka 模板用来向 kafka 发送数据
//    @Autowired
//    KafkaTemplate<String, String> kafka;
//
//    @RequestMapping("/atguigu")
//    public String data(String msg) {
//        try {
//            kafka.send("first", msg);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return "ok";
//    }
//}
