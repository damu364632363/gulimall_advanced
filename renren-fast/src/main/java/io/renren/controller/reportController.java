//package io.renren.controller;
//
//
//import io.renren.util.DateUtils;
//import io.renren.util.JasperReportTypeEn;
//import io.renren.data.vo.NopOrderPickupVo;
//import io.renren.util.JasperReportUtil;
//import io.renren.util.StrUtil;
//import lombok.RequiredArgsConstructor;
//import lombok.extern.slf4j.Slf4j;
//import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
//import org.springframework.validation.annotation.Validated;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletResponse;
//import javax.validation.constraints.NotBlank;
//import java.util.*;
//import java.util.concurrent.TimeUnit;
//
//@Slf4j
//@Validated
//@RestController
//@RequestMapping("report")
//@RequiredArgsConstructor
//public class reportController {
//
//    @PostMapping("exportPickPdf")
//    public void exportPickPdf(@NotBlank(message = "{required}") String ids, HttpServletResponse response) throws Exception {
//        List<NopOrderPickupVo> nopOrderPickupList = new ArrayList<NopOrderPickupVo>();
////        List<Map<String, Object>> dataList = nopOrderDetailService.queryNopOrderPickList(ids.split(StringConstant.COMMA));
//        List<Map<String, Object>> dataList = getMapList();
//        NopOrderPickupVo nopOrderPickup;
//        for (Map<String, Object> map : dataList) {
//            nopOrderPickup = new NopOrderPickupVo();
//            nopOrderPickup.setRelNo(StrUtil.null2empty(map.get("REL_NO")));
//            nopOrderPickup.setSkuCode(StrUtil.null2empty(map.get("SKU_CODE")));
//            nopOrderPickup.setLocationCode(StrUtil.null2empty(map.get("LOCATION_CODE")));
//            nopOrderPickup.setInventoryType(StrUtil.null2empty(map.get("INVENTORY_TYPE")));
//            nopOrderPickup.setQty(StrUtil.null2empty(map.get("QTY")));
//            nopOrderPickup.setReceiveDate(StrUtil.null2empty(map.get("RECEIVE_DATE")));
//            nopOrderPickupList.add(nopOrderPickup);
//        }
//
//        JasperReportUtil jrUtil = new JasperReportUtil();
//        jrUtil.setJasperFile("/jasper/NopOrderPickup.jasper");
//        Map<String, Object> params = new HashMap<>();
//        params.put("CURRENT_TIME", DateUtils.dispStringLong(new Date()));
//        jrUtil.setParameters(params);
//        jrUtil.setDataSource(new JRBeanCollectionDataSource(nopOrderPickupList));
//        jrUtil.createReportResponse(JasperReportTypeEn.PDF, response);
//    }
//
//    public List<Map<String, Object>> getMapList() {
//        Map<String, Object> map1 = new HashMap<>();
//        List<Map<String, Object>> dataList = new ArrayList<>();
//        for (int i = 0; i < 5; i++) {
//            map1.put("REL_NO", "REL_NO" + i);
//            map1.put("SKU_CODE", "SKU_CODE" + i);
//            map1.put("LOCATION_CODE", "LOCATION_CODE" + i);
//            map1.put("INVENTORY_TYPE", "INVENTORY_TYPE" + i);
//            map1.put("QTY", "QTY" + i);
//            map1.put("RECEIVE_DATE", "RECEIVE_DATE" + i);
//            dataList.add(map1);
//        }
//        return dataList;
//    }
//
//
//}
