package io.renren.controller;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Validated
@RestController
@RequestMapping("inter")
@RequiredArgsConstructor
public class interceptroAndFilterController {

    @GetMapping("/li")
    public String v(String value) {
        return value;
    }
}
