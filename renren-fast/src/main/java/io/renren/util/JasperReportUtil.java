//package io.renren.util;
//
//import net.sf.jasperreports.engine.*;
//import net.sf.jasperreports.engine.export.HtmlExporter;
//import net.sf.jasperreports.engine.export.JRPdfExporter;
//import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
//import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
//import net.sf.jasperreports.engine.util.JRLoader;
//import net.sf.jasperreports.export.SimpleExporterInput;
//import net.sf.jasperreports.export.SimpleHtmlExporterConfiguration;
//import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
//import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.boot.system.ApplicationHome;
//import org.springframework.core.io.ClassPathResource;
//
//import javax.servlet.http.HttpServletResponse;
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.OutputStream;
//import java.net.URLEncoder;
//import java.util.Map;
//import java.util.Random;
//import java.util.Set;
//
//public class JasperReportUtil {
//    //private String jrxmlFile;//未编译的文件
//    private String jasperFile;//已经编译好的文件
//    private String filePath;//生成报表保存的路径
//    private String fileName;//生成报表的文件名
//    private Map parameters;
//    private JRDataSource dataSource;
//    /*public String getJrxmlFile() {
//        return jrxmlFile;
//    }
//    public void setJrxmlFile(String jrxmlFile) {
//        this.jrxmlFile = jrxmlFile;
//    }*/
//    public String getJasperFile() {
//        return jasperFile;
//    }
//    public void setJasperFile(String jasperFile) {
//        this.jasperFile = jasperFile;
//    }
//    public String getFilePath() {
//        return filePath;
//    }
//    public void setFilePath(String filePath) {
//        this.filePath = filePath;
//    }
//    public String getFileName() {
//        return fileName;
//    }
//    public void setFileName(String fileName) {
//        this.fileName = fileName;
//    }
//    public JRDataSource getDataSource() {
//        return dataSource;
//    }
//    public void setDataSource(JRDataSource dataSource) {
//        this.dataSource = dataSource;
//    }
//    public Map getParameters() {
//        return parameters;
//    }
//    public void setParameters(Map parameters) {
//        this.parameters = parameters;
//    }
//
//    public void createReportResponse(JasperReportTypeEn reportTypeEn, HttpServletResponse response) throws Exception{
//        if(StringUtils.isBlank(fileName)){
//            Random r = new Random();
//            fileName = System.currentTimeMillis() + "_" + r.nextInt()+reportTypeEn.getSuffix();
//        }
//        //设置响应方式
//        response.setContentType(reportTypeEn.getContentType());
//        response.setHeader("Content-Disposition", "attachment; filename="+ URLEncoder.encode(fileName,"UTF-8"));
//        createReport(reportTypeEn,response.getOutputStream());
//    }
//    public void createReport(JasperReportTypeEn reportTypeEn) throws Exception{
//        FileUtil.newFolder(filePath);//create Folder
//        if(StringUtils.isBlank(fileName)){
//            Random r = new Random();
//            fileName = System.currentTimeMillis() + "_" + r.nextInt()+reportTypeEn.getSuffix();
//        }
//        createReport(reportTypeEn,new FileOutputStream(filePath+File.separator+fileName));
//    }
//    public void createReport(JasperReportTypeEn reportTypeEn, OutputStream out) throws Exception{
//        if(parameters != null){
//            Set<Map.Entry<String, String>> entries = parameters.entrySet();
//            for (Map.Entry<String, String>entry:entries){
//                String key = entry.getKey();
//                if("SUBREPORT_DIR".equals(key) || "LOGO_PATH".equals(key)){
////                    String subFileName = StrUtil.null2empty(entry.getValue());
//                    String subFileName = null == entry.getValue() ? "" : entry.getValue().toString().trim();
//                    if(StringUtils.isNotBlank(subFileName)){
//                        //SpringBoot打成jar包后无法读取resources资源文件的解决方法：https://www.cnblogs.com/qingshanli/p/11730400.html
//                        ApplicationHome applicationHome = new ApplicationHome(JasperReportUtil.class);
//                        //项目打包成jar包所在的根路径
//                        String subFilePath = applicationHome.getSource().getParent() + "/"+subFileName;
//                        File subFile = new File(subFilePath);
//                        //获取文件流
//                        ClassPathResource subResource = new ClassPathResource(subFileName);
//                        boolean flag = true;
//                        if (!subFile.exists()) {
//                            flag = true;
//                        }else{
//                            flag = false;
//                            if(subFile.lastModified() < subResource.lastModified()){//文件更新
//                                flag = true;
//                            }
//                        }
//                        if(flag){
//                            try {
//                                //获取类路径下的指定文件流
//                                FileUtils.copyInputStreamToFile(subResource.getInputStream(), subFile);
//                            } catch (IOException e) {
//                                throw new Exception("新建目录文件操作出错:"+subFilePath);
//                            }
//                        }
//                        if("SUBREPORT_DIR".equals(key)){
//                            entry.setValue(subFile.getParent()+"/");
//                        }else if("LOGO_PATH".equals(key)){
//                            entry.setValue(subFile.getPath());
//                        }
//                    }
//                }
//            }
//        }
//        /*if(StringUtils.isNotBlank(jrxmlFile)&&StringUtils.isBlank(jasperFile)){
//            JasperCompileManager.compileReport((new ClassPathResource(jrxmlFile)).getInputStream());//编译报表格式
//            jasperFile= jrxmlFile.replace(".jrxml", ".jasper");
//        }*/
//        //获取文件流
//        ClassPathResource resource = new ClassPathResource(jasperFile);
//        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(resource.getInputStream());
//        JasperPrint jasperPrint;
//        if(null == dataSource){
//            jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
//        }else{
//            jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
//        }
//        switch (reportTypeEn){
//            case PDF:
//                JRPdfExporter pdfExporter = new JRPdfExporter();
//                //设置输入项
//                pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//                //设置输出项
//                pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
//                pdfExporter.exportReport();
//                out.flush();
//                out.close();
//                break;
//            case EXCEL:
//                JRXlsxExporter excelExporter = new JRXlsxExporter();
//                //设置输入项
//                excelExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//                //设置输出项
//                excelExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
//                //设置响应方式
//                excelExporter.exportReport();
//                out.flush();
//                out.close();
//                break;
//            case WORD:
//                JRDocxExporter wordExporter = new JRDocxExporter();
//                //设置输入项
//                wordExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//                //设置输出项
//                wordExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
//                wordExporter.exportReport();
//                out.flush();
//                out.close();
//                break;
//            case HTML:
//                HtmlExporter htmlExporter = new HtmlExporter();
//                //设置输入项
//                SimpleHtmlExporterConfiguration config = new SimpleHtmlExporterConfiguration();
//                String header= "<html>\n"+
//                        "<head>\n"+
//                        "  <title></title>\n"+
//                        "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/>\n"+
//                        "  <link rel=\"stylesheet\" type=\"text/css\" href=\"css/jasper.css\" />\n"+
//                        "  <style type=\"text/css\">\n"+
//                        "    a {text-decoration: none}\n"+
//                        "  </style>\n"+
//                        "</head>\n";
//                config.setHtmlHeader(header);
//                htmlExporter.setConfiguration(config);
//                htmlExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//                //设置输出项
//                htmlExporter.setExporterOutput(new SimpleHtmlExporterOutput(out));
//                htmlExporter.exportReport();
//                out.flush();
//                out.close();
//                break;
//            default :
//                throw new Exception("不支持导出此类文件!");
//        }
//    }
//}