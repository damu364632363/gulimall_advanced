

package io.renren.util;

import org.apache.commons.lang.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/****************************************************************************
 *<br>
 * <b>功能描述:</b> <br>
 * 日期工具 <br>
 * ------------------------------------------------------------------- <br>
 * 由于数据库设计提出的要求，在Entity对象中日期字段也以String保存 <br>
 * 这个类处理日期和String之间的转换和日期的格式化显示 <br>
 * --------------------------------------------------------------------
 *************************************************************************** 
 */

public class DateUtils {

	/**
	 * 2个日期相差的天数
	 *
	 * @param date1
	 *            开始日期（第一个时间）
	 * @param date2
	 *            截止日期（第二个时间）
	 * @return int 计算后相差的天数
	 */
	public static int dateDiff(Date date1, Date date2) {
		int result = 0;
		DateUtils et = new DateUtils();
		GregorianCalendar gc1 = new GregorianCalendar();
		GregorianCalendar gc2 = new GregorianCalendar();
		gc1.setTime(date1);
		gc2.setTime(date2);
		result = et.getDays(gc1, gc2);
		return result;
	}

	protected int getDays(GregorianCalendar g1, GregorianCalendar g2) {
		int elapsed = 0;
		GregorianCalendar gc1, gc2;
		if (g2.after(g1)) {
			gc2 = (GregorianCalendar) g2.clone();
			gc1 = (GregorianCalendar) g1.clone();
		} else {
			gc2 = (GregorianCalendar) g1.clone();
			gc1 = (GregorianCalendar) g2.clone();
		}

		gc1.clear(Calendar.MILLISECOND);
		gc1.clear(Calendar.SECOND);
		gc1.clear(Calendar.MINUTE);
		gc1.clear(Calendar.HOUR_OF_DAY);

		gc2.clear(Calendar.MILLISECOND);
		gc2.clear(Calendar.SECOND);
		gc2.clear(Calendar.MINUTE);
		gc2.clear(Calendar.HOUR_OF_DAY);

		while (gc1.before(gc2)) {
			gc1.add(Calendar.DATE, 1);
			elapsed++;
		}
		return elapsed;
	}
	//by wgg
	public static  boolean compare(String s1, String s2) {


		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			Date d1 = simpleDateFormat.parse(s1);
			Date d2 = simpleDateFormat.parse(s2);
			if (d1.getTime() > d2.getTime())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		catch (ParseException e) {// TODO Auto-generated catch blocke.printStackTrace();}
			e.printStackTrace();
		}
		return true;
	}
	/**
	 * 2个日期相差的天数
	 *
	 * @param startData
	 *            开始日期（第一个时间）
	 * @param endData
	 *            截止日期（第二个时间）
	 * @return int 计算后相差的天数
	 */
	public static int dateDiff(String startData, String endData) {
		return dateDiff(DateUtils.toDate(startData), DateUtils.toDate(endData));
	}

	/**
	 * 2个日期相差的天数
	 *
	 * @param startData
	 *            开始日期（第一个时间）
	 * @param endData
	 *            截止日期（第二个时间）
	 * @return int 计算后相差的天数
	 */
	public static int dateDiff(String startData, Date endData) {
		return dateDiff(DateUtils.toDate(startData), endData);
	}

	/**
	 * 2个日期相差的天数
	 *
	 * @param startData
	 *            开始日期（第一个时间）
	 * @param endData
	 *            截止日期（第二个时间）
	 * @return int 计算后相差的天数
	 */
	public static int dateDiff(Date startData, String endData) {
		return dateDiff(startData, DateUtils.toDate(endData));
	}

	protected GregorianCalendar getThisDate(Date date, int i,
			int gregorianCalendarType) {
		GregorianCalendar thisday = new GregorianCalendar();
		thisday.setTime(date);
		thisday.add(gregorianCalendarType, i);
		return thisday;
	}

	/**
	 * @Title: dateAdd
	 * @Description: TODO 日期加上天数的计算
	 * @param @param date 日期
	 * @param @param day 要加的天数
	 * @return Date 返回计算后的新时间
	 */
	public static Date dateAdd(Date date, int day) {
		DateUtils dateUtil = new DateUtils();
		GregorianCalendar thisday = dateUtil.getThisDate(date, day,
				GregorianCalendar.DATE);
		return new Date(thisday.getTime().getTime());
	}

	/**
	 * @author:黄跃武
	 * @Title: addDate
	 * @Description:日期加上指定小时
	 * @param date：要加时间的日期，sx：添加的小时数
	 * @return Date
	 * @throws
	 * @date 2012-9-26 上午9:41:57
	 */
	public static Date dateAdd(Date date, Double sx) {

		String str_sx = sx.toString().replace(".", "_");
		String[] array_time = str_sx.split("_");
		int int_day = Integer.parseInt(array_time[0]);
		double d_hour= (Double.parseDouble(sx.toString()) - int_day) * 24;
		int int_min = (int) d_hour;
		return hourAdd(dateAdd(date,int_day),int_min);
	}

	/**
	 * @author:黄跃武
	 * @Title: dateAdd
	 * @Description:日期加上指定小时
	 * @param date：要加时间的日期，sx：添加的小时数
	 * @return Date
	 * @throws
	 * @date 2012-9-26 上午10:05:40
	 */
	public static Date dateAdd(String date, Double sx) {
		return dateAdd(DateUtils.toDate(date),sx);
	}

	/**
	 * @Title: dateAdd
	 * @Description: TODO 日期加上天数的计算
	 * @param @param date 日期
	 * @param @param day 要加的天数
	 * @return Date 返回计算后的新时间
	 */
	public static Date dateAdd(String date, int day) {
		return dateAdd(DateUtils.toDate(date), day);
	}

	/**
	 * @Title: hourAdd
	 * @Description: TODO 日期加上小时的计算
	 * @param @param date 日期
	 * @param @param day 要加的小时数
	 * @return Date 返回计算后的新时间
	 */
	public static Date hourAdd(Date date, int hour) {
		DateUtils dateUtil = new DateUtils();
		GregorianCalendar thisday = dateUtil.getThisDate(date, hour,
				GregorianCalendar.HOUR);
		return new Date(thisday.getTime().getTime());
	}

	/**
	 * @Title: hourAdd
	 * @Description: TODO 日期加上小时的计算
	 * @param @param date 日期
	 * @param @param day 要加的小时数
	 * @return Date 返回计算后的新时间
	 */
	public static Date hourAdd(String date, int hour) {
		return hourAdd(DateUtils.toDate(date), hour);
	}

	/**
	 * @Title: monthAdd
	 * @Description: TODO 日期根据月的计算
	 * @param @param date 日期
	 * @param @param day 要加的月数
	 * @return Date 返回计算后的新时间
	 */
	public static Date monthAdd(Date date, int month) {
		DateUtils dateUtil = new DateUtils();
		GregorianCalendar thisday = dateUtil.getThisDate(date, month,
				GregorianCalendar.MONTH);
		return new Date(thisday.getTime().getTime());
	}

	/**
	 * @Title: monthAdd
	 * @Description: TODO 日期根据月的计算
	 * @param @param date 日期
	 * @param @param day 要加的月数
	 * @return Date 返回计算后的新时间
	 */
	public static Date monthAdd(String date, int month) {
		return monthAdd(DateUtils.toDate(date), month);
	}

	/**
	 * @Title: minuteAdd
	 * @Description: TODO 日期根据分钟的计算
	 * @param @param date 日期
	 * @param @param day 要加的分钟数
	 * @return Date 返回计算后的新时间
	 */
	public static Date minuteAdd(Date date, int minute) {
		DateUtils dateUtil = new DateUtils();
		GregorianCalendar thisday = dateUtil.getThisDate(date, minute,
				GregorianCalendar.MINUTE);
		return new Date(thisday.getTime().getTime());
	}

	/**
	 * @Title: minuteAdd
	 * @Description: TODO 日期根据分钟的计算
	 * @param @param date 日期
	 * @param @param day 要加的分钟数
	 * @return Date 返回计算后的新时间
	 */
	public static Date minuteAdd(String date, int minute) {
		return minuteAdd(DateUtils.toDate(date), minute);
	}

	/**
	 * 时间转换 2006/12/12 12:31:20 to 2006-12-12
	 *
	 * @param d
	 * @return
	 */

	public static String toDateString2(String d) {
		if (d != null && d.length() > 0) {
			String date = d.substring(0, d.lastIndexOf(" "));
			String date2 = d.substring(d.lastIndexOf(" ") + 1, d.length());
			String[] array = date.split("/");
			String newdate = array[2] + "-" + array[0] + "-" + array[1] + " "
					+ date2;
			return newdate;
		} else
			return null;
	}

	/**
	 * 时间转换 061212 to 2006-12-12
	 *
	 * @param d
	 * @return
	 */
	public static String toDateString(String d) {
		// 原来时间格式为061226,转换为
		if (d != null && d.trim().length() == 6) {
			String year = d.substring(0, 2);
			String mouth = d.substring(2, 4);
			String day = d.substring(4, 6);
			String newdate = "20" + year + "-" + mouth + "-" + day;
			return newdate;
		} else
			return "";
	}
	/**
	 * 时间转换 061212 to 2006-12-12 00:00:00
	 *
	 * @param d
	 * @return
	 */
	public static String toDateTimeString(String d) {
		// 原来时间格式为061226,转换为
		if (d != null && d.trim().length() == 14) {
			String year = d.substring(0, 4);
			String mouth = d.substring(4, 6);
			String day = d.substring(6, 8);
			String hh = d.substring(8,10);
			String mm = d.substring(10, 12);
			String ss = d.substring(12, 14);
			String newdate = year + "-" + mouth + "-" + day+" "+hh+":"+mm+":"+ss;
			return newdate;
		} else
			return "";
	}
	/**
	 * 时间转换 2006-12-12 to 20061212000000
	 *
	 * @param d
	 * @return
	 */
	public static String toDateString6(String d) {
		if (!StringUtils.isBlank(d)) {
			return d.replaceAll("-", "")+"000000";
		} else
			return "";
	}

	/**
	 * 时间转换 2006-12-12 to 061212
	 *
	 * @param d
	 * @return
	 */
	public static String toDateString3(String d) {
		if (!StringUtils.isBlank(d)) {
			return d.replaceAll("-", "").substring(2);
		} else
			return "";
	}
	/**
	 * 时间转换 2006-12-12 12:12:12 to 20061212121212
	 * 14位
	 * @param d
	 * @return
	 */
	public static String toDateString14(String d) {
		if (!StringUtils.isBlank(d)) {
			return d.replaceAll("-", "").replaceAll(" ", "").replaceAll(":", "");
		} else
			return "";
	}
	/**
	 * 时间转换 061212 to 5月5号
	 *
	 * @param d
	 * @return
	 */
	public static String toDateString5(String d) {
		// 原来时间格式为061226,转换为
		if (!StringUtils.isBlank(d)) {
			String year = d.substring(0, 2);
			String mouth = d.substring(2, 4);
			String day = d.substring(4, 6);
			String newdate = new Integer(mouth) + "月" + new Integer(day) + "日";
			return newdate;
		} else
			return "";
	}

	public static String toDateString4(String d) {
		if (d != null && d.length() > 10) {
			return d.substring(0, 10);
		} else {
			return "";
		}
	}

	/**
	 * 将日期型转换成短显示格式，格式为"YYYY-MM-DD hh:mm:ss"
	 *
	 * @param d
	 * @return
	 */
	public static String dispStringLong(Date d) {
		return DateUtils.format(d, "yyyy'-'MM'-'dd' 'HH':'mm':'ss");
	}
	/**
	 * 将日期型转换成短显示格式，格式为"DD-Mon-YY"
	 *
	 * @param d
	 * @return
	 */
	public static String dispStringEn(Date d){
		if(null == d){
			return "";
		}
		String date = DateUtils.format(d, "dd'-'MM'-'yy");
		Map<String,String> map= new HashMap<String, String>();
		map.put("01", "Jan");
		map.put("02", "Feb");
		map.put("03", "Mar");
		map.put("04", "Apr");
		map.put("05", "May");
		map.put("06", "Jun");
		map.put("07", "Jul");
		map.put("08", "Aug");
		map.put("09", "Sep");
		map.put("10", "Otc");
		map.put("11", "Nov");
		map.put("12", "Dec");
		String[] strs =date.split("-");

		strs[1] = map.get(strs[1]);
		return strs[0]+"-"+strs[1]+"-"+strs[2];
	}
	/**
	 * 将日期型转换成短显示格式，格式为"YYYY-MM-DD"
	 *
	 * @param d
	 * @return
	 */
	public static String dispShort(Date d) {
		return DateUtils.format(d, "yyyy'-'MM'-'dd");
	}

	/**
	 * 将日期型转换成短显示格式，格式为"yyMMdd"
	 *
	 * @param d
	 * @return
	 */
	public static String dispShort3(Date d) {
		return DateUtils.format(d, "yyMMdd");
	}

	/**
	 * 将日期型转换成短显示格式，格式为"MMddyy"
	 *
	 * @param d
	 * @return
	 */
	public static String dispShort4(Date d) {
		return DateUtils.format(d, "MM'/'dd'/'yy");
	}
	/**
	 * 将日期型转换成短显示格式，格式为"YYYYMMDD"
	 *
	 * @param d
	 * @return
	 */
	public static String dispShort2(Date d) {
		return DateUtils.format(d, "yyyyMMdd");
	}

	/**
	 * 将日期型转换成短中文显示格式，格式为"YYYY年MM月DD日"
	 *
	 * @param d
	 * @return
	 */
	public static String dispShortCn(Date d) {
		return DateUtils.format(d, "yyyy'年'M'月'd'日'");
	}

	/**
	 * 将日期型转换成短中文显示格式，格式为"YYYY年MM月DD日下午K点"
	 *
	 * @param d
	 * @return
	 */
	public static String dispMedCn(Date d) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy'年'M'月'd'日'aK'点'",
				Locale.SIMPLIFIED_CHINESE);
		return sdf.format(d);
	}

	public static String dispUTC(Date d) {

		return DateUtils.format(d, "yyyy'-'MM'-'dd'T'HH':'mm':'ss+08:00");

	}

	/**
	 * 将日期型转换成长显示格式，格式为"YYYY-MM-DD HH:MI:SS"
	 *
	 * @param d
	 * @return
	 */
	public static String dispLong(Date d) {
		return DateUtils.format(d, "yyyy'-'MM'-'dd' 'HH':'mm':'ss");
	}

	/**
	 * 将日期型转换成长显示格式，格式为"YYYY-MM-DD HH:MI"
	 *
	 * @param d
	 * @return
	 */
	public static String dispLong2(Date d) {
		return DateUtils.format(d, "yyyy'-'MM'-'dd' 'HH':'mm");
	}
	/**
	 * 将日期型转换成12位长字符串，格式为"YYYYMMDDHHMISS"
	 *
	 * @param d
	 * @return
	 */
	public static String dispLong3(Date d) {
		return DateUtils.format(d, "yyyyMMddHHmmss");
	}

	/**
	 * 将日期型转换成12位长字符串，格式为"YYYYMMDDHHMI"
	 *
	 * @param d
	 * @return
	 */
	public static String dispLong4(Date d) {
		return DateUtils.format(d, "yyyyMMddHHmm");
	}

	/**
	 * 将日期型转换成长中文显示格式，格式为"YYYY年MM月DD日 HH时MI分"
	 *
	 * @param d
	 * @return
	 */

	public static String dispLongCn(Date d) {
		SimpleDateFormat sdf = new SimpleDateFormat(
				"yyyy'年'MM'月'dd'日'HH'时'mm'分'");
		return sdf.format(d);
	}

	/**
	 *
	 * @Title: format
	 * @Description: TODO 将日期转换为想要的格式
	 * @param @param date 日期
	 * @param @param format 格式
	 * @return String 返回新的日期
	 * @throws
	 */
	public static String format(String date, String format) {
		if (StringUtils.isBlank(date)) {
			return null;
		}
		return format(DateUtils.toDate(date), format);
	}

	/**
	 *
	 * @Title: format
	 * @Description: TODO 将日期转换为想要的格式
	 * @param @param date 日期
	 * @param @param format 格式
	 * @return String 返回新的日期
	 * @throws
	 */
	public static String format(Date date, String format) {
		if (null == date) {
			return null;
		}
		return new SimpleDateFormat(format).format(date);
	}

	/**
	 * @desc 处理日期的年月日格式. e.g: 1997-01-02 处理后为 19970102
	 * @param ss
	 * @return
	 * @throws ParseException
	 */
	private static String toFont(String ss) {

		String year, month, day;
		int j = ss.indexOf("-");
		if (j == 4) {
			year = ss.substring(0, 4);
			int k = ss.substring(5).indexOf("-");
			if (k == 1) {
				month = "0" + ss.substring(5).substring(0, 1);
				if (ss.substring(5).substring(2).length() == 1) {
					day = "0" + ss.substring(5).substring(2);
				} else if (ss.substring(5).substring(2).length() == 2) {
					day = ss.substring(5).substring(2);
				} else {
					return ss;
				}
			} else if (k == 2) {
				month = ss.substring(5).substring(0, 2);
				if (ss.substring(5).substring(3).length() == 1) {
					day = "0" + ss.substring(5).substring(3);
				} else if (ss.substring(5).substring(3).length() == 2) {
					day = ss.substring(5).substring(3);
				} else {
					return ss;
				}
			} else { // 如果月份长度大于二
				return ss;
			}
		} else { // 如果年份长度不等于四
			return ss;
		}
		return (year + month + day);

	}
	/**
	 * 时间转换 20061212121212 to 2006-12-12
	 * @param d
	 * @return
	 */
	public static String toDateFromString14(String d) {
		if (!StringUtils.isEmpty(d)) {
			String year = d.substring(0, 4);
			String month = d.substring(4, 6);
			String day = d.substring(6, 8);
			String newdate = year + "-" + month + "-" + day;
			return newdate;
		} else
			return "";
	}

	/**
	 * 时间转换 20061212121212 to 2006/12/12
	 * @param d
	 * @return
	 */
	public static String toDateFromString15(String d) {
		if (!StringUtils.isEmpty(d)) {
			String year = d.substring(0, 4);
			String month = d.substring(4, 6);
			String day = d.substring(6, 8);
			String newdate = year + "/" + month + "/" + day;
			return newdate;
		} else
			return "";
	}

	/**
	 * @desc 处理时间格式. e.g: 22:20:00 处理后为222000
	 * @param ss
	 * @return
	 * @throws ParseException
	 */
	private static String toEnd(String ss) {

		String hour, min, sec;
		int j = ss.indexOf(":");
		if (j == 2) {
			hour = ss.substring(0, 2);
			int k = ss.substring(3).indexOf(":");
			if (k == 1) {
				min = "0" + ss.substring(3).substring(0, 1);
				if (ss.substring(3).substring(2).length() == 1) {
					sec = "0" + ss.substring(3).substring(2);
				} else if (ss.substring(3).substring(2).length() == 2) {
					sec = ss.substring(3).substring(2);
				} else {
					return ss;
				}
			} else if (k == 2) {
				min = ss.substring(3).substring(0, 2);
				if (ss.substring(3).substring(3).length() == 1) {
					sec = "0" + ss.substring(3).substring(3);
				} else if (ss.substring(3).substring(3).length() == 2) {
					sec = ss.substring(3).substring(3);
				} else {
					return ss;
				}
			} else { // 如果分钟长度大于二
				return ss;
			}
		} else if (j == 1) {
			hour = "0" + ss.substring(0, 1);
			int k = ss.substring(2).indexOf(":");
			if (k == 1) {
				min = "0" + ss.substring(2).substring(0, 1);
				if (ss.substring(2).substring(2).length() == 1) {
					sec = "0" + ss.substring(2).substring(2);
				} else if (ss.substring(2).substring(2).length() == 2) {
					sec = ss.substring(2).substring(2);
				} else {
					return ss;
				}
			} else if (k == 2) {
				min = ss.substring(2).substring(0, 2);
				if (ss.substring(2).substring(3).length() == 1) {
					sec = "0" + ss.substring(2).substring(3);
				} else if (ss.substring(2).substring(3).length() == 2) {
					sec = ss.substring(2).substring(3);
				} else {
					return ss;
				}
			} else { // 如果分钟长度大于二
				return ss;
			}

		} else { // 如果小时长度大于二
			return ss;
		}
		return (hour + min + sec);

	}

	/**
	 * <b>功能说明：</b><br>
	 * 将生日字段转化为年龄
	 *
	 * @param birthStr
	 *            生日字段 "yyyymmdd"
	 * @return 年龄
	 */

	public static String birthDateToAge(String birthStr) {
		if (birthStr == null || birthStr.length() < 8) {
			return "0";
		} else {
			try {
				int birthYear = (Integer.valueOf(birthStr.substring(0, 4)))
						.intValue();
				GregorianCalendar gc = new GregorianCalendar();
				int age = gc.get(gc.YEAR) - birthYear + 1;
				return "" + age;
			} catch (NumberFormatException ex) {
				return birthStr;
			}
		}
	}

	/**
	 * <b>功能说明：</b><br>
	 * 将生日字段转化为年龄
	 *
	 * @param birthStr
	 *            生日字段 "yyyymmdd"
	 * @return 年龄
	 */

	public static short birthDateToAge2(String birthStr) {
		if (birthStr == null || birthStr.length() < 8) {
			return (short) 0;
		} else {
			try {
				int birthYear = (Integer.valueOf(birthStr.substring(0, 4)))
						.intValue();
				GregorianCalendar gc = new GregorianCalendar();
				short age = (short) (gc.get(gc.YEAR) - birthYear + 1);
				return age;
			} catch (NumberFormatException ex) {
				return (short) 0;
			}
		}
	}

	/**
	 * 将字符串转换成日期格式
	 *
	 * @return
	 */
	public static Date toDate(String date) {
		if (StringUtils.isBlank(date)) {
			throw new RuntimeException(
					"Illegal Data String: must be 8 or 12 chars");
		}
		Date dateTemp;
		try {
			switch (date.length()) {
			case 19:
				dateTemp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
						.parse(date);
				break;
			case 14:
				dateTemp = new SimpleDateFormat("yyyyMMddHHmmss").parse(date);
				break;
			case 12:
				dateTemp = new SimpleDateFormat("yyyyMMddHHmm").parse(date);
				break;
			case 16:
				dateTemp = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date);
				break;
			case 10:
				dateTemp = new SimpleDateFormat("yyyy-MM-dd").parse(date);
				break;
			case 8:
				dateTemp = new SimpleDateFormat("yyyyMMdd").parse(date);
				break;
			case 6:
				dateTemp = new SimpleDateFormat("yyMMdd").parse(date);
				break;
			default:
				throw new RuntimeException(
				"Illegal Data String: must be 8 or 12 chars");
			}
		} catch (Exception e) {
			throw new RuntimeException(
					"Illegal Data String: must be 8 or 12 chars");
		}
		return dateTemp;
	}

	public static String getCurrentDate() {
		return DateUtils.dispShort(new Date());
	}

	public static String getCurrentTime() {
		return DateUtils.dispLong(new Date());
	}

	public static String getCurrentYearMonth() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
		return sdf.format(new Date());
	}

	public static String getYearMonthList(String yearMonth) {
		StringBuffer sb = new StringBuffer();
		int month = Integer.parseInt(yearMonth.substring(4));
		String year = yearMonth.substring(0, 4);
		for (int i = 1; i <= 12; i++) {
			sb.append("\n<option value=" + year + (i < 10 ? "0" + i : "" + i));
			if (i == month) {
				sb.append("  selected");
			}
			sb.append(">" + year + "-" + (i < 10 ? "0" + i : "" + i)
					+ "</option>");
		}
		return sb.toString();
	}

	/**
	 * @desc 返回两年的月份列表
	 * @param yearMonth
	 * @return
	 */
	public static String getTwoYearMonthList(String yearMonth) {
		StringBuffer sb = new StringBuffer();
		int month = Integer.parseInt(yearMonth.substring(4));
		String year = yearMonth.substring(0, 4);
		String lastyear = String.valueOf(Integer.parseInt(year) - 1);
		for (int i = 1; i <= 12; i++) {
			sb.append("\n<option value=" + lastyear
					+ (i < 10 ? "0" + i : "" + i));
			sb.append(">" + lastyear + "-" + (i < 10 ? "0" + i : "" + i)
					+ "</option>");
		}

		for (int i = 1; i <= 12; i++) {
			sb.append("\n<option value=" + year + (i < 10 ? "0" + i : "" + i));
			if (i == month) {
				sb.append("  selected");
			}
			sb.append(">" + year + "-" + (i < 10 ? "0" + i : "" + i)
					+ "</option>");
		}
		return sb.toString();
	}

	/**
	 * <b>功能说明：</b><br>
	 * 把长日期转换成存储格式,例如把2004年3月12日下午2点，转换成20040312140000 目前不支持刻/半 <br>
	 *
	 * @param s
	 *            String
	 * @return String
	 * @author 蔡
	 */
	public static String longToStore(String s) {
		// 最后结果
		StringBuffer after = new StringBuffer();
		// 日期分割
		final String BREAK[] = new String[] { "年", "月", "日", "点", "分", "秒",
				"年", "月", "日", "时", "分", "秒", "/", "/", " ", ":", ":", null,
				"/", "/", ",", ":", ":", null, "-", "-", " ", ":", ":", null,
				"-", "-", ",", ":", ":", null };
		// 如果不是数字的，对应要加数值
		final String[] ADD = new String[] { "上午", "下午", "半" };
		// 数值
		final int[] ADD_ = { 0, 12, 30 };
		// 起始位置
		int start = 0;
		for (int i = 0; i < 6; i++) {
			// 找到的分隔符的位置
			int p = -1;
			int j = 0;
			while (p < 0 && j < 6) {
				if (BREAK[i + j * 6] != null) {
					p = s.indexOf(BREAK[i + j * 6], start);
					j++;
				} else {
					p = s.length();
				}
			}
			// 如果循环了6次都没找到，那就是没有这个分隔符，继续取下一个
			if (j >= 6) {
				// 如果剩余长度小于等于2，就是快结束了.
				if (s.length() - start <= 2) {
					p = s.length();
				} else {
					after.append("00");
					continue;
				}
			}
			String xTime = null;
			xTime = s.substring(start, p);
			String x = "";
			// 试着把这段的字符串转换成整形，如果转换不成功可能就是包含中文的日期，再试着转换
			try {
				x = String.valueOf(Integer.parseInt(xTime));
				after.append((x.length() > 1) ? x : "0" + x);
			} catch (NumberFormatException ex) {
				for (j = 0; j < ADD.length; j++) {
					if (xTime.indexOf(ADD[j]) >= 0) {
						break;
					}
				}
				if (j < ADD.length) {
					StringTokenizer st1 = new StringTokenizer(
							xTime, ADD[j]);
					String tt;
					try {
						if (st1.hasMoreElements()) {
							tt = String.valueOf(ADD_[j]
									+ Integer.parseInt(st1.nextElement()
											.toString()));
						} else {
							tt = String.valueOf(ADD_[j]);
						}
					} catch (NumberFormatException ex1) {
						tt = String.valueOf(ADD_[j]);
					}
					after.append(tt.length() >= 2 ? tt : "0" + tt);
				} else {
					after.append("00");
				}
			}
			start = p + BREAK[i].length();
			if (start > s.length()) {
				start = s.length(); // /break;
			}
		}
		return after.toString();
	}

	/**
	 * 4/12/2007 12:00:00 AM
	 * 将4/12/2007 12:00:00 AM转换成日期格式
	 */
	public static Date getDate(String date) throws ParseException {
		if (StringUtils.isBlank(date)) {
			return null;
		}
		String[] info = date.split(" ");

		String[] dates = info[0].split("/");
		if (dates[0].length() < 2) {
			dates[0] = "0" + dates[0];
		}
		if (dates[1].length() < 2) {
			dates[1] = "0" + dates[1];
		}

		info[0] = dates[2] + "-" + dates[0] + "-" + dates[1];
		String[] time = info[1].split(":");

		if (time[0].length() < 2) {
			time[0] = "0" + time[0];
		}
		if (time[1].length() < 2) {
			time[1] = "0" + time[1];
		}
		if (time[2].length() < 2) {
			time[2] = "0" + time[2];
		}

		if ("PM".equals(info[2]) && !"12".equals(time[0])) {
			info[1] = new Integer(new Integer(time[0]).intValue() + 12)
					.toString()
					+ ":" + time[1] + ":" + time[2];
		} else if ("AM".equals(info[2]) && "12".equals(time[0])) {
			info[1] = "00:" + time[1] + ":" + time[2];
		} else {
			info[1] = time[0] + ":" + time[1] + ":" + time[2];
		}
		return toDate(info[0] + " " + info[1]);

	}

	/**
	 * 
	 * @Title: getDateShort
	 * @Description: TODO 将2009-1-1或2009-1-1 12:30装换为2009-01-01
	 * @param @param date
	 * @param @return 设定文件
	 * @return Date 返回类型
	 * @throws
	 */
	public static String getDateShort(String date) {
		if (StringUtils.isBlank(date)) {
			return null;
		}
		String[] info = date.trim().split(" ");
		if (StringUtils.isBlank(info[0])) {
			return null;
		}
		String[] dates = info[0].split("-");
		if (dates[1].length() < 2) {
			dates[0] = "0" + dates[0];
		}
		if (dates[2].length() < 2) {
			dates[1] = "0" + dates[1];
		}
		info[0] = dates[0] + "-" + dates[1] + "-" + dates[2];
		return info[0];
	}

	
	public static Calendar setStartDay(Calendar calendar)
	{
		calendar.set(11, 0);
		calendar.set(12, 0);
		calendar.set(13, 0);
		return calendar;
	}

	public static Calendar setEndDay(Calendar calendar)
	{
		calendar.set(11, 23);
		calendar.set(12, 59);
		calendar.set(13, 59);
		return calendar;
	}

	public static String formatEnDate(Date date)
	{
		SimpleDateFormat simpledateformat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		return simpledateformat.format(date).replaceAll("上午", "AM").replaceAll("下午", "PM");
	}
	
	public static int getDayOfWeek(){
		 Calendar cal = Calendar.getInstance();
	     cal.setTime(new Date());
	     return cal.get(Calendar.DAY_OF_WEEK);
	     
	}
	/**
	 *
	 * @Title: getDays
	 * @Description: TODO 获取两个日期之间的所有日期集合
	 * @param @param startTime 开始日期
	 * @param @param endTime 结束日期
	 * @param @return 设定文件
	 * @return List<String> 返回类型(yyyy-MM-dd格式字符串)
	 * @throws
	 */
	public static List<String> getDays(String startTime, String endTime) {
		// 返回的日期集合
		List<String> days = new ArrayList<String>();

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date start = dateFormat.parse(startTime);
			Date end = dateFormat.parse(endTime);

			Calendar tempStart = Calendar.getInstance();
			tempStart.setTime(start);

			Calendar tempEnd = Calendar.getInstance();
			tempEnd.setTime(end);
			tempEnd.add(Calendar.DATE, +1);// 日期加1(包含结束)
			while (tempStart.before(tempEnd)) {
				days.add(dateFormat.format(tempStart.getTime()));
				tempStart.add(Calendar.DAY_OF_YEAR, 1);
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return days;
	}
	/**
	 *
	 * @Title: getBetweenDates
	 * @Description: TODO 获取两个日期之间的所有日期集合
	 * @param @param start 开始日期
	 * @param @param end 结束日期
	 * @param @return 设定文件
	 * @return List<Date> 返回类型(Date)
	 * @throws
	 */
	private List<Date> getBetweenDates(Date start, Date end) {
		List<Date> result = new ArrayList<Date>();
		Calendar tempStart = Calendar.getInstance();
		tempStart.setTime(start);
		tempStart.add(Calendar.DAY_OF_YEAR, 1);

		Calendar tempEnd = Calendar.getInstance();
		tempEnd.setTime(end);
		while (tempStart.before(tempEnd)) {
			result.add(tempStart.getTime());
			tempStart.add(Calendar.DAY_OF_YEAR, 1);
		}
		return result;
	}

	public static void main(String[] args) throws Exception {
    }
}
