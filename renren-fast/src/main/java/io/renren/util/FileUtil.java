package io.renren.util;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.Date;
import java.util.logging.Logger;
@Slf4j
public class FileUtil {
//    private static Logger log = Logger.getLogger(FileUtil.class);

    /**
     * 移动文件
     *
     * @param srcFile
     *            eg: c:\windows\abc.txt
     * @param destPath
     *            eg: c:\temp
     * @return
     */
    public static boolean move(String srcFile, String destPath) {
        // File (or directory) to be moved
        File file = new File(srcFile);
        // Destination directory
        File dir = new File(destPath);
        // Move file to new directory
        boolean success = file.renameTo(new File(dir, file.getName()));

        return success;
    }

    /**
     * 新建目录
     *
     * @param folderPath
     *            String 如 c:/fqf
     * @return boolean
     */
    public static void newFolder(String folderPath)  {
        try {

            java.io.File myFilePath = new java.io.File(folderPath.trim());
            if (!myFilePath.exists()) {
                myFilePath.mkdirs();
            }
        } catch (Exception e) {
            log.error("新建目录操作出错," + folderPath + "创建失败");
            throw new ExceptionDemo("新建目录操作出错," + folderPath + "创建失败");
        }

    }

    /**
     * 新建文件
     *
     * @param filePathAndName
     *            String 文件路径及名称 如c:/fqf.txt
     * @param fileContent
     *            String 文件内容
     * @return boolean
     */
    public static void newFile(String filePathAndName, String fileContent) {
        FileWriter resultFile =null;
        PrintWriter myFile = null;
        try {
            File myFilePath = new File(filePathAndName.trim());
            if (!myFilePath.exists()) {
                myFilePath.createNewFile();

            }
            resultFile = new FileWriter(myFilePath);
            myFile = new PrintWriter(resultFile);
            String strContent = fileContent;
            myFile.println(strContent);
            resultFile.flush();

        } catch (Exception e) {
            log.error("新建目录操作出错");
//            throw new Exception("新建目录文件操作出错," + filePathAndName
//                    + "创建失败:" + e.getMessage());
        }
        finally{
            try {
                resultFile.flush();
                resultFile.close();
                myFile.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }

    public static void newFile(String filePath) throws RuntimeException {
        try {
            //System.out.println("filePath:"+filePath);
            File myFilePath = new File(filePath.trim());
            if (!myFilePath.exists()) {
                myFilePath.createNewFile();
            }
        } catch (IOException e) {
            log.error("新建目录操作出错");
            throw new ExceptionDemo("新建目录文件操作出错,"
                    + "创建失败:" + e.getMessage());
        }
    }

    /**
     * 删除文件
     *
     * @param filePathAndName
     *            String 文件路径及名称 如c:/test.xml
//     * @param fileContent
     *            String
     * @return boolean
     */
    public static void delFile(String filePathAndName) {
        try {
            java.io.File myDelFile = new java.io.File(filePathAndName.trim());
            if(myDelFile.exists()){
                myDelFile.delete();
            }
        } catch (Exception e) {
            log.error("删除文件操作出错");
            e.printStackTrace();
            throw new ExceptionDemo("删除文件操作出错," + filePathAndName + "删除失败:"
                    + e.getMessage());
        }

    }

    /**
     * 删除文件夹
     *
//     * @param filePathAndName
     *            String 文件夹路径及名称 如c:/test
//     * @param fileContent
     *            String
     * @return boolean
     */
    public static void delFolder(String folderPath)  {
        try {
            delAllFile(folderPath); // 删除完里面所有内容
            java.io.File myFilePath = new java.io.File(folderPath);
            myFilePath.delete(); // 删除空文件夹
        } catch (Exception e) {
            log.error("删除文件夹操作出错");
            e.printStackTrace();
            throw new ExceptionDemo("删除文件操作出错," + folderPath + "删除失败:"
                    + e.getMessage());

        }

    }

    /**
     * 复制单个文件
     *
     * @param oldPath
     *            String 原文件路径 如：c:/fqf.txt
     * @param newPath
     *            String 复制后路径 如：f:/fqf.txt
     * @return boolean
     */
	/*public boolean copyFile(String oldPath, String newPath)
			throws RuntimeException {
		try {

			File oldfile = new File(oldPath);
			if (oldfile.exists()) { // 文件存在时
				InputStream is = new FileInputStream(oldPath); // 读入原文件
				FileOutputStream fos = new FileOutputStream(newPath);
				byte[] buffer = new byte[1024];
				int n = 0;
				while ((n = is.read(buffer)) > 0) {
					fos.write(buffer, 0, n);
				}
				fos.flush();
				fos.close();
				is.close();
				return true;
			}else{
				return false;
			}
		} catch (Exception e) {
			log.error("复制单个文件" + oldPath + "操作出错");
			e.printStackTrace();
			throw new RuntimeException("复制单个文件" + oldPath + "操作出错:"
					+ e.getMessage());
         
		}

	}
  */
    public static boolean copyFile(String oldPath, String newPath) {
        InputStream is = null;
        FileOutputStream fos = null;
        try {

            File oldfile = new File(oldPath);
            if (oldfile.exists()) { // 文件存在时
                is = new FileInputStream(oldPath); // 读入原文件
                fos = new FileOutputStream(newPath);
                byte[] buffer = new byte[1024];
                int n = 0;
                while ((n = is.read(buffer)) > 0) {
                    fos.write(buffer, 0, n);
                }
                fos.flush();

                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            log.error("复制单个文件" + oldPath + "操作出错");
            e.printStackTrace();
            throw new ExceptionDemo("复制单个文件" + oldPath + "操作出错:"
                    + e.getMessage());

        } finally {
            try {
                fos.flush();
                fos.close();
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }



    /**
     * 复制整个文件夹内容
     *
     * @param oldPath
     *            String 原文件路径 如：c:/fqf
     * @param newPath
     *            String 复制后路径 如：f:/fqf/ff
     * @return boolean
     */
    public static void copyFolder(String oldPath, String newPath) {
        try {
            (new File(newPath)).mkdir(); // 如果文件夹不存在 则建立新文件夹
            File file = new File(oldPath.trim());
            File[] file_list = file.listFiles();
            for (int i = 0; i < file_list.length; i++) {
                if (file_list[i].isFile()) {
                    copyFile(file_list[i].getPath(), newPath + File.separator
                            + file_list[i].getName());
                } else if (file_list[i].isDirectory()) {
                    copyFolder(file_list[i].getPath(), newPath + File.separator
                            + file_list[i].getName());
                }
            }
        } catch (Exception e) {
            log.error("复制整个文件夹" + oldPath + "到" + newPath + "出错");
            e.printStackTrace();
            throw new ExceptionDemo("复制整个文件夹" + oldPath + "到" + newPath
                    + "出错:" + e.getMessage());
        }
    }
    /**
     * 移动文件到指定目录
     *
     * @param oldPath
     *            String 如：c:/fqf.txt
     * @param newPath
     *            String 如：d:/fqf.txt
     */
    public static void moveFile(String oldPath, String newPath) {
        copyFile(oldPath, newPath);
        delFile(oldPath);

    }

    /**
     * 移动文件到指定目录
     *
     * @param oldPath
     *            String 如：c:/fqf.txt
     * @param newPath
     *            String 如：d:/fqf.txt
     */
    public static void moveFolder(String oldPath, String newPath) {
        copyFolder(oldPath, newPath);
        delFolder(oldPath);

    }

    /**
     * 删除文件夹下所有的文件（包括子文件夹）
     *
     * @param folderName_Path
     */

    public  static void delAllFile(String folderName_Path) {
        try {
            File file = new File(folderName_Path.trim());
            if(!file.isDirectory()){
                file.delete();
            }else{
                File[] file_list = file.listFiles();
                for (int i = 0; i < file_list.length; i++) {
                    if (file_list[i].isFile()) {
                        file_list[i].delete();
                    } else if (file_list[i].isDirectory()) {
                        delAllFile(file_list[i].getPath());
                        file_list[i].delete();
                    }
                }
            }
        } catch (Exception e) {
            log.error("delAllFile " + folderName_Path + " Error！");
            e.printStackTrace();
            throw new ExceptionDemo("delAllFile" + folderName_Path + "Error:"
                    + e.getMessage());
        }

    }

    public  static String createYMDFolder(String path){
        StringBuilder sb = new StringBuilder();
        sb.append(path);
        if(!path.endsWith(String.valueOf(File.separatorChar))){

            sb.append(File.separatorChar);
        }
        String date = DateUtil.dispShort2(new Date());

        String year = date.substring(0, 4);
        String month = date.substring(4, 6);
        String day = date.substring(6, 8);
        sb.append(File.separatorChar).append(year);
        FileUtil.newFolder(sb.toString());
        sb.append(File.separatorChar).append(month);
        FileUtil.newFolder(sb.toString());
        sb.append(File.separatorChar).append(day);
        FileUtil.newFolder(sb.toString());
        sb.append(File.separatorChar);
        return sb.toString();
    }
}
