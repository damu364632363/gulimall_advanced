package io.renren.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FtpUtil {

    /**
     * FTP_ADDRESS: ftp 服务器ip地址
     * FTP_PORT: ftp 服务器port，默认是21
     * FTP_USERNAME: ftp 服务器用户名
     * FTP_PASSWORD: ftp 服务器密码
     * FTP_BASE_PATH: ftp 服务器存储图片的绝对路径
     * IMAGE_BASE_URL: ftp 服务器外网访问图片路径
     */
//     测试环境图片服务器地址
//    private static String FTP_ADDRESS = "192.168.90.234";
    // 正式环境图片服务器地址
    private static String FTP_ADDRESS = "192.168.90.207";
    private static Integer FTP_PORT = 21;
    private static String FTP_USERNAME = "kerry";
    private static String FTP_PASSWORD = "kerry";
    // 测试环境
//    private static String IMAGE_BASE_URL = "https://xmntsys.kerryeas.com:8057/images/" + FTP_USERNAME + "/";
    // 正式环境
    private static String IMAGE_BASE_URL = "https://xmnpsys.kerryeas.com:8057/images/" + FTP_USERNAME + "/";

    /**
     * 上传图片
     *
     * @param inputStream 输入流
     * @param name        文件名
     * @return 图片 url
     * @throws IOException IO异常
     */
    public static String uploadImage(InputStream inputStream, String ftpPath, String name) throws IOException {
        FTPClient ftpClient = new FTPClient();

        // 1. 获取扩展名
        String substring = name.substring(name.lastIndexOf("."));

        // 2. 用uuid 重新命名图片  防止重名
        String uuid = UUID.randomUUID().toString().replace("-", "");

        // 3. 拼接新图片名
        name = uuid + substring;

        try {
            System.out.println(FTP_ADDRESS);
            ftpClient.connect(FTP_ADDRESS, FTP_PORT);
            ftpClient.login(FTP_USERNAME, FTP_PASSWORD);
            ftpClient.setBufferSize(1024);
            ftpClient.setControlEncoding("UTF-8");
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            ftpClient.enterLocalPassiveMode();

            if (!ftpClient.changeWorkingDirectory(ftpPath)) {
                ftpClient.makeDirectory(ftpPath);
            }
            //跳转目标目录
            ftpClient.changeWorkingDirectory(ftpPath);

            boolean isSucceed = ftpClient.storeFile(name, inputStream);
            if (isSucceed) {
                return IMAGE_BASE_URL + ftpPath + "/" + name;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ftpClient.logout();
        }
        return IMAGE_BASE_URL + "error";
    }

    /**
     * 返回名称和地址
     *
     * @param inputStream
     * @param ftpPath
     * @param name
     * @return
     * @throws IOException
     */
    public static Map<String, String> uploadImageMap(InputStream inputStream, String ftpPath, String name) throws IOException {
        FTPClient ftpClient = new FTPClient();
        // 1. 获取扩展名
        String substring = name.substring(name.lastIndexOf("."));
        // 2. 用uuid 重新命名图片  防止重名
        String uuid = UUID.randomUUID().toString().replace("-", "");
        // 3. 拼接新图片名
        name = uuid + substring;
        Map<String, String> map = new HashMap();
        map.put("name", name);
        try {
            System.out.println(FTP_ADDRESS);
            ftpClient.connect(FTP_ADDRESS, FTP_PORT);
            ftpClient.login(FTP_USERNAME, FTP_PASSWORD);
            ftpClient.setBufferSize(1024);
            ftpClient.setControlEncoding("UTF-8");
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            ftpClient.enterLocalPassiveMode();

            if (!ftpClient.changeWorkingDirectory(ftpPath)) {
                ftpClient.makeDirectory(ftpPath);
            }
            //跳转目标目录
            ftpClient.changeWorkingDirectory(ftpPath);
            boolean isSucceed = ftpClient.storeFile(name, inputStream);
            if (isSucceed) {
                map.put("url", IMAGE_BASE_URL + ftpPath + "/" + name);
                return map;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ftpClient.logout();
        }
        map.put("url", IMAGE_BASE_URL + "error");
        return map;
    }

    public static void downloadZipFile(String ftpPath, List<String> fileNameList, HttpServletResponse response) {
        FTPClient ftpClient = getFTPClient();
        OutputStream os = null;
        ZipOutputStream zos = null;
        BufferedInputStream bis = null;
//        FileInputStream in = null;
        InputStream in = null;
        try {
            // 通过response对象获取OutputStream流
            os = response.getOutputStream();
            // 获取zip的输出流
            zos = new ZipOutputStream(os);
            ftpClient.changeWorkingDirectory(ftpPath);
            FTPFile[] ftpFiles = ftpClient.listFiles();
            //遍历文件名列表天津压缩包
            for (int i = 0; i < fileNameList.size(); i++) {
                String fileName = fileNameList.get(i);
                for (FTPFile ftpFile : ftpFiles) {
                    if (fileName.equals(ftpFile.getName())) {
                        in = ftpClient.retrieveFileStream(ftpFile.getName());
                        // 创建ZIP实体，并添加进压缩包
                        ZipEntry zipEntry = new ZipEntry(fileName);
                        zos.putNextEntry(zipEntry);
                        // 设置压缩后的文件名
                        String zipFileName = "dataFile.zip";
                        // 设置Content-Disposition响应头，控制浏览器弹出保存框，若没有此句浏览器会直接打开并显示文件
                        // 中文名要进行URLEncoder.encode编码，否则客户端能下载但名字会乱码
                        response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(zipFileName, "UTF-8"));
                        // 输入缓冲流
                        bis = new BufferedInputStream(in, 1024 * 100);
                        // 创建读写缓冲区
                        byte[] buf = new byte[1024 * 100];
                        int len = 0;
                        while ((len = bis.read(buf, 0, 1024 * 100)) > 0) {
                            // 使用OutputStream将缓冲区的数据输出到客户端浏览器
                            zos.write(buf, 0, len);
                        }
                        bis.close();
                        in.close();
                        //调用ftpClient.completePendingCommand()解决第二次读取为空情况
                        ftpClient.completePendingCommand();
                        zos.closeEntry();
                    }
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(null != bis){
                    bis.close();
                }
                if(null != in){
                    in.close();
                }
                if(null !=ftpClient){
                    ftpClient.logout();
                }
                if(null != zos){
                    zos.close();
                }
                if(null != os){
                    os.close();
                }
            } catch (Exception e2) {
                e2.toString();
            }

        }


    }

    /**
     * 文件下载
     */
    public static FTPFile downloadFile(String ftpPath, String fileName, HttpServletResponse response) {
        FTPClient ftpClient = getFTPClient();
        try {
            // 连接FTP服务器,设置IP和端口
            ftpClient.connect("192.168.90.207", FTP_PORT);
            // 账号密码
            ftpClient.login(FTP_USERNAME, FTP_PASSWORD);
            // 连接超时时间,5秒
            ftpClient.setConnectTimeout(50000);
            // 设置中文编码,防止中文乱码
            ftpClient.setControlEncoding("UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (ftpClient == null) {
            return null;
        }
        try {
            ftpClient.changeWorkingDirectory(ftpPath);
            FTPFile[] ftpFiles = ftpClient.listFiles();
            for (FTPFile ftpFile : ftpFiles) {
                if (fileName.equals(ftpFile.getName())) {
                    System.out.println(">>>.>" + ftpFile.getName());
                    ServletOutputStream out = response.getOutputStream();
                    OutputStream ouputStream = response.getOutputStream();
                    InputStream inputStream = ftpClient.retrieveFileStream(ftpFile.getName());
                    int available = inputStream.available();
                    byte[] bytes = new byte[available];
                    int bytesRead;
                    while ((bytesRead = inputStream.read(bytes)) != -1) {
                        ouputStream.write(bytes, 0, bytesRead);
                    }
                    inputStream.read(bytes);
                    response.reset();
                    // 设置response的Header
                    response.setHeader("content-type", "application/octet-stream");
                    response.setContentType("application/octet-stream");
                    response.setContentType("application/octet-stream;charset=UTF-8");
                    System.out.println("ftpFile.getName>>>" + ftpFile.getName());
//                    response.addHeader("Content-disposition", headerValue +".xlsx");
                    response.setHeader("Content-Disposition", "attachment;filename=" + new String(ftpFile.getName()));
                    ouputStream.flush();
                    ouputStream.close();
                    inputStream.close();
                }
            }
            ftpClient.logout();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 文件下载到本地指定目录
     */
    public static void downloadFileToLocal(String ftpPath, String localPath, String fileName) {
        FTPClient ftpClient = getFTPClient();
        if (ftpClient == null) {
            return;
        }
        try {
            //转移到FTP服务器根目录下的指定子目录
            // "/"：表示用户的根目录，为空时表示不变更
            //参数必须是目录，当是文件时改变路径无效
            // 设置ftp中的目录
            ftpClient.changeWorkingDirectory(ftpPath);
//            FTPFile[] ftpFiles = ftpClient.listFiles(ftpPath);
            FTPFile[] ftpFiles = ftpClient.listFiles();
            for (FTPFile ftpFile : ftpFiles) {
                System.out.println(">>>>" + ftpFile.getName());
                if (fileName.equals(ftpFile.getName())) {
                    File file = new File(localPath + fileName);
                    OutputStream outputStream = new FileOutputStream(file);
                    boolean result = ftpClient.retrieveFile(ftpFile.getName(), outputStream);
                    System.out.println("下载结果：" + result);
                    outputStream.close();
                }
            }
            ftpClient.logout();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    /**
     * 获取FTPClient对象
     *
     * @return FTPClient
     */
    public static FTPClient getFTPClient() {
        FTPClient ftpClient = new FTPClient();
        try {
            // 连接FTP服务器,设置IP和端口
            ftpClient.connect(FTP_ADDRESS, FTP_PORT);
            // 账号密码
            ftpClient.login(FTP_USERNAME, FTP_PASSWORD);
            // 连接超时时间,5秒
            ftpClient.setConnectTimeout(50000);
            // 设置中文编码,防止中文乱码
            ftpClient.setControlEncoding("UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ftpClient;
    }

    /**
     * 关闭FTP方法
     *
     * @param ftp
     * @return
     */
    public static boolean closeFTP(FTPClient ftp) {
        try {
            ftp.logout();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (ftp.isConnected()) {
                try {
                    ftp.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    /**
     * FTP文件上传工具类(单个文件上传到指定文件夹,不存在创建文件夹,文件名按照数字顺序排列)
     *
     * @param fileName 文件名
     * @param in       InputStream
     * @param ftpPath  文件夹名
     * @return
     */
    public static String uploadFile(String fileName, String ftpPath, InputStream in) {
        FTPClient ftp = getFTPClient();
        String filePath = "";
        try {
            // 设置PassiveMode传输
            ftp.enterLocalPassiveMode();
            //设置二进制传输，使用BINARY_FILE_TYPE，ASC容易造成文件损坏
            ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
            //判断FPT目标文件夹是否存在不存在则创建
            if (!ftp.changeWorkingDirectory(ftpPath)) {
                ftp.makeDirectory(ftpPath);
            }
            //跳转目标目录
            ftp.changeWorkingDirectory(ftpPath);
            // 获取新文件名
            int fileNum = ftp.listFiles().length + 1;
            String substring = fileName.substring(fileName.lastIndexOf("."));
            String newFileName = fileNum + substring;
            boolean isSucceed = ftp.storeFile(newFileName, in);
            if (isSucceed) {
                filePath = IMAGE_BASE_URL + ftpPath + "/" + newFileName;
            } else {
                // 上传失败把失败的文件删除
                ftp.deleteFile(newFileName);
                filePath = IMAGE_BASE_URL + ftpPath + "/" + "error";
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeFTP(ftp);
        }
        return filePath;
    }

    /**
     * FTP文件上传工具类(上传文件数组)
     *
     * @param files   文件数组
     * @param ftpPath 文件夹名
     * @return
     */
    public static List<String> uploadFile(MultipartFile[] files, String ftpPath) {
        FTPClient ftp = getFTPClient();
        List<String> filePathList = new ArrayList<>();
        try {
            // 设置PassiveMode传输
            ftp.enterLocalPassiveMode();
            //设置二进制传输，使用BINARY_FILE_TYPE，ASC容易造成文件损坏
            ftp.setFileType(FTPClient.BINARY_FILE_TYPE);
            //判断FPT目标文件夹是否存在不存在则创建
            if (!ftp.changeWorkingDirectory(ftpPath)) {
                ftp.makeDirectory(ftpPath);
            }
            //跳转目标目录
            ftp.changeWorkingDirectory(ftpPath);
            // 读取处理文件
            for (MultipartFile file : files) {
                InputStream in = file.getInputStream();
                String fileName = file.getOriginalFilename();
                // 保存文件名为当前文件个数+1.后缀名
                int fileNum = ftp.listFiles().length + 1;
                String substring = fileName.substring(fileName.lastIndexOf("."));
                String newFileName = fileNum + substring;
                boolean isSucceed = ftp.storeFile(newFileName, in);
                if (isSucceed) {
                    filePathList.add(IMAGE_BASE_URL + ftpPath + "/" + newFileName);
                } else {
                    // 上传失败把失败的文件删除
                    ftp.deleteFile(newFileName);
                    filePathList.add(IMAGE_BASE_URL + ftpPath + "/" + "error");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeFTP(ftp);
        }
        return filePathList;
    }

    /**
     * 删除FTP上指定文件夹下文件及其子文件方法，添加了对中文目录的支持
     *
     * @param filePaths 文件地址集合
     * @return
     */
    public static void deleteByPath(List<String> filePaths) {
        FTPClient ftp = getFTPClient();
        try {
            // 进入指定文件夹
            ftp.enterLocalPassiveMode();
            for (String filePath : filePaths) {
                // 需要删除的文件夹
                int length = IMAGE_BASE_URL.length();
                String file = filePath.substring(length - 1);
                int lastIndexOf = file.lastIndexOf("/");
                String fileName = file.substring(lastIndexOf + 1);
                String folderPath = file.substring(0, lastIndexOf);
                boolean b = true;
                if (StringUtils.isNotBlank(folderPath)) {
                    // 到指定文件夹下
                    b = ftp.changeWorkingDirectory(new String(folderPath.getBytes("UTF-8"), "ISO-8859-1"));
                }
                if (b) {
                    // 删除文件
                    boolean result = ftp.deleteFile(new String(fileName.getBytes("UTF-8"), "ISO-8859-1"));
                    if (result) {
                        System.out.println("删除" + filePath + "成功!!!");
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeFTP(ftp);
        }
    }


    public static void main(String[] args) throws IOException {
//        FTPClient ftp = FtpUtil.getFTPClient();
//        ftpClient.enterLocalPassiveMode();
//        ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
//        ftpClient.changeWorkingDirectory("/aaa");
//        System.out.println(ftpClient.printWorkingDirectory());
//        ftpClient.changeWorkingDirectory("/bbb");
//        System.out.println(ftpClient.printWorkingDirectory());
/*        //判断FPT目标文件夹是否存在,不存在则创建.

        if(!ftpClient.changeWorkingDirectory("/122")){
            ftpClient.makeDirectory("/122");
        }
        // 切换文件夹
        ftpClient.changeWorkingDirectory("/121");
        // 上传图片
//        ftpClient.storeFile("1.jpg",new FileInputStream(new File("C:/Users/geyt/Pictures/Saved Pictures/112320206489.jpg")));
        // 查看当前所在文件夹
        System.out.println(ftpClient.printWorkingDirectory());
        // 当前文件夹下文件个数
        System.out.println(ftpClient.listFiles().length);
        // 遍历文件名
        for (String ftpFile : ftpClient.listNames()) {
            System.out.println(ftpFile);
        }*/
        File file = new File("C:\\Users\\linp01\\rec.xls");
        FileInputStream inputStream = new FileInputStream(file);
        String s = FtpUtil.uploadImage(inputStream, "kass", "111.jpg");
//        String s = FtpUtil.uploadFile("rec.xls", "kass", inputStream);
        System.out.println(s);

    }
}




