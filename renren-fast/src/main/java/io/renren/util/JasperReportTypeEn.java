package io.renren.util;

public enum JasperReportTypeEn {
    PDF(1,".pdf","application/pdf"),
    EXCEL(2,".xlsx","application/vnd.ms-excel"),
    WORD(3,".doc","application/msword"),
    HTML(4,".html","text/html");

    JasperReportTypeEn(int index, String suffix, String contentType) {
        this.index = index;
        this.suffix = suffix;
        this.contentType = contentType;
    }
    private int index;
    private String suffix;
    private String contentType;
    public int getIndex() {
        return index;
    }
    public String getSuffix() {
        return suffix;
    }
    public String getContentType() {
        return contentType;
    }
}
