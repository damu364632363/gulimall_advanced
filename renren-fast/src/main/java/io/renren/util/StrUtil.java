package io.renren.util;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;

public class StrUtil {
    /**
     * 判断是否为正整数
     * @param str
     * @return 是返回true,否则返回false
     */
    public static boolean isPositiveInteger(String str) {
        Pattern pattern = Pattern.compile("^[1-9][0-9]*$");
        return pattern.matcher(str).matches();
    }
    public static boolean isPositiveNumber(String string) {
        Pattern pattern = Pattern.compile("^(0|[1-9]\\d*)(\\.\\d+)?$");
        return pattern.matcher(string).matches();
    }
    public static int null2zero(Object o) {
        if (o == null|| StringUtils.isBlank(o.toString().trim())) {
            return 0;
        }

        return Integer.parseInt(o.toString());
    }
    public static float null2zeroFloat(Object o) {
        if (o == null||StringUtils.isBlank(o.toString())) {
            return 0.0f;
        }

        return Float.parseFloat(o.toString());
    }
    public static Double null2zeroDouble(Object o) {
        if (o == null||StringUtils.isBlank(o.toString())) {
            return 0.00;
        }

        return Double.parseDouble(o.toString());
    }
    public static String null2empty(Object o) {
        return  null == o ? "" : o.toString().trim();
    }
    public static String leftAddChar(String oldStr, int totalLength, String addChar) {
        int length = StrUtil.null2empty(oldStr).length();

        for (int i = 0; i < totalLength - length; i++) {
            oldStr = addChar + oldStr;
        }

        return oldStr;
    }

    public static String rightAddChar(String oldStr, int totalLength, String addChar) {
        int length = StrUtil.null2empty(oldStr).length();

        for (int i = 0; i < totalLength - length; i++) {
            oldStr = oldStr + addChar;
        }

        return oldStr;
    }
    /**
     * 判断一个字符串是否是数字
     */
    public static boolean isNumber(String string) {
        if (string == null)
            return false;
        Pattern pattern = Pattern.compile("^-?\\d+(\\.\\d+)?$");
        return pattern.matcher(string).matches();
    }
}
