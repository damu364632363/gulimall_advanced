package io.renren.util;

public class ExceptionDemo extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ExceptionDemo() {
        super();
    }

    public ExceptionDemo(String message) {
        super(message);
    }

    public ExceptionDemo(String message, Throwable cause) {
        super(message, cause);
    }

    public ExceptionDemo(Throwable cause) {
        super(cause);
    }
}
