package io.renren.util;

import org.apache.commons.lang.StringUtils;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DateUtil {

    /**
     * 2个日期相差的天数
     *
     * @return int 计算后相差的天数
     */
    public static int dateDiff(Date date1, Date date2) {
        int result = 0;
        DateUtil et = new DateUtil();
        GregorianCalendar gc1 = new GregorianCalendar();
        GregorianCalendar gc2 = new GregorianCalendar();
        gc1.setTime(date1);
        gc2.setTime(date2);
        result = et.getDays(gc1, gc2);
        return result;
    }

    protected int getDays(GregorianCalendar g1, GregorianCalendar g2) {
        int elapsed = 0;
        GregorianCalendar gc1, gc2;

        boolean flag = false;

        if (g2.after(g1)) {
            gc2 = (GregorianCalendar) g2.clone();
            gc1 = (GregorianCalendar) g1.clone();
            flag = true;
        } else {
            gc2 = (GregorianCalendar) g1.clone();
            gc1 = (GregorianCalendar) g2.clone();
        }



        gc1.clear(Calendar.MILLISECOND);
        gc1.clear(Calendar.SECOND);
        gc1.clear(Calendar.MINUTE);
        gc1.clear(Calendar.HOUR_OF_DAY);

        gc2.clear(Calendar.MILLISECOND);
        gc2.clear(Calendar.SECOND);
        gc2.clear(Calendar.MINUTE);
        gc2.clear(Calendar.HOUR_OF_DAY);

        while (gc1.before(gc2)) {
            gc1.add(Calendar.DATE, 1);
            elapsed++;
        }

        if(flag){
            return elapsed;
        }else{
            return -elapsed;
        }
    }

    /**
     * 2个日期相差的天数
     *
     * @param startData
     *            开始日期（第一个时间）
     * @param endData
     *            截止日期（第二个时间）
     * @return int 计算后相差的天数
     */
    public static int dateDiff(String startData, String endData) {
        return dateDiff(DateUtil.toDate(startData), DateUtil.toDate(endData));
    }

    /**
     * 2个日期相差的天数
     *
     * @param startData
     *            开始日期（第一个时间）
     * @param endData
     *            截止日期（第二个时间）
     * @return int 计算后相差的天数
     */
    public static int dateDiff(String startData, Date endData) {
        return dateDiff(DateUtil.toDate(startData), endData);
    }

    /**
     * 2个日期相差的天数
     *
     * @param startData
     *            开始日期（第一个时间）
     * @param endData
     *            截止日期（第二个时间）
     * @return int 计算后相差的天数
     */
    public static int dateDiff(Date startData, String endData) {
        return dateDiff(startData, DateUtil.toDate(endData));
    }

    protected GregorianCalendar getThisDate(Date date, int i,
                                            int gregorianCalendarType) {
        GregorianCalendar thisday = new GregorianCalendar();
        thisday.setTime(date);
        thisday.add(gregorianCalendarType, i);
        return thisday;
    }

    /**
     * @Title: dateAdd
     * @Description: TODO 日期加上天数的计算
     * @param @param date 日期
     * @param @param day 要加的天数
     * @return Date 返回计算后的新时间
     */
    public static Date dateAdd(Date date, int day) {
        DateUtil dateUtil = new DateUtil();
        GregorianCalendar thisday = dateUtil.getThisDate(date, day,
                GregorianCalendar.DATE);
        return new Date(thisday.getTime().getTime());
    }

    /**
     * @Title: dateAdd
     * @Description: TODO 日期加上天数的计算
     * @param @param date 日期
     * @param @param day 要加的天数
     * @return Date 返回计算后的新时间
     */
    public static Date dateAdd(String date, int day) {
        return dateAdd(DateUtil.toDate(date), day);
    }

    /**
     * @Title: hourAdd
     * @Description: TODO 日期加上小时的计算
     * @param @param date 日期
     * @param @param day 要加的小时数
     * @return Date 返回计算后的新时间
     */
    public static Date hourAdd(Date date, int hour) {
        DateUtil dateUtil = new DateUtil();
        GregorianCalendar thisday = dateUtil.getThisDate(date, hour,
                GregorianCalendar.HOUR);
        return new Date(thisday.getTime().getTime());
    }

    /**
     * @Title: hourAdd
     * @Description: TODO 日期加上小时的计算
     * @param @param date 日期
     * @param @param day 要加的小时数
     * @return Date 返回计算后的新时间
     */
    public static Date hourAdd(String date, int hour) {
        return hourAdd(DateUtil.toDate(date), hour);
    }

    /**
     * @Title: monthAdd
     * @Description: TODO 日期根据月的计算
     * @param @param date 日期
     * @param @param day 要加的月数
     * @return Date 返回计算后的新时间
     */
    public static Date monthAdd(Date date, int month) {
        DateUtil dateUtil = new DateUtil();
        GregorianCalendar thisday = dateUtil.getThisDate(date, month,
                GregorianCalendar.MONTH);
        return new Date(thisday.getTime().getTime());
    }

    /**
     * @Title: monthAdd
     * @Description: TODO 日期根据月的计算
     * @param @param date 日期
     * @param @param day 要加的月数
     * @return Date 返回计算后的新时间
     */
    public static Date monthAdd(String date, int month) {
        return monthAdd(DateUtil.toDate(date), month);
    }

    /**
     * @Title: minuteAdd
     * @Description: TODO 日期根据分钟的计算
     * @param @param date 日期
     * @param @param day 要加的分钟数
     * @return Date 返回计算后的新时间
     */
    public static Date minuteAdd(Date date, int minute) {
        DateUtil dateUtil = new DateUtil();
        GregorianCalendar thisday = dateUtil.getThisDate(date, minute,
                GregorianCalendar.MINUTE);
        return new Date(thisday.getTime().getTime());
    }

    /**
     * @Title: minuteAdd
     * @Description: TODO 日期根据分钟的计算
     * @param @param date 日期
     * @param @param day 要加的分钟数
     * @return Date 返回计算后的新时间
     */
    public static Date minuteAdd(String date, int minute) {
        return minuteAdd(DateUtil.toDate(date), minute);
    }

    /**
     * 时间转换 2006/12/12 12:31:20 to 2006-12-12
     *
     * @param d
     * @return
     */

    public static String toDateString2(String d) {
        if (d != null && d.length() > 0) {
            String date = d.substring(0, d.lastIndexOf(" "));
            String date2 = d.substring(d.lastIndexOf(" ") + 1, d.length());
            String[] array = date.split("/");
            String newdate = array[2] + "-" + array[0] + "-" + array[1] + " "
                    + date2;
            return newdate;
        } else
            return null;
    }

    /**
     * 时间转换 061212 to 2006-12-12
     *
     * @param d
     * @return
     */
    public static String toDateString(String d) {
        // 原来时间格式为061226,转换为

        String date = "";

        if(StringUtils.isNotEmpty(d)){

            if (d.trim().length() == 6) {
                String year = d.substring(0, 2);
                String month = d.substring(2, 4);
                String day = d.substring(4, 6);
                date = "20" + year + "-" + month + "-" + day;
            }

            if (d.trim().length() == 8) {
                String year = d.substring(0, 4);
                String month = d.substring(4, 6);
                String day = d.substring(6, 8);
                date = year + "-" + month + "-" + day;
            }

        }

        return date;

    }

    /**
     * 时间转换 2006-12-12 to 061212
     *
     * @param d
     * @return
     */
    public static String toDateString3(String d) {
        if (!StringUtils.isEmpty(d)) {
            return d.replaceAll("-", "").substring(2);
        } else
            return "";
    }
    /**
     * 时间转换 2006-12-12 12:12:12 to 20061212121212
     * 14位
     * @param d
     * @return
     */
    public static String toDateString14(String d) {
        if (!StringUtils.isEmpty(d)) {
            return d.replaceAll("-", "").replaceAll(" ", "").replaceAll(":", "");
        } else
            return "";
    }
    /**
     * 时间转换 061212 to 5月5号
     *
     * @param d
     * @return
     */
    public static String toDateString5(String d) {
        // 原来时间格式为061226,转换为
        if (!StringUtils.isEmpty(d)) {
            String year = d.substring(0, 2);
            String mouth = d.substring(2, 4);
            String day = d.substring(4, 6);
            String newdate = new Integer(mouth) + "月" + new Integer(day) + "日";
            return newdate;
        } else
            return "";
    }

    public static String toDateString4(String d) {
        if (d != null && d.length() > 10) {
            return d.substring(0, 10);
        } else {
            return "";
        }
    }

    /**
     * 时间转换 2006-12-12 to 20061212000000
     *
     * @param d
     * @return
     */
    public static String toDateString6(String d) {
        if (!StringUtils.isEmpty(d)) {
            return d.replaceAll("-", "")+"000000";
        } else
            return "";
    }

    /**
     * 将日期型转换成短显示格式，格式为"YYYY-MM-DD hh:mm:ss"
     *
     * @param d
     * @return
     */
    public static String dispStringLong(Date d) {
        return DateUtil.format(d, "yyyy'-'MM'-'dd' 'HH':'mm':'ss");
    }
    /**
     * 将日期型转换成短显示格式，格式为"DD-Mon-YY"
     *
     * @param d
     * @return
     */
    public static String dispStringEn(Date d){
        if(null == d){
            return "";
        }
        String date = DateUtil.format(d, "dd'-'MM'-'yy");
        Map<String,String> map= new HashMap<String, String>();
        map.put("01", "Jan");
        map.put("02", "Feb");
        map.put("03", "Mar");
        map.put("04", "Apr");
        map.put("05", "May");
        map.put("06", "Jun");
        map.put("07", "Jul");
        map.put("08", "Aug");
        map.put("09", "Sep");
        map.put("10", "Otc");
        map.put("11", "Nov");
        map.put("12", "Dec");
        String[] strs =date.split("-");

        strs[1] = map.get(strs[1]);
        return strs[0]+"-"+strs[1]+"-"+strs[2];
    }
    /**
     * 将日期型转换成短显示格式，格式为"YYYY-MM-DD"
     *
     * @param d
     * @return
     */
    public static String dispShort(Date d) {
        return DateUtil.format(d, "yyyy'-'MM'-'dd");
    }

    /**
     * 将日期型转换成长显示格式，格式为"YYYYMMDDHHMI"
     *
     * @param d
     * @return
     */
    public static String dispLong5(Date d) {
        return DateUtil.format(d, "yyyyMMddHHmm");
    }

    /**
     * 将日期型转换成短显示格式，格式为"yyMMddHHmm"
     *
     * @param d
     * @return
     */
    public static String dispShort6(Date d){
        return DateUtil.format(d, "dd-MM-yyyy");
    }

    /**
     * 将日期型转换成短显示格式，格式为"yyMMddHHmm"
     *
     * @param d
     * @return
     */
    public static String dispShort5(Date d){
        return DateUtil.format(d, "yyMMddHHmm");
    }

    /**
     * 将日期型转换成短显示格式，格式为"yyyyMM"
     *
     * @param d
     * @return
     */
    public static String dispShort4(Date d) {
        return DateUtil.format(d, "yyMM");
    }

    /**
     * 将日期型转换成短显示格式，格式为"yyMMdd"
     *
     * @param d
     * @return
     */
    public static String dispShort3(Date d) {
        return DateUtil.format(d, "yyMMdd");
    }

    /**
     * 将日期型转换成短显示格式，格式为"YYYYMMDD"
     *
     * @param d
     * @return
     */
    public static String dispShort2(Date d) {
        return DateUtil.format(d, "yyyyMMdd");
    }

    /**
     * 将日期型转换成短中文显示格式，格式为"YYYY年MM月DD日"
     *
     * @param d
     * @return
     */
    public static String dispShortCn(Date d) {
        return DateUtil.format(d, "yyyy'年'M'月'd'日'");
    }

    /**
     * 将日期型转换成短中文显示格式，格式为"YYYY年MM月DD日下午K点"
     *
     * @param d
     * @return
     */
    public static String dispMedCn(Date d) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy'年'M'月'd'日'aK'点'",
                Locale.SIMPLIFIED_CHINESE);
        return sdf.format(d);
    }

    /**
     * 将日期型转换成长显示格式，格式为"YYYY-MM-DD HH:MI:SS"
     *
     * @param d
     * @return
     */
    public static String dispLong(Date d) {
        return DateUtil.format(d, "yyyy'-'MM'-'dd' 'HH':'mm':'ss");
    }

    public static String dispUTC(Date d) {

        return DateUtil.format(d, "yyyy'-'MM'-'dd'T'HH':'mm':'ss+08:00");

    }
    /**
     * 将日期型转换成长显示格式，格式为"YYYY-MM-DD HH:MI"
     *
     * @param d
     * @return
     */
    public static String dispLong2(Date d) {
        return DateUtil.format(d, "yyyy'-'MM'-'dd' 'HH':'mm");
    }

    /**
     * 将日期型转换成12位长字符串，格式为"YYYYMMDDHHMISS"
     *
     * @param d
     * @return
     */
    public static String dispLong3(Date d) {
        return DateUtil.format(d, "yyyyMMddHHmmss");
    }

    /**
     * 将日期型转换成12位长字符串，格式为"YYYYMMDDHHMI"
     *
     * @param d
     * @return
     */
    public static String dispLong4(Date d) {
        return DateUtil.format(d, "yyyyMMddHHmm");
    }

    /**
     * 将日期型转换成10位长字符串，格式为"YYYYMMDDHH"
     *
     * @param d
     * @return
     */
    public static String dispLong6(Date d) {
        return DateUtil.format(d, "yyyyMMddHH");
    }

    /**
     * 将日期型转换成长中文显示格式，格式为"YYYY年MM月DD日 HH时MI分"
     *
     * @param d
     * @return
     */

    public static String dispLongCn(Date d) {
        SimpleDateFormat sdf = new SimpleDateFormat(
                "yyyy'年'MM'月'dd'日'HH'时'mm'分'");
        return sdf.format(d);
    }

    /**
     *
     * @Title: format
     * @Description: TODO 将日期转换为想要的格式
     * @param @param date 日期
     * @param @param format 格式
     * @return String 返回新的日期
     * @throws
     */
    public static String format(String date, String format) {
        if (StringUtils.isEmpty(date)) {
            return null;
        }
        return format(DateUtil.toDate(date), format);
    }

    /**
     *
     * @Title: format
     * @Description: TODO 将日期转换为想要的格式
     * @param @param date 日期
     * @param @param format 格式
     * @return String 返回新的日期
     * @throws
     */
    public static String format(Date date, String format) {
        if (null == date) {
            return null;
        }
        return new SimpleDateFormat(format).format(date);
    }

    /**
     * @desc 处理日期的年月日格式. e.g: 1997-01-02 处理后为 19970102
     * @param
     * @return
     * @throws ParseException
     */
    private static String toFont(String ss) {

        String year, month, day;
        int j = ss.indexOf("-");
        if (j == 4) {
            year = ss.substring(0, 4);
            int k = ss.substring(5).indexOf("-");
            if (k == 1) {
                month = "0" + ss.substring(5).substring(0, 1);
                if (ss.substring(5).substring(2).length() == 1) {
                    day = "0" + ss.substring(5).substring(2);
                } else if (ss.substring(5).substring(2).length() == 2) {
                    day = ss.substring(5).substring(2);
                } else {
                    return ss;
                }
            } else if (k == 2) {
                month = ss.substring(5).substring(0, 2);
                if (ss.substring(5).substring(3).length() == 1) {
                    day = "0" + ss.substring(5).substring(3);
                } else if (ss.substring(5).substring(3).length() == 2) {
                    day = ss.substring(5).substring(3);
                } else {
                    return ss;
                }
            } else { // 如果月份长度大于二
                return ss;
            }
        } else { // 如果年份长度不等于四
            return ss;
        }
        return (year + month + day);

    }

    /**
     * 时间转换 20061212121212 to 2006-12-12
     * @param d
     * @return
     */
    public static String toDateFromString14(String d) {
        if (!StringUtils.isEmpty(d)) {
            String year = d.substring(0, 4);
            String month = d.substring(4, 6);
            String day = d.substring(6, 8);
            String newdate = year + "-" + month + "-" + day;
            return newdate;
        } else
            return "";
    }

    /**
     * @desc 处理时间格式. e.g: 22:20:00 处理后为222000
     * @param
     * @return
     * @throws ParseException
     */
    private static String toEnd(String ss) {

        String hour, min, sec;
        int j = ss.indexOf(":");
        if (j == 2) {
            hour = ss.substring(0, 2);
            int k = ss.substring(3).indexOf(":");
            if (k == 1) {
                min = "0" + ss.substring(3).substring(0, 1);
                if (ss.substring(3).substring(2).length() == 1) {
                    sec = "0" + ss.substring(3).substring(2);
                } else if (ss.substring(3).substring(2).length() == 2) {
                    sec = ss.substring(3).substring(2);
                } else {
                    return ss;
                }
            } else if (k == 2) {
                min = ss.substring(3).substring(0, 2);
                if (ss.substring(3).substring(3).length() == 1) {
                    sec = "0" + ss.substring(3).substring(3);
                } else if (ss.substring(3).substring(3).length() == 2) {
                    sec = ss.substring(3).substring(3);
                } else {
                    return ss;
                }
            } else { // 如果分钟长度大于二
                return ss;
            }
        } else if (j == 1) {
            hour = "0" + ss.substring(0, 1);
            int k = ss.substring(2).indexOf(":");
            if (k == 1) {
                min = "0" + ss.substring(2).substring(0, 1);
                if (ss.substring(2).substring(2).length() == 1) {
                    sec = "0" + ss.substring(2).substring(2);
                } else if (ss.substring(2).substring(2).length() == 2) {
                    sec = ss.substring(2).substring(2);
                } else {
                    return ss;
                }
            } else if (k == 2) {
                min = ss.substring(2).substring(0, 2);
                if (ss.substring(2).substring(3).length() == 1) {
                    sec = "0" + ss.substring(2).substring(3);
                } else if (ss.substring(2).substring(3).length() == 2) {
                    sec = ss.substring(2).substring(3);
                } else {
                    return ss;
                }
            } else { // 如果分钟长度大于二
                return ss;
            }

        } else { // 如果小时长度大于二
            return ss;
        }
        return (hour + min + sec);

    }

    /**
     * <b>功能说明：</b><br>
     * 将生日字段转化为年龄
     *
     * @param birthStr
     *            生日字段 "yyyymmdd"
     * @return 年龄
     */

    public static String birthDateToAge(String birthStr) {
        if (birthStr == null || birthStr.length() < 8) {
            return "0";
        } else {
            try {
                int birthYear = (Integer.valueOf(birthStr.substring(0, 4)))
                        .intValue();
                GregorianCalendar gc = new GregorianCalendar();
                int age = gc.get(gc.YEAR) - birthYear + 1;
                return "" + age;
            } catch (NumberFormatException ex) {
                return birthStr;
            }
        }
    }

    /**
     * <b>功能说明：</b><br>
     * 将生日字段转化为年龄
     *
     * @param birthStr
     *            生日字段 "yyyymmdd"
     * @return 年龄
     */

    public static short birthDateToAge2(String birthStr) {
        if (birthStr == null || birthStr.length() < 8) {
            return (short) 0;
        } else {
            try {
                int birthYear = (Integer.valueOf(birthStr.substring(0, 4)))
                        .intValue();
                GregorianCalendar gc = new GregorianCalendar();
                short age = (short) (gc.get(gc.YEAR) - birthYear + 1);
                return age;
            } catch (NumberFormatException ex) {
                return (short) 0;
            }
        }
    }

    /**
     * 将字符串转换成日期格式
     *
     * @return
     * @exception
     */
    public static Date toDate(String date) {
        if (StringUtils.isEmpty(date)) {
            throw new RuntimeException(
                    "Illegal Data String: must be 8 or 12 chars");
        }
        //System.out.println(date);
        Date dateTemp;
        try {
            switch (date.length()) {
                case 19:
                    dateTemp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                            .parse(date);
                    break;
                case 16:
                    dateTemp = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date);
                    break;
                case 14:
                    dateTemp = new SimpleDateFormat("yyyyMMddHHmmss").parse(date);
                    break;
                case 12:
                    dateTemp = new SimpleDateFormat("yyyyMMddHHmm").parse(date);
                    break;
                case 10:
                {
                    if(date.indexOf("-")>=0){
                        dateTemp = new SimpleDateFormat("yyyy-MM-dd").parse(date);
                    }else{
                        dateTemp = new SimpleDateFormat("yyyyMMddHH").parse(date);
                    }
                    break;
                }
                case 8:
                    dateTemp = new SimpleDateFormat("yyyyMMdd").parse(date);
                    break;
                case 6:
                    dateTemp = new SimpleDateFormat("yyMMdd").parse(date);
                    break;
                default:
                    throw new RuntimeException(
                            "Illegal Data String: must be 8 or 12 chars");
            }
        } catch (Exception e) {
            throw new RuntimeException(
                    "Illegal Data String: must be 8 or 12 chars");
        }
        return dateTemp;
    }

    public static String getCurrentDate() {
        return DateUtil.dispShort(new Date());
    }

    public static String getCurrentTime() {
        return DateUtil.dispLong(new Date());
    }

    public static String getCurrentYearMonth() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        return sdf.format(new Date());
    }

    public static String getYearMonthList(String yearMonth) {
        StringBuffer sb = new StringBuffer();
        int month = Integer.parseInt(yearMonth.substring(4));
        String year = yearMonth.substring(0, 4);
        for (int i = 1; i <= 12; i++) {
            sb.append("\n<option value=" + year + (i < 10 ? "0" + i : "" + i));
            if (i == month) {
                sb.append("  selected");
            }
            sb.append(">" + year + "-" + (i < 10 ? "0" + i : "" + i)
                    + "</option>");
        }
        return sb.toString();
    }

    /**
     * @desc 返回两年的月份列表

     * @return
     */
    public static String getTwoYearMonthList(String yearMonth) {
        StringBuffer sb = new StringBuffer();
        int month = Integer.parseInt(yearMonth.substring(4));
        String year = yearMonth.substring(0, 4);
        String lastyear = String.valueOf(Integer.parseInt(year) - 1);
        for (int i = 1; i <= 12; i++) {
            sb.append("\n<option value=" + lastyear
                    + (i < 10 ? "0" + i : "" + i));
            sb.append(">" + lastyear + "-" + (i < 10 ? "0" + i : "" + i)
                    + "</option>");
        }

        for (int i = 1; i <= 12; i++) {
            sb.append("\n<option value=" + year + (i < 10 ? "0" + i : "" + i));
            if (i == month) {
                sb.append("  selected");
            }
            sb.append(">" + year + "-" + (i < 10 ? "0" + i : "" + i)
                    + "</option>");
        }
        return sb.toString();
    }

    /**
     * <b>功能说明：</b><br>
     * 把长日期转换成存储格式,例如把2004年3月12日下午2点，转换成20040312140000 目前不支持刻/半 <br>
     *
     * @param s
     *            String
     * @return String
     * @exception
     * @author 蔡
     */
    public static String longToStore(String s) {
        // 最后结果
        StringBuffer after = new StringBuffer();
        // 日期分割
        final String BREAK[] = new String[] { "年", "月", "日", "点", "分", "秒",
                "年", "月", "日", "时", "分", "秒", "/", "/", " ", ":", ":", null,
                "/", "/", ",", ":", ":", null, "-", "-", " ", ":", ":", null,
                "-", "-", ",", ":", ":", null };
        // 如果不是数字的，对应要加数值
        final String[] ADD = new String[] { "上午", "下午", "半" };
        // 数值
        final int[] ADD_ = { 0, 12, 30 };
        // 起始位置
        int start = 0;
        for (int i = 0; i < 6; i++) {
            // 找到的分隔符的位置
            int p = -1;
            int j = 0;
            while (p < 0 && j < 6) {
                if (BREAK[i + j * 6] != null) {
                    p = s.indexOf(BREAK[i + j * 6], start);
                    j++;
                } else {
                    p = s.length();
                }
            }
            // 如果循环了6次都没找到，那就是没有这个分隔符，继续取下一个
            if (j >= 6) {
                // 如果剩余长度小于等于2，就是快结束了.
                if (s.length() - start <= 2) {
                    p = s.length();
                } else {
                    after.append("00");
                    continue;
                }
            }
            String xTime = null;
            xTime = s.substring(start, p);
            String x = "";
            // 试着把这段的字符串转换成整形，如果转换不成功可能就是包含中文的日期，再试着转换
            try {
                x = String.valueOf(Integer.parseInt(xTime));
                after.append((x.length() > 1) ? x : "0" + x);
            } catch (NumberFormatException ex) {
                for (j = 0; j < ADD.length; j++) {
                    if (xTime.indexOf(ADD[j]) >= 0) {
                        break;
                    }
                }
                if (j < ADD.length) {
                    java.util.StringTokenizer st1 = new java.util.StringTokenizer(
                            xTime, ADD[j]);
                    String tt;
                    try {
                        if (st1.hasMoreElements()) {
                            tt = String.valueOf(ADD_[j]
                                    + Integer.parseInt(st1.nextElement()
                                    .toString()));
                        } else {
                            tt = String.valueOf(ADD_[j]);
                        }
                    } catch (NumberFormatException ex1) {
                        tt = String.valueOf(ADD_[j]);
                    }
                    after.append(tt.length() >= 2 ? tt : "0" + tt);
                } else {
                    after.append("00");
                }
            }
            start = p + BREAK[i].length();
            if (start > s.length()) {
                start = s.length(); // /break;
            }
        }
        return after.toString();
    }

    /**
     * 4/12/2007 12:00:00 AM
     * 将4/12/2007 12:00:00 AM转换成日期格式
     */
    public static Date getDate(String date) throws ParseException {
        if (StringUtils.isEmpty(date)) {
            return null;
        }
        String[] info = date.split(" ");

        String[] dates = info[0].split("/");
        if (dates[0].length() < 2) {
            dates[0] = "0" + dates[0];
        }
        if (dates[1].length() < 2) {
            dates[1] = "0" + dates[1];
        }

        info[0] = dates[2] + "-" + dates[0] + "-" + dates[1];
        String[] time = info[1].split(":");

        if (time[0].length() < 2) {
            time[0] = "0" + time[0];
        }
        if (time[1].length() < 2) {
            time[1] = "0" + time[1];
        }
        if (time[2].length() < 2) {
            time[2] = "0" + time[2];
        }

        if ("PM".equals(info[2]) && !"12".equals(time[0])) {
            info[1] = new Integer(new Integer(time[0]).intValue() + 12)
                    .toString()
                    + ":" + time[1] + ":" + time[2];
        } else if ("AM".equals(info[2]) && "12".equals(time[0])) {
            info[1] = "00:" + time[1] + ":" + time[2];
        } else {
            info[1] = time[0] + ":" + time[1] + ":" + time[2];
        }
        return toDate(info[0] + " " + info[1]);

    }

    /**
     *
     * @Title: getDateShort
     * @Description: TODO 将2009-1-1或2009-1-1 12:30装换为2009-01-01
     * @param @param date
     * @param @return 设定文件
     * @return Date 返回类型
     * @throws
     */
    public static String getDateShort(String date) {
        if (StringUtils.isEmpty(date)) {
            return null;
        }
        String[] info = date.trim().split(" ");
        if (StringUtils.isEmpty(info[0])) {
            return null;
        }
        String[] dates = info[0].split("-");
        if (dates[1].length() < 2) {
            dates[0] = "0" + dates[0];
        }
        if (dates[2].length() < 2) {
            dates[1] = "0" + dates[1];
        }
        info[0] = dates[0] + "-" + dates[1] + "-" + dates[2];
        return info[0];
    }
    /**
     *
     * @Title: getXmlGregorian
     * @Description: 将日期转化成XMLGregorianCalendar各式的对象日期
     * @param date
     * @return XMLGregorianCalendar
     * @throws DatatypeConfigurationException
     */
    public static XMLGregorianCalendar getXmlGregorian(Date date){
        if(null==date){
            return null;
        }
        XMLGregorianCalendar calendar =null;
        try {
            calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar();
            calendar.setYear(Integer.parseInt(new SimpleDateFormat("yyyy").format(date)));
            calendar.setMonth(Integer.parseInt(new SimpleDateFormat("MM").format(date)));
            calendar.setDay(Integer.parseInt(new SimpleDateFormat("dd").format(date)));
            calendar.setHour(Integer.parseInt(new SimpleDateFormat("hh").format(date)));
            calendar.setMinute(Integer.parseInt(new SimpleDateFormat("mm").format(date)));
            calendar.setSecond(Integer.parseInt(new SimpleDateFormat("ss").format(date)));
            return calendar;
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        return null;
    }


    //public boolean isDate(){
    //String	^(?ni:(?=\d)((?'year'((1[6-9])|([2-9]\d))\d\d)(?'sep'[/.-])(?'month'0?[1-9]|1[012])\2(?'day'((?<!(\2((0?[2469])|11)\2))31)|(?<!\2(0?2)\2)(29|30)|((?<=((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(16|[2468][048]|[3579][26])00)\2\3\2)29)|((0?[1-9])|(1\d)|(2[0-8])))(?:(?=\x20\d)\x20|$))?((?<time>((0?[1-9]|1[012])(:[0-5]\d){0,2}(\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2}))?)$
    //}
    /**
     *
     * @Title: getFirstDayOfMonth
     * @Description: 获取月份第一天
     * @param date：'YYYY-MM'
     * @return 月份第一天yyyymmdd
     * @throws
     */
    public static String getFirstDayOfMonth(String date)
    {
        String[] dates=date.split("-");
        int year=Integer.parseInt(dates[0]);
        int month=Integer.parseInt(dates[1]);

        if(month>12){
//            throw new KeasRuntimeException("月份不正确!");
        }

        return year+""+(month<10?("0"+month):(month))+"01";

	  /*System.out.println("本月第一天为:"+year+"-"+(month<10?("0"+month):(month))+"-"+"01");
	  month++;
	  if(month>12)
	  {
	   year++;
	   month=1;
	  }
	  System.out.println("下月第一天为:"+year+"-"+(month<10?("0"+month):(month))+"-"+"01");*/
    }

    /**
     *
     * @Title: getLastDayOfMonth
     * @Description: 获取月份最后一天
     * @param date：'YYYY-MM'
     * @return yyyymmdd
     * @throws
     */
    public   static   String   getLastDayOfMonth(String date){
        String[] dates=date.split("-");
        int year=Integer.parseInt(dates[0]);
        int month=Integer.parseInt(dates[1]);
        if(month>12){
//            throw new KeasRuntimeException("月份不正确!");
        }
        //下个月第一天的日期，然后减去1天,Calendar的月是从0-11
        Calendar   c   =   new   GregorianCalendar(year,  month,   1);
        c.add(Calendar.DATE,   -1);

        int m = c.get(Calendar.MONTH)+1;
        int day = c.get(Calendar.DATE);

        return c.get(Calendar.YEAR)+ ""+(m<10?("0"+m):(m))+""+(day<10?("0"+day):(day));
    }

    /**
     *
     * @Title: getFirstDayOfWeek
     * @Description: 获取月第index周的星期一
     * @param date：'YYYY-MM'
     * @param index：第几周
     * @return yyyymmdd
     * @throws
     */
    public static String getFirstDayOfWeek(String date,int index){
        if(index<1){
//            throw new KeasRuntimeException("index is not right!");
        }

        String[] dates=date.split("-");
        int year=Integer.parseInt(dates[0]);
        int month=Integer.parseInt(dates[1]);
        Calendar cal = new GregorianCalendar(year, month-1, 1);
        int a =cal.get(Calendar.DAY_OF_WEEK);
        if(a!=1){//如果月第一天不是星期天，则先算出第一个星期天
            cal.add(Calendar.DATE, 8-a);
        }

        cal.add(Calendar.DATE, 7*(index-1)-6);//月的第index个星期一

        int m = cal.get(Calendar.MONTH)+1;
        int d = cal.get(Calendar.DATE);
        return cal.get(Calendar.YEAR)+ ""+(m<10?("0"+m):(m))+""+(d<10?("0"+d):(d));
    }

    public static int dayForWeek(Date pTime) throws Exception {
        Calendar c = Calendar.getInstance();
        c.setTime(pTime);

        int dayForWeek = 0;

        if(c.get(Calendar.DAY_OF_WEEK) == 1) {
            dayForWeek = 7;
        } else {
            dayForWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
        }

        return dayForWeek;
    }

    /**
     *
     * @Title: getLastDayOfWeek
     * @Description: 获取月第index周的星期天
     * @param date：'YYYY-MM'
     * @param index：第几周
     * @return yyyymmdd
     * @throws
     */
    public static String getLastDayOfWeek(String date,int index){
        if(index<1){
//            throw new KeasRuntimeException("index is not right!");
        }

        String[] dates=date.split("-");
        int year=Integer.parseInt(dates[0]);
        int month=Integer.parseInt(dates[1]);
        Calendar cal = new GregorianCalendar(year, month-1, 1);
        int a =cal.get(Calendar.DAY_OF_WEEK);
        if(a!=1){//如果月第一天不是星期天，则先算出第一个星期天
            cal.add(Calendar.DATE, 8-a);
        }

        cal.add(Calendar.DATE, 7*(index-1));//月的第index个星期一

        int m = cal.get(Calendar.MONTH)+1;
        int d = cal.get(Calendar.DATE);
        return cal.get(Calendar.YEAR)+ ""+(m<10?("0"+m):(m))+""+(d<10?("0"+d):(d));
    }


    public static void main(String[] args) throws Exception {
        System.out.println(DateUtil.getLastDayOfWeek("2012-06",2));
        // System.out.println(DateUtil.getFirstDayOfWeek("2012-2"));
//		 Calendar   c   =   new   GregorianCalendar(2012,  12,   1);
//		 System.out.println(c.get(Calendar.YEAR));
//		 System.out.println(c.get(Calendar.MONTH));
//		 System.out.println(c.get(Calendar.DATE));
    }



}
