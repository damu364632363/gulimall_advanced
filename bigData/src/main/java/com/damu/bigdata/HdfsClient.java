package com.damu.bigdata;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.yarn.webapp.hamlet2.Hamlet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class HdfsClient {

    private FileSystem fs;

    @Before
    public void init() throws URISyntaxException, IOException, InterruptedException {
        // 1 获取文件系统
        Configuration configuration = new Configuration();
        // FileSystem fs = FileSystem.get(new URI("hdfs://hadoop102:8020"), configuration);
        fs = FileSystem.get(new URI("hdfs://master:8020"),
                configuration, "root");
    }

    @After
    public void close() throws IOException {
        fs.close();
    }


    /**
     *
     * 创建目录
     * @throws IOException
     */
    @Test
    public void makeDires() throws IOException {
        fs.mkdirs(new Path("/xxm/gn/"));
    }


//    @Test
//    public void testMkdirs() throws IOException, URISyntaxException,
//            InterruptedException {
//        // 1 获取文件系统
//        Configuration configuration = new Configuration();
//        // FileSystem fs = FileSystem.get(new URI("hdfs://hadoop102:8020"), configuration);
//        FileSystem fs = FileSystem.get(new URI("hdfs://master:8020"),
//                configuration, "root");
//        // 2 创建目录
//        fs.mkdirs(new Path("/xiyou/huaguoshan/"));
//        // 3 关闭资源
//        fs.close();
//    }

    //HDFS 文件上传（测试参数优先级）
    @Test
    public void testCopyFromLocalFile() throws IOException,
            InterruptedException, URISyntaxException {
//        // 1 获取文件系统
//        Configuration configuration = new Configuration();
//        configuration.set("dfs.replication", "2");
//        FileSystem fs = FileSystem.get(new URI("hdfs://master:8020"),
//                configuration, "root");
        // 2 上传文件
        fs.copyFromLocalFile(new Path("E:\\meili.txt"), new
                Path("/xxm/gn"));
//        // 3 关闭资源
//        fs.close();
    }

    //HDFS 文件下载
    @Test
    public void getFile() throws IOException,
            InterruptedException, URISyntaxException {
        fs.copyToLocalFile(false, new Path("hdfs://master/xxm/gn/meili.txt"), new Path("E:\\meili3.txt"),
                false);
    }

    //HDFS 文件更名和移动
    @Test
    public void testRename() throws IOException,
            InterruptedException, URISyntaxException {
//        fs.copyToLocalFile(false, new Path("hdfs://master/xxm/gn/meili.txt"), new Path("E:\\meili3.txt"), false);
        //参数一，源文件路径。参数二，目标路劲
        fs.rename(new Path("/xxm/gn/meili.txt"),new Path("/xxm/gn/meihouwang.txt"));
    }

    //HDFS文件删除
    @Test
    public void testDelete() throws IOException, InterruptedException,
            URISyntaxException {
        //路劲，是否递归
        fs.delete(new Path("/xiyou"), true);
    }


    //查看文件详情
    @Test
    public void testListFiles() throws IOException, InterruptedException,
            URISyntaxException {
        // 2 获取文件详情
        RemoteIterator<LocatedFileStatus> listFiles = fs.listFiles(new Path("/"),
                true);
        while (listFiles.hasNext()) {
            LocatedFileStatus fileStatus = listFiles.next();
            System.out.println("========" + fileStatus.getPath() + "=========");
            System.out.println(fileStatus.getPermission());
            System.out.println(fileStatus.getOwner());
            System.out.println(fileStatus.getGroup());
            System.out.println(fileStatus.getLen());
            System.out.println(fileStatus.getModificationTime());
            System.out.println(fileStatus.getReplication());
            System.out.println(fileStatus.getBlockSize());
            System.out.println(fileStatus.getPath().getName());
            // 获取块信息
            BlockLocation[] blockLocations = fileStatus.getBlockLocations();
            System.out.println(Arrays.toString(blockLocations));
        }
    }

    @Test
    public void testListStatus() throws IOException {
        //判断是文件还是文件夹
        FileStatus[] fileStatuses = fs.listStatus(new Path("/"));
        for (FileStatus fileStatus : fileStatuses){
            // 如果是文件
            if (fileStatus.isFile()) {
                System.out.println("f:"+fileStatus.getPath().getName());
            }else {
                System.out.println("d:"+fileStatus.getPath().getName());
            }
        }
    }


}
